# What is this repository for? #

A basic user management using CodeIgniter version 2.2.0

## Can ##
* create user
* admin roles
* user status
* register, activation, etc with email registration sending
* login remember me. cookie
* update profile, user, etc
* forgot password
* ..and more

## How do I get set up? ##

* create a folder in local server
* cd project
* clone repo
* check out "127_0_0_1.sql" to install in the database
* check out config files and adjust setup