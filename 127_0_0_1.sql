-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 22, 2016 at 02:46 PM
-- Server version: 5.6.16
-- PHP Version: 5.5.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `basic_user_management_template`
--

-- --------------------------------------------------------

--
-- Table structure for table `ci_sessions`
--

CREATE TABLE IF NOT EXISTS `ci_sessions` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) NOT NULL,
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `user_data` text NOT NULL,
  PRIMARY KEY (`session_id`),
  KEY `last_activity_idx` (`last_activity`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `user_role_name_status_names`
--

CREATE TABLE IF NOT EXISTS `user_role_name_status_names` (
  `i_id` int(11) NOT NULL AUTO_INCREMENT,
  `s_name` varchar(55) NOT NULL,
  PRIMARY KEY (`i_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `user_role_name_status_names`
--

INSERT INTO `user_role_name_status_names` (`i_id`, `s_name`) VALUES
(1, 'Activated'),
(2, 'Deactivated');

-- --------------------------------------------------------

--
-- Table structure for table `user_role_names`
--

CREATE TABLE IF NOT EXISTS `user_role_names` (
  `i_id` int(11) NOT NULL AUTO_INCREMENT,
  `s_name` varchar(55) NOT NULL,
  `i_urnsn_id` int(11) NOT NULL,
  PRIMARY KEY (`i_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `user_role_names`
--

INSERT INTO `user_role_names` (`i_id`, `s_name`, `i_urnsn_id`) VALUES
(1, 'Admin', 1),
(2, 'User Viewer', 1),
(3, 'Test', 2);

-- --------------------------------------------------------

--
-- Table structure for table `user_roles`
--

CREATE TABLE IF NOT EXISTS `user_roles` (
  `i_u_id` int(11) NOT NULL,
  `i_urn_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_roles`
--

INSERT INTO `user_roles` (`i_u_id`, `i_urn_id`) VALUES
(1, 1),
(2, 2);

-- --------------------------------------------------------

--
-- Table structure for table `user_status_names`
--

CREATE TABLE IF NOT EXISTS `user_status_names` (
  `i_id` int(11) NOT NULL AUTO_INCREMENT,
  `s_name` varchar(55) NOT NULL,
  PRIMARY KEY (`i_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `user_status_names`
--

INSERT INTO `user_status_names` (`i_id`, `s_name`) VALUES
(1, 'Activated'),
(2, 'Email Activation Required'),
(3, 'Deactivated'),
(4, 'Banned'),
(5, 'Lock - Max Failed Login');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `i_id` int(11) NOT NULL AUTO_INCREMENT,
  `s_date_registration` datetime NOT NULL,
  `s_email` varchar(55) NOT NULL,
  `s_username` varchar(55) NOT NULL,
  `s_firstname` varchar(55) NOT NULL,
  `s_lastname` varchar(55) NOT NULL,
  `s_password` varchar(255) NOT NULL,
  `s_unique_key` varchar(255) NOT NULL,
  `i_usn_id` int(11) NOT NULL,
  PRIMARY KEY (`i_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`i_id`, `s_date_registration`, `s_email`, `s_username`, `s_firstname`, `s_lastname`, `s_password`, `s_unique_key`, `i_usn_id`) VALUES
(1, '2014-11-21 00:00:00', 'admin1@yahoo.com', 'admin', 'Admin First Name 2', 'Admin Last Name 2', '15acb765a111eba6e36cf64d6560e09a', 'c485125bb5ea781b383cb65da95d23bf', 1),
(2, '2014-11-27 09:47:02', 'test1@yahoo.com', 'username6', 'First Name 1', 'Last Name 1', '15acb765a111eba6e36cf64d6560e09a', '440f67f6d62477f49b00d643ef94a735', 1);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
