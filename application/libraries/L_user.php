<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class L_user {

	/**
	* constructor
	* @desc		
	*
	**/
	public function __construct()
	{
		$this->CI =& get_instance();
		/*
		$this->CI->load->library('l_def_sql');
		$this->CI->load->model('m_user');
		$this->CI->load->model('m_def_table_fields');
		*/
		
	}
	
	
	/**
	* send_email_activation
	*
	* @desc 	used to send an email to user for account acctivation
	* @param 1 	a_params
	* @return: 	int
	*
	**/
	public function send_email_activation( $a_params = array() )
	{
		$i_send_email_result = 0;
		$a_send_email_result = array();

		if( isset($a_params) && !empty($a_params) && !empty($a_params['s_u_email']) && !empty($a_params['s_u_key']) )
		{
			$s_u_email = $a_params['s_u_email'];
			$s_u_key = $a_params['s_u_key'];
			
			$this->CI->load->library('encrypt');
			
			$s_email_from = SITE_URL_EMAIL;
			$s_email_to = $s_u_email;
			$s_email_subject = 'Email Activation';
			$s_email_message = '';
			$a_email_config = array();
			
			$s_email_user_activation_link = $s_u_key;
			$s_email_user_activation_link = $this->CI->encrypt->encode($s_email_user_activation_link);
			$s_email_user_activation_link = base64_encode($s_email_user_activation_link);
			
			/*
			echo $s_email_user_activation_link;
			exit();
			*/
			
			$a_html_email_activation_data['s_email_subject'] = $s_email_subject;
			$a_html_email_activation_data['s_email_user_activation_link'] = $s_email_user_activation_link;
			$s_v_html_email_activation = $this->CI->load->view('templates_v1/v_html_email_activation_v1', $a_html_email_activation_data, true);
			$s_email_message = $s_v_html_email_activation;
			
			$a_send_email_params = array();
			$a_send_email_params['s_email_from'] = $s_email_from;
			$a_send_email_params['s_email_to'] = $s_email_to;
			$a_send_email_params['s_email_subject'] = $s_email_subject;
			$a_send_email_params['s_email_message'] = $s_email_message;
			$a_send_email_params['a_email_config'] = $a_email_config;
			
			$i_send_email_result = $this->send_email( $a_send_email_params );

			$a_send_email_result['i_send_email_result'] = $i_send_email_result;

		}
		
		return $a_send_email_result;
	}
	
	
	/**
	* send_email
	*
	* @desc 	used for sending a basic email
	* @param 1 	a_params
	* @return: 	int
	*
	**/
	public function send_email( $a_params = array() )
	{
		$i_result = 0;
		
		$this->CI->load->library(array('email'));
		
		if( isset($a_params) && !empty($a_params) && !empty($a_params['s_email_from']) && !empty($a_params['s_email_to']) && !empty($a_params['s_email_subject']) && !empty($a_params['s_email_message']) )
		{
			$s_email_from = $a_params['s_email_from'];
			$s_email_to = $a_params['s_email_to'];
			$s_email_subject = $a_params['s_email_subject'];
			$s_email_message = $a_params['s_email_message'];
			$a_email_config = $a_params['a_email_config'];
		
			if( empty($a_email_config) )
			{
				/*
					NOTE - 11/29/2014:
					Default setup working on eleven2 after complaining. They fix it by sending a request to unban the shared hosting IP
						Your message has been successfully sent using the following protocol: mail
						User-Agent: CodeIgniter
						Date: Sat, 29 Nov 2014 04:08:12 +0100
						From: <no_reply@trusted-freelancer.com>
						Return-Path: <no_reply@trusted-freelancer.com>
						Reply-To: "no_reply@trusted-freelancer.com" <no_reply@trusted-freelancer.com>
						X-Sender: no_reply@trusted-freelancer.com
						X-Mailer: CodeIgniter
						X-Priority: 3 (Normal)
						Message-ID: <5479389c531dd@trusted-freelancer.com>
						Mime-Version: 1.0
						Content-Type: multipart/alternative; boundary="B_ALT_5479389c531e9"
						=?utf-8?Q?Email_Activation?=
						This is a multi-part message in MIME format.
						Your email application may not support this format.
					This site is working, but I tried trusted-freelancer.com contact form and its not working. 
						Error says mail and sendmail may not be setup for this server
						Weird because I test a simple file "mail.php" in trusted-freelancer root folder and its working, i received the test mail
						but the contact form was not
					Conclusion:
						If someday this mailing does not work again. Its either because
							- our shared hosting server is blacklisted again
							OR
							- same issue with trusted-freelancer that IDK now
				*/
				
				
				/*
				$a_email_config['mailtype'] = 'html';
				$a_email_config['protocol'] = 'smtp';
				$a_email_config['smtp_host'] = 'ssl://smtp.gmail.com';
				$a_email_config['smtp_port']    = '465';
				*/
				
				/*
				$a_email_config['protocol']    = 'sendmail';
				$a_email_config['smtp_host']    = 'ssl://smtp.gmail.com';
				$a_email_config['smtp_port']    = '465';
				$a_email_config['smtp_user']    = 'jmgcheng@gmail.com';
				$a_email_config['smtp_pass']    = 'Vp112168456120779+';
				*/
				//$a_email_config['smtp_timeout'] = '7';
				//$a_email_config['charset']    = 'utf-8';
				//$a_email_config['newline']    = "\r\n";
				//$a_email_config['validation'] = TRUE;
				$a_email_config['mailtype'] = 'html'; // or text
			}
			
			$this->CI->email->initialize($a_email_config);
			$this->CI->email->from($s_email_from);
			$this->CI->email->to($s_email_to);
			$this->CI->email->subject($s_email_subject);
			$this->CI->email->message($s_email_message);
			
			if ( $this->CI->email->send() )
			{
				$i_result = 1;
			}

			/*
			echo $this->CI->email->print_debugger();
			exit();
			*/

		}
		
		return $i_result;
	}
	
	
	/**
	* send_reset_password
	*
	* @desc 	used to send and email to the user with a link so he can go to the url link and update his password
	* @param 1 	a_params
	* @return 	int
	*
	**/
	public function send_reset_password( $a_params = array() )
	{
		$i_send_email_result = 0;
		$a_send_email_result = array();
		
		
		if( !empty($a_params['s_u_email_or_username']) )
		{
			
			
			//$a_user_details = $this->CI->m_user->get_u_by_uemailouusername($s_u_email_or_username);
			$a_u_row_result = array();
			$a_u_query_where = array();
			$a_u_query_where_string = array();
			$a_u_query_limit = array();
			$a_u_query_params = array();
			array_push( $a_u_query_where_string, "(users.s_email = '" . $a_params['s_u_email_or_username'] . "' OR users.s_username = '" . $a_params['s_u_email_or_username'] . "')" );
			$a_u_query_limit = array('i_limit' => 1, 'i_offset' => 0);
			$a_u_query_params['a_where_string'] = $a_u_query_where_string;
			$a_u_query_params['a_limit'] = $a_u_query_limit;
			$a_u_query_params['s_table_fields'] = $this->CI->m_def_table_fields->s_users_fields;
			$a_u_query_params['s_table_name'] = 'users';
			$a_u_result = $this->CI->l_def_sql->read_data( $a_u_query_params );
			if( isset($a_u_result) && !empty($a_u_result) )
			{ $a_u_row_result = $a_u_result[0]; }
			
			if( !empty($a_u_row_result) )
			{
				$this->CI->load->library('encrypt');
				
				$s_email_from = SITE_URL_EMAIL;
				$s_email_to = $a_u_row_result['s_u_email'];
				$s_email_subject = 'Request for Reset Password';
				$s_email_message = '';
				$a_email_config = array();
				
				$s_u_key = $a_u_row_result['s_u_unique_key'];
				$s_u_email = $a_u_row_result['s_u_email'];
				$s_reset_password_link_details = 's_email='. $s_u_email .'&s_key='. $s_u_key .'';
				$s_reset_password_link_details = $this->CI->encrypt->encode($s_reset_password_link_details);
				$s_reset_password_link_details = base64_encode($s_reset_password_link_details);

				$a_html_email_reset_password_data['s_email_subject'] = $s_email_subject;
				$a_html_email_reset_password_data['s_reset_password_link_details'] = $s_reset_password_link_details;
				
				$s_v_html_email_reset_password = $this->CI->load->view('templates_v1/v_html_email_request_reset_password_v1', $a_html_email_reset_password_data, true);
				$s_email_message = $s_v_html_email_reset_password;

				/*
				echo $s_email_message;
				exit();
				*/
				
				$a_send_email_params = array();
				$a_send_email_params['s_email_from'] = $s_email_from;
				$a_send_email_params['s_email_to'] = $s_email_to;
				$a_send_email_params['s_email_subject'] = $s_email_subject;
				$a_send_email_params['s_email_message'] = $s_email_message;
				$a_send_email_params['a_email_config'] = $a_email_config;
				$i_send_email_result = $this->send_email( $a_send_email_params );
				
				$a_send_email_result['i_send_email_result'] = $i_send_email_result;
			}
		}
		
		return $a_send_email_result;
	}

	
	/**
	* login
	*
	* @desc 	
	* @param 1 	a_params
	* @return 	array
	*
	**/
	public function login( $a_params = array() )
	{
		// ========================================================
		$a_result = array();
		$a_result['i_result'] = 0;
		// ========================================================
		
		
		// ========================================================
		$this->CI->load->library('l_def_sql');
		$this->CI->load->model('m_def_table_fields');
		
		// ========================================================

		
		// ========================================================
		if( !empty($a_params) && !empty($a_params['s_u_email_or_username']) && !empty($a_params['s_u_password']) )
		{
			//$a_user_details = $this->CI->m_user->get_u_by_upasswordnuemailousername( $a_params );
			$a_u_row_result = array();
			$a_u_query_where = array();
			$a_u_query_where_string = array();
			$a_u_query_limit = array();
			$a_u_query_params = array();
			array_push( $a_u_query_where, array( 's_field' => 'users.s_password', 'a_data' => $a_params['s_u_password'] ) );
			array_push( $a_u_query_where_string, "(users.s_email = '" . $a_params['s_u_email_or_username'] . "' OR users.s_username = '" . $a_params['s_u_email_or_username'] . "')" );
			$a_u_query_limit = array('i_limit' => 1, 'i_offset' => 0);
			$a_u_query_params['a_where'] = $a_u_query_where;
			$a_u_query_params['a_where_string'] = $a_u_query_where_string;
			$a_u_query_params['a_limit'] = $a_u_query_limit;
			$a_u_query_params['s_table_fields'] = $this->CI->m_def_table_fields->s_users_fields;
			$a_u_query_params['s_table_name'] = 'users';
			$a_u_result = $this->CI->l_def_sql->read_data( $a_u_query_params );
			if( isset($a_u_result) && !empty($a_u_result) )
			{ $a_u_row_result = $a_u_result[0]; }
			

			if( !empty($a_u_row_result) )
			{
				if( $a_u_row_result['i_u_usn_id'] == 1 ) // by default, 1 is Activated
				{
					$a_session_user_details = array(
						'a_user_details'  => $a_u_row_result,
						'b_is_user_login'  => TRUE
					);
					
					$this->CI->session->set_userdata($a_session_user_details);

					$a_result['i_result'] = 1;
				}
				elseif( $a_u_row_result['i_u_usn_id'] == 2 ) // by default
				{
					$a_result['s_message_status'] = 'info';
					$a_result['s_message_notice'] = 'Email Activation Required';
				}
				elseif( $a_u_row_result['i_u_usn_id'] == 3 ) // by default
				{
					$a_result['s_message_status'] = 'info';
					$a_result['s_message_notice'] = 'Deactivated';
				}
				elseif( $a_u_row_result['i_u_usn_id'] == 4 ) // by default
				{
					$a_result['s_message_status'] = 'error';
					$a_result['s_message_notice'] = 'Banned';
				}
				elseif( $a_u_row_result['i_u_usn_id'] == 5 ) // by default
				{
					$a_result['s_message_status'] = 'info';
					$a_result['s_message_notice'] = 'Lock - Max Failed Login';
				}
				else
				{
					$a_result['s_message_status'] = 'info';
					$a_result['s_message_subject'] = 'Login';
					$a_result['s_message_notice'] = 'Account NOT activated';
				}
			}
			else
			{
				$a_result['s_message_status'] = 'error';
				$a_result['s_message_subject'] = 'Login';
				$a_result['s_message_notice'] = 'Incorrect Details';
			}
		}

		return $a_result;
		// ========================================================
	}
	
	
	/**
	* set_autologin
	*
	* @desc 	used to set cookie details for autologin later
	* @params  	void
	* @return 	void
	*
	**/
	public function set_autologin()
	{
		$a_user_details = $this->CI->session->userdata('a_user_details');
		
		$s_u_unique_key = $a_user_details['s_u_unique_key'];
		$s_u_unique_key = $this->CI->encrypt->encode($s_u_unique_key);
		$s_u_unique_key = base64_encode($s_u_unique_key);

		$a_autologin_cookie = array(
			'name'   => 'sitename_autologin',
			'value'  => $s_u_unique_key,
			'expire' => 3600,
			'path'   => '/'
		);

		$this->CI->input->set_cookie($a_autologin_cookie);
	}
	
	
	/**
	* autologin
	*
	* @desc 	Called in a Controller constructor. Auto Loggedin user if current user is currently logged out and has cookie details that match s_u_key
	* @params  	void
	* @return 	void
	*
	**/
	public function autologin()
	{
		// ========================================================
		$this->CI->load->library('l_def_sql');
		$this->CI->load->model('m_def_table_fields');
		
		// ========================================================
		
		
		// ========================================================
		$a_user_details = $this->CI->session->userdata('a_user_details');
		
		
		/*
			Only do autologin if empty($a_user_details) which means user is not logged in
				to test, set session timer in config to 1 sec
		*/
		if( empty($a_user_details) )
		{
			$s_u_unique_key = $this->CI->input->cookie('sitename_autologin', TRUE);
			
			/*
				echo 'testestset';
				echo $s_u_unique_key;
				exit();
			*/
			
			if( !empty($s_u_unique_key) )
			{
				$s_u_unique_key = base64_decode($s_u_unique_key);
				$s_u_unique_key = $this->CI->encrypt->decode($s_u_unique_key);
				
				
				//$a_user_details = $this->CI->m_user->get_u_by_ukey($s_u_unique_key);
				$a_u_row_result = array();
				$a_u_query_where = array();
				$a_u_query_where_string = array();
				$a_u_query_limit = array();
				$a_u_query_params = array();
				array_push( $a_u_query_where, array( 's_field' => 'users.s_unique_key', 'a_data' => $s_u_unique_key ) );
				$a_u_query_limit = array('i_limit' => 1, 'i_offset' => 0);
				$a_u_query_params['a_where'] = $a_u_query_where;
				$a_u_query_params['a_limit'] = $a_u_query_limit;
				$a_u_query_params['s_table_fields'] = $this->CI->m_def_table_fields->s_users_fields;
				$a_u_query_params['s_table_name'] = 'users';
				$a_u_result = $this->CI->l_def_sql->read_data( $a_u_query_params );
				if( isset($a_u_result) && !empty($a_u_result) )
				{ $a_u_row_result = $a_u_result[0]; }
				
				
				if( !empty($a_u_row_result) )
				{
					$a_session_user_details = array(
						'a_user_details'  => $a_u_row_result,
						'b_is_user_login'  => TRUE
					);
					
					$this->CI->session->set_userdata($a_session_user_details);
					$this->set_autologin();
				}
			}
		}
		// ========================================================
	}
	
	
}