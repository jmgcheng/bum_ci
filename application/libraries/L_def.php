<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class L_def {

	/**
	* constructor
	* @desc		
	*
	**/
	public function __construct()
	{
		$this->CI =& get_instance();
	}
	
	
	/**
	* is_existing
	* @desc		addon callback function for form validation. Check if data is existing.
				Opposite to what is_unique[...] is used
	*
	**/
	public function is_existing( $m_var, $s_table_detail )
	{
		//= Declare Start-Up Variables Here ====================
		$b_result = FALSE;
		$a_table_detail = array();
		
		//======================================================
		
		
		//======================================================
		$this->CI->load->library('l_def_sql');
		
		$this->CI->load->model('m_def_table_fields');
		//======================================================
		
		
		//======================================================
		$a_table_detail = explode('.', $s_table_detail);
	
	
		$a_query_where = array();
		$a_query_params = array();
		array_push( $a_query_where, array( 's_field' => $a_table_detail[0] . '.' . $a_table_detail[1], 'a_data' => $m_var ) );
		$a_query_params['a_where'] = $a_query_where;
		$a_query_params['s_table_fields'] = $a_table_detail[0] . '.' . $a_table_detail[1];
		$a_query_params['s_table_name'] = $a_table_detail[0];
		$a_product_duplicate_result = array();
		$a_product_duplicate_result = $this->CI->l_def_sql->read_data( $a_query_params );
		
		
		if( isset($a_product_duplicate_result) && !empty($a_product_duplicate_result) )
		{
			$b_result = TRUE;
		}
		else
		{
			$this->CI->form_validation->set_message('is_existing', 'The value entered at %s does not existing in the database');
		}
		
		return $b_result;
		//======================================================
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}