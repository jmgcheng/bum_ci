<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['default_controller'] = "c_home";
$route['testmail_1'] = "c_home/testmail_1";
$route['404_override'] = '';

$route['notice/(:any)'] = 'c_notice';
$route['notice'] = 'c_notice';

$route['user/register_form'] = 'c_user/register_form';
$route['user/register'] = 'c_user/register';
$route['user/register_activation_required'] = 'c_user/register_activation_required';
$route['user/register_activation/.+'] = 'c_user/register_activation';
$route['user/update_captcha_register_form_ajx'] = 'c_user/update_captcha_register_form_ajx';
$route['user/login_form'] = 'c_user/login_form';
$route['user/login'] = 'c_user/login';
$route['user/logout'] = 'c_user/logout';

$route['user/request_forgot_password_form'] = 'c_user/request_forgot_password_form';
$route['user/request_forgot_password'] = 'c_user/request_forgot_password';
$route['user/request_reset_forgot_password_form/(:any)'] = 'c_user/request_reset_forgot_password_form';
$route['user/request_reset_forgot_password/(:any)'] = 'c_user/request_reset_forgot_password';

$route['user/read_all_user/(:any)'] = 'c_user/read_all_user';
$route['user/read_all_user'] = 'c_user/read_all_user';

$route['user/read_profile/(:any)'] = 'c_user/read_profile';
$route['user/read_profile'] = 'c_user/read_profile';

$route['account/read_profile'] = 'c_account/read_profile';
$route['account/update_profile_form'] = 'c_account/update_profile_form';
$route['account/update_profile'] = 'c_account/update_profile';

$route['admin/read_all_user/(:any)'] = 'c_admin/read_all_user';
$route['admin/read_all_user'] = 'c_admin/read_all_user';
$route['admin/create_user_form'] = 'c_admin/create_user_form';
$route['admin/create_user'] = 'c_admin/create_user';
$route['admin/read_profile/(:any)'] = 'c_admin/read_profile';
$route['admin/read_profile'] = 'c_admin/read_profile';
$route['admin/update_user_form/(:any)'] = 'c_admin/update_user_form';
$route['admin/update_user/(:any)'] = 'c_admin/update_user';

$route['admin/create_user_role_form'] = 'c_admin/create_user_role_form';
$route['admin/create_user_role'] = 'c_admin/create_user_role';
$route['admin/read_all_user_role/(:any)'] = 'c_admin/read_all_user_role';
$route['admin/read_all_user_role'] = 'c_admin/read_all_user_role';
$route['admin/update_user_role_form/(:any)'] = 'c_admin/update_user_role_form';
$route['admin/update_user_role/(:any)'] = 'c_admin/update_user_role';

$route['admin/create_user_status_form'] = 'c_admin/create_user_status_form';
$route['admin/create_user_status'] = 'c_admin/create_user_status';
$route['admin/read_all_user_status/(:any)'] = 'c_admin/read_all_user_status';
$route['admin/read_all_user_status'] = 'c_admin/read_all_user_status';
$route['admin/update_user_status_form/(:any)'] = 'c_admin/update_user_status_form';
$route['admin/update_user_status/(:any)'] = 'c_admin/update_user_status';

























/* End of file routes.php */
/* Location: ./application/config/routes.php */