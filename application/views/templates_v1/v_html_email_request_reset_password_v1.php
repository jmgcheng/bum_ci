<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>
			<?php
				if( isset($s_email_subject) && !empty($s_email_subject) )
				{
					echo $s_email_subject;
				}
			?>
		</title>
		<style type="text/css"></style>
	</head>
	<body>
		<h1>
			<?php
				if( isset($s_email_subject) && !empty($s_email_subject) )
				{
					echo $s_email_subject;
				}
			?>
		</h1>
		<p>
			Good Day, 
			
			<br/><br/>
			
			Someone requested a reset password for this account. 
			Kindly disregard this email if the request didn't came from you.
			
			<br/><br/>
			
			<strong>Reset Account Password Link:</strong>
			<?php if( isset($s_reset_password_link_details) && !empty($s_reset_password_link_details) ) : ?>
				<a href="<?php echo base_url() . 'user/request_reset_forgot_password_form/' . $s_reset_password_link_details; ?>"><?php echo base_url() . 'user/request_reset_forgot_password_form/' . $s_reset_password_link_details; ?></a>
			<?php endif; ?>
			
			<br/><br/><br/>
			
			Many Thanks,
			
			<br/>
			
			<?php
				if( isset($s_email_subject) && !empty($s_email_subject) )
				{
					echo $s_email_subject;
				}
			?>
		</p>
		
	</body>
</html>