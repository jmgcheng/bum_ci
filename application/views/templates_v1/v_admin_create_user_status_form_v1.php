		<main>
						
			
			<?php  if( isset($s_nav_menu) && !empty($s_nav_menu) ) { echo $s_nav_menu; } ?>
			
			
			
			<section class="sec_maincontent_cls">
				<h2>
					Admin - Create User Status Names
				</h2>
				
				<form id="frm_admin_create_user_status_names" name="frm_admin_create_user_status_names" action="<?php echo base_url() . 'admin/create_user_status'; ?>" method="post">
				
					<table>
						
						<tr>
							<td>
								<label for="">Name:</label>
							</td>
							<td>
								<input type="text" id="txt_admin_create_user_status_names_name" name="txt_admin_create_user_status_names_name" value="<?php echo set_value('txt_admin_create_user_status_names_name'); ?>" placeholder="" />
								<?php if( isset($a_form_notice['s_txt_admin_create_user_status_names_name_error']) && !empty($a_form_notice['s_txt_admin_create_user_status_names_name_error']) ) : ?>
									<p class="p_texterror_cls"><?php echo $a_form_notice['s_txt_admin_create_user_status_names_name_error']; ?></p>
								<?php endif; ?>
							</td>
						</tr>
						
						<?php
							if( isset($s_view_site_responses) && !empty($s_view_site_responses) ):
						?>
						<tr>
							<td colspan="2">
								<?php
									echo $s_view_site_responses;
								?>
							</td>
						</tr>
						<?php
							endif;
						?>
						
						<tr>
							<td colspan="2">
								<input type="submit" value="Create">
							</td>
						</tr>
						
					</table>
				
				</form>
				
			</section>
			
			
			
			<div style="clear:both;"></div>
		</main>
		