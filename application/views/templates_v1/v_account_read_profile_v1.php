		<main>
						
			
			<?php  if( isset($s_nav_menu) && !empty($s_nav_menu) ) { echo $s_nav_menu; } ?>
			
			
			<section class="sec_maincontent_cls">
				<h2>
					Account - View Profile
				</h2>
				
				<?php
					if( isset($a_user_details) && !empty($a_user_details) ):
						
						/**/
						if( isset($a_user_roles_result) && !empty($a_user_roles_result) )
						{
							$s_roles = '';
							foreach( $a_user_roles_result AS $a_user_roles_result_row )
							{
								if( isset($s_roles) && !empty($s_roles) )
								{
									$s_roles = $s_roles . ', ' . $a_user_roles_result_row['s_urn_name'];
								}
								else
								{
									$s_roles = $a_user_roles_result_row['s_urn_name'];
								}
							}
						}
				?>
				<table>
					<tr>
						<td>
							Username
						</td>
						<td>
							<?php echo $a_user_details['s_u_username']; ?>
						</td>
					</tr>
					<tr>
						<td>
							Firstname
						</td>
						<td>
							<?php echo $a_user_details['s_u_firstname']; ?>
						</td>
					</tr>
					<tr>	
						<td>
							Lastname
						</td>
						<td>
							<?php echo $a_user_details['s_u_lastname']; ?>
						</td>
					</tr>

					<tr>
						<td>
							Roles
						</td>
						<td>
							<?php
								if( isset($s_roles) && !empty($s_roles) ):
									echo $s_roles;
								else:
							?>
								no roles assigned
							<?php
								endif;
							?>
						</td>
					</tr>
					
					<tr>
						<td colspan="2">
							<a href="<?php echo base_url() . 'account/update_profile_form'; ?>">
								Edit Profile
							</a>
						</td>
					</tr>
					
				</table>
				<?php
					else:
				?>
				<table>
					<tr>
						<td>
							No Data
						</td>
					</tr>
				</table>
				<?php
					endif;
				?>
				
			</section>
			
			
			
			<div style="clear:both;"></div>
		</main>
