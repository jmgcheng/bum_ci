
		<main>
				
				
			<?php  if( isset($s_nav_menu) && !empty($s_nav_menu) ) { echo $s_nav_menu; } ?>
			
			
			
			<section class="sec_maincontent_cls">
				<h2>
					Admin - Update User Status Names
				</h2>
				
				<form id="frm_admin_update_user_status_name" name="frm_admin_update_user_status_name" action="<?php echo base_url() . 'admin/update_user_status/user_status_name_id/' . $a_user_status_names_row_result['i_usn_id']; ?>" method="post">
				
					<table>
						
						<tr>
							<td>
								<label for="">Name:</label>
							</td>
							<td>
								<input type="text" id="txt_admin_update_user_status_name_name" name="txt_admin_update_user_status_name_name" value="<?php if( isset($a_user_status_names_row_result['s_usn_name']) && !empty($a_user_status_names_row_result['s_usn_name']) ) { echo $a_user_status_names_row_result['s_usn_name']; } ?>" placeholder="" />
								<?php if( isset($a_form_notice['s_txt_admin_update_user_status_name_name_error']) && !empty($a_form_notice['s_txt_admin_update_user_status_name_name_error']) ) : ?>
									<p class="p_texterror_cls"><?php echo $a_form_notice['s_txt_admin_update_user_status_name_name_error']; ?></p>
								<?php endif; ?>
							</td>
						</tr>
						
						<?php
							if( isset($s_view_site_responses) && !empty($s_view_site_responses) ):
						?>
						<tr>
							<td colspan="2">
								<?php
									echo $s_view_site_responses;
								?>
							</td>
						</tr>
						<?php
							endif;
						?>
						
						<tr>
							<td colspan="2">
								<input type="submit" name="submit" value="Delete">
								<input type="submit" name="submit" value="Update">
							</td>
						</tr>
						
					</table>
				
				</form>
				
			</section>
			
			
			
			<div style="clear:both;"></div>
		</main>
		
		