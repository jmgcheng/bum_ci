		<main>
						
			
			<?php  if( isset($s_nav_menu) && !empty($s_nav_menu) ) { echo $s_nav_menu; } ?>
			
			
			<section class="sec_maincontent_cls">
				<h2>
					Admin - View Users
				</h2>
				
				<br/>
				<section>
					
					<form id="frm_filter_admin_read_all_users" name="frm_filter_admin_read_all_users" action="<?php echo base_url() . 'admin/read_all_user'; ?>" method="post">
					
						<table>
							<tr>
								<th colspan="2">
									Filter
								</th>
							</tr>
							<tr>
								<td>
									Status
								</td>
								<td>
									<select id="opt_filter_admin_read_all_users_status" name="opt_filter_admin_read_all_users_status">
										<option value="0" <?php if( !isset($a_assoc_uri['status']) || empty($a_assoc_uri['status']) || $a_assoc_uri['status'] == 0 ) { echo ' selected="selected" '; } ?> >All</option>
										<?php
											if( isset($a_user_status_names_result) && !empty($a_user_status_names_result) ):
												foreach( $a_user_status_names_result AS $a_user_status_names_result_row ):
										?>
										<option value="<?php echo $a_user_status_names_result_row['i_usn_id']; ?>" <?php if( isset($a_assoc_uri['status']) && !empty($a_assoc_uri['status']) && $a_assoc_uri['status'] == $a_user_status_names_result_row['i_usn_id'] ) { echo ' selected="selected" '; } ?> ><?php echo $a_user_status_names_result_row['s_usn_name']; ?></option>
										<?php
												endforeach;
											endif;
										?>
									</select>
								</td>
							</tr>
							<tr>
								<td>
									Order By
								</td>
								<td>
									<select id="opt_filter_admin_read_all_users_sort" name="opt_filter_admin_read_all_users_sort">
										<?php
											if( isset($a_expected_sort) && !empty($a_expected_sort) ) :
												foreach( $a_expected_sort AS $s_key => $s_expected_sort_row ) :
										?>
										<option value="<?php echo $s_key; ?>" <?php if( isset($a_assoc_uri['sort']) && !empty($a_assoc_uri['sort']) && $a_assoc_uri['sort'] == $s_key ) { echo ' selected="selected" '; } ?> ><?php echo $s_expected_sort_row; ?></option>
										<?php
												endforeach ;
											endif ;
										?>
									</select>
									<br/>
									<select id="opt_filter_admin_read_all_users_order" name="opt_filter_admin_read_all_users_order">
										<?php
											if( isset($a_expected_order) && !empty($a_expected_order) ) :
												foreach( $a_expected_order AS $s_key => $s_expected_order_row ) :
										?>
										<option value="<?php echo $s_key; ?>" <?php if( isset($a_assoc_uri['order']) && !empty($a_assoc_uri['order']) && $a_assoc_uri['order'] == $s_key ) { echo ' selected="selected" '; } ?> ><?php echo $s_expected_order_row; ?></option>
										<?php
												endforeach ;
											endif ;
										?>
									</select>
								</td>
							</tr>
							<tr>
								<td>
									Limit
								</td>
								<td>
									<input type="text" id="txt_filter_admin_read_all_users_limit" name="txt_filter_admin_read_all_users_limit" value="<?php if( isset($a_assoc_uri['limit']) && !empty($a_assoc_uri['limit']) ) { echo $a_assoc_uri['limit']; } ?>" />
								</td>
							</tr>
							<tr>
								<td>
									&nbsp;
								</td>
								<td>
									<input type="submit" id="" name="" value="Filter" />
								</td>
							</tr>
						</table>
					
					</form>
					
				</section>
				<br/>
				
				<table>
					<?php
						if(isset($a_user_result) && !empty($a_user_result)) :
					?>
					<tr>
						<th>
							DB ID
						</th>
						<th>
							Date Registered
						</th>
						<th>
							Email
						</th>
						<th>
							Username
						</th>
						<th>
							Firstname
						</th>
						<th>
							Lastname
						</th>
						<th>
							Status
						</th>
						<th>
							User Roles
						</th>
						<th>
							&nbsp;
						</th>
					</tr>
					<?php
						foreach( $a_user_result AS $a_user_result_row ) :
					?>
					
					<tr>
						<td>
							<?php
								echo $a_user_result_row['i_u_id'];
							?>
						</td>
						<td>
							<?php
								echo $a_user_result_row['s_u_date_registration'];
							?>
						</td>
						<td>
							<?php
								echo $a_user_result_row['s_u_email'];
							?>
						</td>
						<td>
							<?php
								echo $a_user_result_row['s_u_username'];
							?>
						</td>
						<td>
							<?php
								echo $a_user_result_row['s_u_firstname'];
							?>
						</td>
						<td>
							<?php
								echo $a_user_result_row['s_u_lastname'];
							?>
						</td>
						<td>
							<?php
								echo $a_user_result_row['s_usn_name'];
							?>
						</td>
						<td>
							<?php
								if( 	isset($a_showing_u_roles) && !empty($a_showing_u_roles) 
									&&	isset($a_showing_u_roles[$a_user_result_row['i_u_id']]) && !empty($a_showing_u_roles[$a_user_result_row['i_u_id']])
								)
								{
									echo $a_showing_u_roles[$a_user_result_row['i_u_id']];
								}
							?>
						</td>
						<td>
							<a href="<?php echo base_url() . 'admin/read_profile/user_id/' . $a_user_result_row['i_u_id']; ?>">
								View
							</a>, 
							<a href="<?php echo base_url() . 'admin/update_user_form/user_id/' . $a_user_result_row['i_u_id']; ?>">
								Edit
							</a>
						</td>
					</tr>
					<?php
						endforeach;
					?>
					<?php
						else :
					?>
					<tr>
						<td>
							No Data
						</td>
					</tr>
					<?php
						endif;
					?>
				</table>
				<nav class="nav_pagination_cls">
					<?php
						if( isset($s_page_links_pagination) && !empty($s_page_links_pagination) )
						{
							echo $s_page_links_pagination;
						}
					?>
				</nav>
			</section>
			
			
			
			<div style="clear:both;"></div>
		</main>
		