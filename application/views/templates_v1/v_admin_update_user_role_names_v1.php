
		<main>
						
			
			<?php  if( isset($s_nav_menu) && !empty($s_nav_menu) ) { echo $s_nav_menu; } ?>
			
			
			<section class="sec_maincontent_cls">
				<h2>
					User Role Names - Update
				</h2>
				
				<form id="frm_admin_update_user_role_name" name="frm_admin_update_user_role_name" action="<?php echo base_url() . 'admin/update_user_role/user_role_name_id/' . $a_user_role_names_row_result['i_urn_id']; ?>" method="post">
				
					<table>

						<tr>
							<td>
								<label for="">Name:</label>
							</td>
							<td>
								<input type="text" id="txt_admin_update_user_role_name_name" name="txt_admin_update_user_role_name_name" value="<?php if( isset($a_user_role_names_row_result['s_urn_name']) && !empty($a_user_role_names_row_result['s_urn_name']) ) { echo $a_user_role_names_row_result['s_urn_name']; } ?>" placeholder="" />
								<?php if( isset($a_form_notice['s_txt_admin_update_user_status_name_name_error']) && !empty($a_form_notice['s_txt_admin_update_user_status_name_name_error']) ) : ?>
									<p class="p_texterror_cls"><?php echo $a_form_notice['s_txt_admin_update_user_status_name_name_error']; ?></p>
								<?php endif; ?>
							</td>
						</tr>
						
						<tr>	
							<td>
								<label for="">Status:</label>
							</td>
							<td>
								<?php
									if( isset($a_user_role_name_status_names_result) && !empty($a_user_role_name_status_names_result) ):
								?>
								<select id="opt_admin_update_user_role_name_status" name="opt_admin_update_user_role_name_status">
									<?php
										foreach( $a_user_role_name_status_names_result AS $a_user_role_name_status_names_result_row ):
									?>
										<option value="<?php echo $a_user_role_name_status_names_result_row['i_urnsn_id']; ?>" <?php if( isset($a_user_role_names_row_result['i_urnsn_id']) && !empty($a_user_role_names_row_result['i_urnsn_id']) && $a_user_role_names_row_result['i_urnsn_id'] == $a_user_role_name_status_names_result_row['i_urnsn_id'] ) { echo ' selected="selected" '; } ?> ><?php echo $a_user_role_name_status_names_result_row['s_urnsn_name']; ?></option>
									<?php
										endforeach;
									?>
								</select>
								<?php
									endif;
								?>
							</td>
						</tr>
						
						<?php
							if( isset($s_view_site_responses) && !empty($s_view_site_responses) ):
						?>
						<tr>
							<td colspan="2">
								<?php
									echo $s_view_site_responses;
								?>
							</td>
						</tr>
						<?php
							endif;
						?>
						
						<tr>
							<td colspan="2">
								<input type="submit" value="Update">
							</td>
						</tr>
						
					</table>
				
				</form>
				
			</section>
			
			
			
			<div style="clear:both;"></div>
		</main>
		
		