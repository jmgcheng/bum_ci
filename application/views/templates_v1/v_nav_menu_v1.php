			<?php
				$a_user_details = $this->session->userdata('a_user_details');
				$a_user_roles_result = $this->session->userdata('a_user_roles_result');
				
				/*
				print_r($a_user_details);
				print_r($a_user_roles_result);
				exit();
				*/
			?>
			
			<section class="sec_nav_cls">
				<nav>
					<ul>
						<?php
							if( isset($a_user_details) && !empty($a_user_details) ) :
						?>
						
							<?php
								if( 	isset($a_user_roles_result) && !empty($a_user_roles_result) 
									&& (
												array_key_exists( 1, $a_user_roles_result )
											||	array_key_exists( 2, $a_user_roles_result )
										)	
								) :
							?>
						<li>
							<a href="<?php echo base_url() . 'user/read_all_user'; ?>">
								User - View Users
							</a>
						</li>
							<?php
								endif;
							?>
							
							<?php
								if( 	isset($a_user_roles_result) && !empty($a_user_roles_result) 
									&& (array_key_exists( 1, $a_user_roles_result ))	
								) :
							?>
						<li>
							<a href="<?php echo base_url() . 'admin/create_user_form'; ?>">
								Admin - Create User
							</a>
						</li>
							<?php
								endif;
							?>
						
							<?php
								if( 	isset($a_user_roles_result) && !empty($a_user_roles_result) 
									&& (array_key_exists( 1, $a_user_roles_result ))	
								) :
							?>
						<li>
							<a href="<?php echo base_url() . 'admin/read_all_user'; ?>">
								Admin - View Users
							</a>
						</li>
							<?php
								endif;
							?>
						
							<?php
								if( 	isset($a_user_roles_result) && !empty($a_user_roles_result) 
									&& (array_key_exists( 1, $a_user_roles_result ))	
								) :
							?>
						<li>
							<a href="<?php echo base_url() . 'admin/create_user_role_form'; ?>">
								Admin - Create User Role
							</a>
						</li>
							<?php
								endif;
							?>
						
							<?php
								if( 	isset($a_user_roles_result) && !empty($a_user_roles_result) 
									&& (array_key_exists( 1, $a_user_roles_result ))	
								) :
							?>
						<li>
							<a href="<?php echo base_url() . 'admin/read_all_user_role'; ?>">
								Admin - View User Roles
							</a>
						</li>
							<?php
								endif;
							?>
						
							<?php
								if( 	isset($a_user_roles_result) && !empty($a_user_roles_result) 
									&& (array_key_exists( 1, $a_user_roles_result ))	
								) :
							?>
						<li>
							<a href="<?php echo base_url() . 'admin/create_user_status_form'; ?>">
								Admin - Create User Status
							</a>
						</li>
							<?php
								endif;
							?>
						
							<?php
								if( 	isset($a_user_roles_result) && !empty($a_user_roles_result) 
									&& (array_key_exists( 1, $a_user_roles_result ))	
								) :
							?>
						<li>
							<a href="<?php echo base_url() . 'admin/read_all_user_status'; ?>">
								Admin - View User Status
							</a>
						</li>
							<?php
								endif;
							?>
						
							
						<li>
							<a href="<?php echo base_url() . 'account/read_profile'; ?>">
								View Profile
							</a>
						</li>	
						<li>
							<a href="<?php echo base_url() . 'user/logout'; ?>">
								Logout
							</a>
						</li>
						
						<?php
							else:
						?>
						<li>
							<a href="<?php echo base_url() . 'user/login_form'; ?>">
								Login
							</a>
						</li>
						<li>
							<a href="<?php echo base_url() . 'user/register_form'; ?>">
								Register
							</a>
						</li>
						<?php
							endif;
						?>
					</ul>
				</nav>
			</section>
			