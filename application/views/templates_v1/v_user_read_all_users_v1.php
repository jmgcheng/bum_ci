		<main>
						
			
			<?php  if( isset($s_nav_menu) && !empty($s_nav_menu) ) { echo $s_nav_menu; } ?>
			
			
			<section class="sec_maincontent_cls">
				<h2>
					User - View Users
				</h2>
				<table>
					<?php
						if(isset($a_user_result) && !empty($a_user_result)) :
					?>
					<tr>
						<th>
							Username
						</th>
						<th>
							Firstname
						</th>
						<th>
							Lastname
						</th>
						<th>
							&nbsp;
						</th>
					</tr>
						<?php
							foreach( $a_user_result AS $a_user_result_row ) :
						?>
					<tr>
						<td>
							<?php echo $a_user_result_row['s_u_username']; ?>
						</td>
						<td>
							<?php echo $a_user_result_row['s_u_firstname']; ?>
						</td>
						<td>
							<?php echo $a_user_result_row['s_u_lastname']; ?>
						</td>
						<td>
							<a href="<?php echo base_url() . 'user/read_profile/s_username/' . $a_user_result_row['s_u_username']; ?>">
								View Profile
							</a>
						</td>
					</tr>
						<?php
							endforeach;
						?>
					<?php
						else:
					?>
					<tr>
						<td>
							No Data Yet..
						</td>
					</tr>
					<?php
						endif;
					?>
				</table>
				<nav class="nav_pagination_cls">
					<?php
						if( isset($s_page_links_pagination) && !empty($s_page_links_pagination) )
						{
							echo $s_page_links_pagination;
						}
					?>
				</nav>
			</section>
			
			
			
			<div style="clear:both;"></div>
		</main>
		