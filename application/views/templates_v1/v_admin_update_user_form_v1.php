
		<main>
						
			
			<?php  if( isset($s_nav_menu) && !empty($s_nav_menu) ) { echo $s_nav_menu; } ?>
						
			
			<section class="sec_maincontent_cls">
				<h2>
					User - Update
				</h2>
				
				<form id="frm_admin_update_user" name="frm_admin_update_user" action="<?php echo base_url() . 'admin/update_user/user_id/' . $a_u_row_result['i_u_id']; ?>" method="post">
				
				
					<table>
						<tr>
							<td>
								<label for="">Email:</label>
							</td>
							<td>
								<input type="text" id="txt_admin_update_user_email" name="txt_admin_update_user_email" value="<?php if( isset($a_u_row_result['s_u_email']) && !empty($a_u_row_result['s_u_email']) ) { echo $a_u_row_result['s_u_email']; } ?>" placeholder="" />
								<?php if( isset($a_form_notice['s_txt_admin_update_user_email_error']) && !empty($a_form_notice['s_txt_admin_update_user_email_error']) ) : ?>
									<p class="p_texterror_cls"><?php echo $a_form_notice['s_txt_admin_update_user_email_error']; ?></p>
								<?php endif; ?>
							</td>
						</tr>
						<tr>
							<td>
								<label for="">Username:</label>
							</td>
							<td>
								<input type="text" id="txt_admin_update_user_username" name="txt_admin_update_user_username" value="<?php if( isset($a_u_row_result['s_u_username']) && !empty($a_u_row_result['s_u_username']) ) { echo $a_u_row_result['s_u_username']; } ?>" placeholder="" />
								<?php if( isset($a_form_notice['s_txt_admin_update_user_username_error']) && !empty($a_form_notice['s_txt_admin_update_user_username_error']) ) : ?>
									<p class="p_texterror_cls"><?php echo $a_form_notice['s_txt_admin_update_user_username_error']; ?></p>
								<?php endif; ?>
							</td>
						</tr>
						
						<tr>
							<td>
								<label for="">Firstname:</label>
							</td>
							<td>
								<input type="text" id="txt_admin_update_user_firstname" name="txt_admin_update_user_firstname" value="<?php if( isset($a_u_row_result['s_u_firstname']) && !empty($a_u_row_result['s_u_firstname']) ) { echo $a_u_row_result['s_u_firstname']; } ?>" placeholder="" />
								<?php if( isset($a_form_notice['s_txt_admin_update_user_firstname_error']) && !empty($a_form_notice['s_txt_admin_update_user_firstname_error']) ) : ?>
									<p class="p_texterror_cls"><?php echo $a_form_notice['s_txt_admin_update_user_firstname_error']; ?></p>
								<?php endif; ?>
							</td>
						</tr>
						<tr>	
							<td>
								<label for="">Lastname:</label>
							</td>
							<td>
								<input type="text" id="txt_admin_update_user_lastname" name="txt_admin_update_user_lastname" value="<?php if( isset($a_u_row_result['s_u_lastname']) && !empty($a_u_row_result['s_u_lastname']) ) { echo $a_u_row_result['s_u_lastname']; } ?>" placeholder="" />
								<?php if( isset($a_form_notice['s_txt_admin_update_user_lastname_error']) && !empty($a_form_notice['s_txt_admin_update_user_lastname_error']) ) : ?>
									<p class="p_texterror_cls"><?php echo $a_form_notice['s_txt_admin_update_user_lastname_error']; ?></p>
								<?php endif; ?>
							</td>
						</tr>
						
						<tr>
							<td>
								<label for="">New Password:</label>
							</td>
							<td>
								<input type="password" id="txt_admin_update_user_password" name="txt_admin_update_user_password" value="" />
								<?php if( isset($a_form_notice['s_txt_admin_update_user_password_error']) && !empty($a_form_notice['s_txt_admin_update_user_password_error']) ) : ?>
									<p class="p_texterror_cls"><?php echo $a_form_notice['s_txt_admin_update_user_password_error']; ?></p>
								<?php endif; ?>
							</td>
						</tr>
						<tr>
							<td>
								<label for="">Retype New Password:</label>
							</td>
							<td>
								<input type="password" id="txt_admin_update_user_password_conf" name="txt_admin_update_user_password_conf" value="" />
								<?php if( isset($a_form_notice['s_txt_admin_update_user_password_conf_error']) && !empty($a_form_notice['s_txt_admin_update_user_password_conf_error']) ) : ?>
									<p class="p_texterror_cls"><?php echo $a_form_notice['s_txt_admin_update_user_password_conf_error']; ?></p>
								<?php endif; ?>
							</td>
						</tr>
						
						
						
						<tr>	
							<td>
								<label for="">Status:</label>
							</td>
							<td>
								<?php
									if( isset($a_user_status_names_result) && !empty($a_user_status_names_result) ):
								?>
								<select id="opt_admin_update_user_status" name="opt_admin_update_user_status">
									<?php
										foreach( $a_user_status_names_result AS $a_user_status_names_result_row ):
									?>
									<option value="<?php echo $a_user_status_names_result_row['i_usn_id']; ?>" <?php if( isset($a_u_row_result['i_usn_id']) && !empty($a_u_row_result['i_usn_id']) && $a_u_row_result['i_usn_id'] == $a_user_status_names_result_row['i_usn_id'] ) { echo ' selected="selected" '; } ?> ><?php echo $a_user_status_names_result_row['s_usn_name']; ?></option>
									<?php
										endforeach;
									?>
								</select>
								<?php
									endif; 
								?>
								<?php if( isset($a_form_notice['s_opt_admin_update_user_status_error']) && !empty($a_form_notice['s_opt_admin_update_user_status_error']) ) : ?>
									<p class="p_texterror_cls"><?php echo $a_form_notice['s_opt_admin_update_user_status_error']; ?></p>
								<?php endif; ?>
							</td>
						</tr>
						
						<tr>	
							<td>
								<label for="">Roles:</label>
							</td>
							<td>
								<?php
									if( isset($a_user_role_names_result) && !empty($a_user_role_names_result) ):
								?>
								<ul>
									<?php
										foreach( $a_user_role_names_result AS $a_user_role_names_result_row ):
									?>
									<li>
										<input type="checkbox" name="chk_admin_update_user_roles[]" value="<?php echo $a_user_role_names_result_row['i_urn_id']; ?>" <?php if( isset($a_u_roles_result) && !empty($a_u_roles_result) ) { if( array_key_exists($a_user_role_names_result_row['i_urn_id'], $a_u_roles_result) ) { echo ' checked="checked" '; } } ?> /><?php echo $a_user_role_names_result_row['s_urn_name']; ?>
									</li>
									<?php
										endforeach;
									?>
								</ul>
								<?php
									endif;
								?>
								<?php if( isset($a_form_notice['s_chk_admin_update_user_roles_error']) && !empty($a_form_notice['s_chk_admin_update_user_roles_error']) ) : ?>
									<p class="p_texterror_cls"><?php echo $a_form_notice['s_chk_admin_update_user_roles_error']; ?></p>
								<?php endif; ?>
							</td>
						</tr>
						
						<?php
							if( isset($s_view_site_responses) && !empty($s_view_site_responses) ):
						?>
						<tr>
							<td colspan="2">
								<?php
									echo $s_view_site_responses;
								?>
							</td>
						</tr>
						<?php
							endif;
						?>
						
						<tr>
							<td colspan="2">
								<input type="submit" value="Update">
							</td>
						</tr>
						
					</table>
				
				</form>
				
				
			</section>
			
			
			
			<div style="clear:both;"></div>
		</main>
		
		