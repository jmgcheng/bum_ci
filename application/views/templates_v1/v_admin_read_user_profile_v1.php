
		<main>
						
			
			<?php  if( isset($s_nav_menu) && !empty($s_nav_menu) ) { echo $s_nav_menu; } ?>
			
			
			<section class="sec_maincontent_cls">
				<h2>
					Admin - View User Profile
				</h2>
				<table>
					<tr>
						<td>
							DB ID
						</td>
						<td>
							<?php echo $a_u_row_result['i_u_id'];?>
						</td>
					</tr>
					<tr>
						<td>
							Date Registered
						</td>
						<td>
							<?php echo $a_u_row_result['s_u_date_registration'];?>
						</td>
					</tr>
					<tr>
						<td>
							Email
						</td>
						<td>
							<?php echo $a_u_row_result['s_u_email'];?>
						</td>
					</tr>
					<tr>
						<td>
							Username
						</td>
						<td>
							<?php echo $a_u_row_result['s_u_username'];?>
						</td>
					</tr>
					<tr>
						<td>
							Firstname
						</td>
						<td>
							<?php echo $a_u_row_result['s_u_firstname'];?>
						</td>
					</tr>
					<tr>	
						<td>
							Lastname
						</td>
						<td>
							<?php echo $a_u_row_result['s_u_lastname'];?>
						</td>
					</tr>
					<tr>	
						<td>
							Status
						</td>
						<td>
							<?php echo $a_u_row_result['s_usn_name'];?>
						</td>
					</tr>
					<tr>
						<td>
							Roles
						</td>
						<td>
							<?php
								$s_user_roles = '';
								if( isset($a_user_roles_result) && !empty($a_user_roles_result) )
								{
									foreach( $a_user_roles_result AS $a_user_roles_result_row )
									{
										if( isset($s_user_roles) && !empty($s_user_roles) )
										{
											$s_user_roles = $s_user_roles . ', ' . $a_user_roles_result_row['s_urn_name'];
										}
										else
										{
											$s_user_roles = $a_user_roles_result_row['s_urn_name'];
										}
									}
								}
							?>
							<?php
								echo $s_user_roles;
							?>
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<a href="<?php echo base_url() . 'admin/update_user_form/user_id/' . $a_u_row_result['i_u_id']; ?>">
								Edit User
							</a>
						</td>
					</tr>
				</table>
			</section>
			
			
			
			<div style="clear:both;"></div>
		</main>
		