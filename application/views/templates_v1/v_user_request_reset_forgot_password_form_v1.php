
		<main>
			
			
			<?php  if( isset($s_nav_menu) && !empty($s_nav_menu) ) { echo $s_nav_menu; } ?>
			
			
			
			<section class="sec_maincontent_cls">
				<header>
					<h2>
						User - Request Reset Forgot Password Form
					</h2>
				</header>
				<form id="frm_user_request_reset_password" name="frm_user_request_reset_password" action="<?php echo base_url() . 'user/request_reset_forgot_password/'; ?><?php if( isset($s_reset_password_link_details) && !empty($s_reset_password_link_details) ) { echo $s_reset_password_link_details; } ?>" method="post">
					<table>
						<tr>
							<td>
								<label for="">New Password:</label>
							</td>
							<td>
								<input type="password" id="txt_user_request_reset_password_password" name="txt_user_request_reset_password_password" value="" placeholder="" />
							</td>
						</tr>
						<tr>
							<td>
								<label for="">Retype New Password:</label>
							</td>
							<td>
								<input type="password" id="txt_user_request_reset_password_password_conf" name="txt_user_request_reset_password_password_conf" value="" placeholder="" />
							</td>
						</tr>
						
						<?php
							if( isset($s_view_site_responses) && !empty($s_view_site_responses) ):
						?>
						<tr>
							<td colspan="2">
								<?php
									echo $s_view_site_responses;
								?>
							</td>
						</tr>
						<?php
							endif;
						?>
						
						<tr>
							<td colspan="2">
								<input type="submit" value="Reset Password">
							</td>
						</tr>
						
					</table>
				</form>
			</section>
			
			
			
			<div style="clear:both;"></div>
		</main>
		