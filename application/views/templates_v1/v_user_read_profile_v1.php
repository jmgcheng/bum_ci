		<main>
						
			
			<?php  if( isset($s_nav_menu) && !empty($s_nav_menu) ) { echo $s_nav_menu; } ?>
			
			
			<section class="sec_maincontent_cls">
				<h2>
					User - View Profile
				</h2>
				
				<?php
					if( isset($a_u_row_result) && !empty($a_u_row_result) ):
				?>
				<table>
					<tr>
						<td>
							Username
						</td>
						<td>
							<?php echo $a_u_row_result['s_u_username']; ?>
						</td>
					</tr>
					<tr>
						<td>
							Firstname
						</td>
						<td>
							<?php echo $a_u_row_result['s_u_firstname']; ?>
						</td>
					</tr>
					<tr>	
						<td>
							Lastname
						</td>
						<td>
							<?php echo $a_u_row_result['s_u_lastname']; ?>
						</td>
					</tr>
					
				</table>
				<?php
					else:
				?>
				<table>
					<tr>
						<td>
							No Data
						</td>
					</tr>
				</table>
				<?php
					endif;
				?>
				
			</section>
			
			
			
			<div style="clear:both;"></div>
		</main>
