
		<main>
						
			
			<?php  if( isset($s_nav_menu) && !empty($s_nav_menu) ) { echo $s_nav_menu; } ?>
			
			
			<section class="sec_maincontent_cls">
				<h2>
					Admin - View User Role Names
				</h2>
				
				<br/>
				<section>
					<form id="frm_filter_admin_read_all_user_role_names" name="frm_filter_admin_read_all_user_role_names" action="<?php echo base_url() . 'admin/read_all_user_role'; ?>" method="post">
						<table>
							<tr>
								<th colspan="2">
									Filter
								</th>
							</tr>
							<tr>
								<td>
									Status
								</td>
								<td>
									<select id="opt_filter_admin_read_all_user_role_names_status" name="opt_filter_admin_read_all_user_role_names_status">
										<option value="0" <?php if( !isset($a_assoc_uri['status']) || empty($a_assoc_uri['status']) || $a_assoc_uri['status'] == 0 ) { echo ' selected="selected" '; } ?> >All</option>
										<?php
											if( isset($a_user_role_name_status_names_result) && !empty($a_user_role_name_status_names_result) ):
												foreach( $a_user_role_name_status_names_result AS $a_user_role_name_status_names_result_row ):
										?>
										<option value="<?php echo $a_user_role_name_status_names_result_row['i_urnsn_id']; ?>" <?php if( isset($a_assoc_uri['status']) && !empty($a_assoc_uri['status']) && $a_assoc_uri['status'] == $a_user_role_name_status_names_result_row['i_urnsn_id'] ) { echo ' selected="selected" '; } ?> ><?php echo $a_user_role_name_status_names_result_row['s_urnsn_name']; ?></option>
										<?php
												endforeach;
											endif;
										?>
									</select>
								</td>
							</tr>
							<tr>
								<td>
									Order By
								</td>
								<td>
									<select id="opt_filter_admin_read_all_user_role_names_sort" name="opt_filter_admin_read_all_user_role_names_sort">
										<?php
											if( isset($a_expected_sort) && !empty($a_expected_sort) ) :
												foreach( $a_expected_sort AS $s_key => $s_expected_sort_row ) :
										?>
										<option value="<?php echo $s_key; ?>" <?php if( isset($a_assoc_uri['sort']) && !empty($a_assoc_uri['sort']) && $a_assoc_uri['sort'] == $s_key ) { echo ' selected="selected" '; } ?> ><?php echo $s_expected_sort_row; ?></option>
										<?php
												endforeach ;
											endif ;
										?>
									</select>
									<br/>
									<select id="opt_filter_admin_read_all_user_role_names_order" name="opt_filter_admin_read_all_user_role_names_order">
										<?php
											if( isset($a_expected_order) && !empty($a_expected_order) ) :
												foreach( $a_expected_order AS $s_key => $s_expected_order_row ) :
										?>
										<option value="<?php echo $s_key; ?>" <?php if( isset($a_assoc_uri['order']) && !empty($a_assoc_uri['order']) && $a_assoc_uri['order'] == $s_key ) { echo ' selected="selected" '; } ?> ><?php echo $s_expected_order_row; ?></option>
										<?php
												endforeach ;
											endif ;
										?>
									</select>
								</td>
							</tr>
							<tr>
								<td>
									Limit
								</td>
								<td>
									<input type="text" id="txt_filter_admin_read_all_users_limit" name="txt_filter_admin_read_all_users_limit" value="<?php if( isset($a_assoc_uri['limit']) && !empty($a_assoc_uri['limit']) ) { echo $a_assoc_uri['limit']; } ?>" />
								</td>
							</tr>
							<tr>
								<td>
									&nbsp;
								</td>
								<td>
									<input type="submit" id="" name="" value="Filter" />
								</td>
							</tr>
						</table>
					
					</form>
				</section>
				<br/>
				
				<table>
					<?php
						if(isset($a_user_role_names_result) && !empty($a_user_role_names_result)) :
					?>
					<tr>
						<th>
							DB ID
						</th>
						<th>
							Role Name
						</th>
						<th>
							Status
						</th>
						<th>
							&nbsp;
						</th>
					</tr>
					<?php
						foreach( $a_user_role_names_result AS $a_user_role_names_result_row ) :
					?>
					<tr>
						<td>
							<?php echo $a_user_role_names_result_row['i_urn_id']; ?>
						</td>
						<td>
							<?php echo $a_user_role_names_result_row['s_urn_name']; ?>
						</td>
						<td>
							<?php echo $a_user_role_names_result_row['s_urnsn_name']; ?>
						</td>
						<td>
							<?php
								if( isset($a_user_role_names_result_row['i_urn_id']) && !empty($a_user_role_names_result_row['i_urn_id']) &&  
									( $a_user_role_names_result_row['i_urn_id'] == 1 || $a_user_role_names_result_row['i_urn_id'] == 2 )
								):
							?>
							(Default. Important.)
							<?php
								else:
							?>
							<a href="<?php echo base_url() . 'admin/update_user_role_form/user_role_name_id/' . $a_user_role_names_result_row['i_urn_id']; ?>">
								Edit
							</a>
							<?php
								endif;
							?>
						</td>
					</tr>
					<?php
						endforeach;
					?>
					<?php
						else:
					?>
					<tr>
						<td>
							No Data
						</td>
					</tr>
					<?php
						endif;
					?>
				</table>
				<nav class="nav_pagination_cls">
					<?php
						if( isset($s_page_links_pagination) && !empty($s_page_links_pagination) )
						{
							echo $s_page_links_pagination;
						}
					?>
				</nav>
			</section>
			
			
			
			<div style="clear:both;"></div>
		</main>
		
		