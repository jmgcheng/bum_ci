
		<main>
						
						
			<?php  if( isset($s_nav_menu) && !empty($s_nav_menu) ) { echo $s_nav_menu; } ?>
			
			
			<section class="sec_maincontent_cls">
				<h2>
					User - Update Profile
				</h2>
				
				
				<form id="frm_account_update_profile" name="frm_account_update_profile" action="<?php echo base_url() . 'account/update_profile'; ?>" method="post">
				
					<table>
						<tr>
							<td>
								<label for="txt_account_update_profile_email">Email:</label>
							</td>
							<td>
								<input type="text" id="txt_account_update_profile_email" name="txt_account_update_profile_email" value="<?php echo $a_u_row_result['s_u_email']; ?>" placeholder="" />
								<?php if( isset($a_form_notice['s_txt_account_update_profile_email_error']) && !empty($a_form_notice['s_txt_account_update_profile_email_error']) ) : ?>
									<p class="p_texterror_cls"><?php echo $a_form_notice['s_txt_account_update_profile_email_error']; ?></p>
								<?php endif; ?>
							</td>
						</tr>
						<tr>
							<td>
								<label for="txt_account_update_profile_username">Username:</label>
							</td>
							<td>
								<input type="text" id="txt_account_update_profile_username" name="txt_account_update_profile_username" value="<?php echo $a_u_row_result['s_u_username']; ?>" placeholder="Enter your Username" />
								<?php if( isset($a_form_notice['s_txt_account_update_profile_username_error']) && !empty($a_form_notice['s_txt_account_update_profile_username_error']) ) : ?>
									<p class="p_texterror_cls"><?php echo $a_form_notice['s_txt_account_update_profile_username_error']; ?></p>
								<?php endif; ?>
							</td>
						</tr>
						
						<tr>
							<td>
								<label for="txt_account_update_profile_firstname">Firstname:</label>
							</td>
							<td>
								<input type="text" id="txt_account_update_profile_firstname" name="txt_account_update_profile_firstname" value="<?php echo $a_u_row_result['s_u_firstname']; ?>" placeholder="" />
								<?php if( isset($a_form_notice['s_txt_account_update_profile_firstname_error']) && !empty($a_form_notice['s_txt_account_update_profile_firstname_error']) ) : ?>
									<p class="p_texterror_cls"><?php echo $a_form_notice['s_txt_account_update_profile_firstname_error']; ?></p>
								<?php endif; ?>
							</td>
						</tr>
						<tr>	
							<td>
								<label for="txt_account_update_profile_lastname">Lastname:</label>
							</td>
							<td>
								<input type="text" id="txt_account_update_profile_lastname" name="txt_account_update_profile_lastname" value="<?php echo $a_u_row_result['s_u_lastname']; ?>" placeholder="" />
								<?php if( isset($a_form_notice['s_txt_account_update_profile_lastname_error']) && !empty($a_form_notice['s_txt_account_update_profile_lastname_error']) ) : ?>
									<p class="p_texterror_cls"><?php echo $a_form_notice['s_txt_account_update_profile_lastname_error']; ?></p>
								<?php endif; ?>
							</td>
						</tr>
						
						
						<tr>
							<td>
								<label for="txt_account_update_profile_new_password">New Password:</label>
							</td>
							<td>
								<input type="password" id="txt_account_update_profile_new_password" name="txt_account_update_profile_new_password" value="" />
								<?php if( isset($a_form_notice['s_txt_account_update_profile_new_password_error']) && !empty($a_form_notice['s_txt_account_update_profile_new_password_error']) ) : ?>
									<p class="p_texterror_cls"><?php echo $a_form_notice['s_txt_account_update_profile_new_password_error']; ?></p>
								<?php endif; ?>
							</td>
						</tr>
						<tr>
							<td>
								<label for="txt_account_update_profile_new_password_conf">Retype New Password:</label>
							</td>
							<td>
								<input type="password" id="txt_account_update_profile_new_password_conf" name="txt_account_update_profile_new_password_conf" value="" />
								<?php if( isset($a_form_notice['s_txt_account_update_profile_new_password_conf_error']) && !empty($a_form_notice['s_txt_account_update_profile_new_password_conf_error']) ) : ?>
									<p class="p_texterror_cls"><?php echo $a_form_notice['s_txt_account_update_profile_new_password_conf_error']; ?></p>
								<?php endif; ?>
							</td>
						</tr>
						
						
						<?php
							if( isset($s_view_site_responses) && !empty($s_view_site_responses) ):
						?>
						<tr>
							<td colspan="2">
								<?php
									echo $s_view_site_responses;
								?>
							</td>
						</tr>
						<?php
							endif;
						?>
						
						<tr>
							<td colspan="2">
								<input type="submit" value="Update">
							</td>
						</tr>
						
					</table>
				
				</form>
				
			</section>
			
			
			
			<div style="clear:both;"></div>
		</main>