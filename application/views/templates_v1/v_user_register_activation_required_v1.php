		<main>
			

			<?php  if( isset($s_nav_menu) && !empty($s_nav_menu) ) { echo $s_nav_menu; } ?>
			
			
			<section class="sec_maincontent_cls">
				<h2>
					Email Account Activation Required
				</h2>
				<p>
					You are now registered. 
					<br/>
					We recently sent you an email so you can activate your account and start using the site. 
					<br/>
					Please check your email now or later. Thanks
				</p>
			</section>
			
			
			<div style="clear:both;"></div>
			
			
		</main>