<!DOCTYPE HTML>
<html lang="en">
	<head>
		<meta charset="utf-8" />

		<?php 
			if( isset($a_current_webpage_inc_metas) && !empty($a_current_webpage_inc_metas) ):
				foreach($a_current_webpage_inc_metas as $a_current_webpage_inc_meta):
		?>
				<meta name="<?php echo $a_current_webpage_inc_meta['s_inc_meta_name'] ?>" content="<?php echo $a_current_webpage_inc_meta['s_inc_meta_content'] ?>" />
		<?php
				endforeach;
			endif;
		?>
		
		
		
		<link rel="shortcut icon" href="" />
		
		<?php 
			if( isset($a_current_webpage_inc_css) && !empty($a_current_webpage_inc_css) ):
				foreach($a_current_webpage_inc_css as $s_current_webpage_inc_css):
		?>
			<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?><?php echo $s_current_webpage_inc_css; ?>" />
		<?php 
				endforeach;
			endif;
		?>

		
		<?php 
			if( isset($a_current_webpage_inc_js_batch1) && !empty($a_current_webpage_inc_js_batch1) ):
				foreach ($a_current_webpage_inc_js_batch1 as $s_current_webpage_inc_js_batch1):
		?>
				<script type="text/javascript" src="<?php echo base_url(); ?><?php echo $s_current_webpage_inc_js_batch1; ?>"></script>
		<?php 
				endforeach;
			endif;
		?>

		<base href="<?php echo base_url(); ?>" target="_self" />
		<title><?php  if( isset($s_current_webpage_title) && !empty($s_current_webpage_title) ) { echo $s_current_webpage_title; } ?></title>
	</head>
	<body>

		<?php  if( isset($s_current_webpage_header) && !empty($s_current_webpage_header) ) { echo $s_current_webpage_header; } ?>
		<?php  if( isset($s_current_webpage_main) && !empty($s_current_webpage_main) ) { echo $s_current_webpage_main; } ?>
		<?php  if( isset($s_current_webpage_footer) && !empty($s_current_webpage_footer) ) { echo $s_current_webpage_footer; } ?>
		<?php 
			if( isset($a_current_webpage_inc_js_batch2) && !empty($a_current_webpage_inc_js_batch2) ):
				foreach ($a_current_webpage_inc_js_batch2 as $s_current_webpage_inc_js_batch2):
		?>
				<script type="text/javascript" src="<?php echo base_url(); ?><?php echo $s_current_webpage_inc_js_batch2; ?>"></script>
		<?php 
				endforeach;
			endif;
		?>		
		
	</body>
</html>