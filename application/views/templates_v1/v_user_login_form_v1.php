
		<main>
			
			
			<?php  if( isset($s_nav_menu) && !empty($s_nav_menu) ) { echo $s_nav_menu; } ?>
			
			
			<section class="sec_maincontent_cls">
				<header>
					<h2>
						Login Form
					</h2>
				</header>
				<form id="frm_user_login" name="frm_user_login" action="<?php echo base_url() . 'user/login'; ?>" method="post">
					<table>
						<tr>
							<td>
								<label for="txt_user_login_emailorusername">Email or Username:</label>
							</td>
							<td>
								<input type="text" id="txt_user_login_emailorusername" name="txt_user_login_emailorusername" value="<?php echo set_value('txt_user_login_emailorusername'); ?>" />
								<?php if( isset($a_form_notice['s_txt_user_login_emailorusername_error']) && !empty($a_form_notice['s_txt_user_login_emailorusername_error']) ) : ?>
									<p class="p_texterror_cls"><?php echo $a_form_notice['s_txt_user_login_emailorusername_error']; ?></p>
								<?php endif; ?>	
							</td>
						</tr>
						<tr>
							<td>
								<label for="txt_user_login_password">Password:</label>
							</td>
							<td>
								<input type="password" id="txt_user_login_password" name="txt_user_login_password" />
								<?php if( isset($a_form_notice['s_txt_user_login_password_error']) && !empty($a_form_notice['s_txt_user_login_password_error']) ) : ?>
									<p class="p_texterror_cls"><?php echo $a_form_notice['s_txt_user_login_password_error']; ?></p>
								<?php endif; ?>
							</td>
						</tr>
						
						<tr>
							<td colspan="2">
								<input type="checkbox" name="chk_user_login_remember_me" value="remember" /> Remember Me?
							</td>
						</tr>
						
						
						<?php
							if( isset($s_view_site_responses) && !empty($s_view_site_responses) ):
						?>
						<tr>
							<td colspan="2">
								<?php
									echo $s_view_site_responses;
								?>
							</td>
						</tr>
						<?php
							endif;
						?>
						
						
						<tr>
							<td colspan="2">
								<input type="submit" value="Login">
							</td>
						</tr>
						<tr>
							<td colspan="2">
								<a href="<?php echo base_url() . 'user/request_forgot_password_form'; ?>">
									Forgot Password
								</a>
							</td>
						</tr>
					</table>
				</form>
			</section>
			
			
			
			<div style="clear:both;"></div>
		</main>
		
		
	