
		<main>
			
			
			<?php  if( isset($s_nav_menu) && !empty($s_nav_menu) ) { echo $s_nav_menu; } ?>
			
			
			
			<section class="sec_maincontent_cls">
				<header>
					<h2>
						Request Forgot Password Form
					</h2>
				</header>
				<form id="frm_user_request_forgot_password" name="frm_user_request_forgot_password" action="<?php echo base_url() . 'user/request_forgot_password'; ?>" method="post">
					<table>
						<tr>
							<td>
								<label for="">Email or Username:</label>
							</td>
							<td>
								<input type="text" id="txt_user_request_forgot_password_emailousername" name="txt_user_request_forgot_password_emailousername" value="<?php echo set_value('txt_user_request_forgot_password_emailousername'); ?>" placeholder="" />
								<?php if( isset($a_form_notice['s_txt_user_request_forgot_password_emailousername_error']) && !empty($a_form_notice['s_txt_user_request_forgot_password_emailousername_error']) ) : ?>
									<p class="p_texterror_cls"><?php echo $a_form_notice['s_txt_user_request_forgot_password_emailousername_error']; ?></p>
								<?php endif; ?>
							</td>
						</tr>
						
						<?php
							if( isset($s_view_site_responses) && !empty($s_view_site_responses) ):
						?>
						<tr>
							<td colspan="2">
								<?php
									echo $s_view_site_responses;
								?>
							</td>
						</tr>
						<?php
							endif;
						?>
						
						<tr>
							<td colspan="2">
								<input type="submit" value="Request">
							</td>
						</tr>
						
					</table>
				</form>
			</section>
			
			
			
			<div style="clear:both;"></div>
		</main>
		
	