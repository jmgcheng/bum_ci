		<main>
			
			
			<?php  if( isset($s_nav_menu) && !empty($s_nav_menu) ) { echo $s_nav_menu; } ?>
			
			
			<section class="sec_maincontent_cls">
				<header>
					<h2>
						Register Form
					</h2>
				</header>
				<form id="frm_user_registration" name="frm_user_registration" action="<?php echo base_url() . 'user/register'; ?>" method="post">
					<table>
						<tr>
							<td>
								<label for="txt_user_registration_email">Email:</label>
							</td>
							<td>
								<input type="text" id="txt_user_registration_email" name="txt_user_registration_email" value="<?php echo set_value('txt_user_registration_email'); ?>" placeholder="" />
								<?php if( isset($a_form_notice['s_txt_user_registration_email_error']) && !empty($a_form_notice['s_txt_user_registration_email_error']) ) : ?>
									<p class="p_texterror_cls"><?php echo $a_form_notice['s_txt_user_registration_email_error']; ?></p>
								<?php endif; ?>
							</td>
						</tr>
						<tr>
							<td>
								<label for="txt_user_registration_username">Username:</label>
							</td>
							<td>
								<input type="text" id="txt_user_registration_username" name="txt_user_registration_username" value="<?php echo set_value('txt_user_registration_username'); ?>" placeholder="" />
								<?php if( isset($a_form_notice['s_txt_user_registration_username_error']) && !empty($a_form_notice['s_txt_user_registration_username_error']) ) : ?>
									<p class="p_texterror_cls"><?php echo $a_form_notice['s_txt_user_registration_username_error']; ?></p>
								<?php endif; ?>
							</td>
						</tr>
						<tr>
							<td>
								<label for="txt_user_registration_password">Password:</label>
							</td>
							<td>
								<input type="password" id="txt_user_registration_password" name="txt_user_registration_password" />
								<?php if( isset($a_form_notice['s_txt_user_registration_password_error']) && !empty($a_form_notice['s_txt_user_registration_password_error']) ) : ?>
									<p class="p_texterror_cls"><?php echo $a_form_notice['s_txt_user_registration_password_error']; ?></p>
								<?php endif; ?>	
							</td>
						</tr>
						<tr>
							<td>
								<label for="txt_user_registration_password_conf">Retype Password:</label>
							</td>
							<td>
								<input type="password" id="txt_user_registration_password_conf" name="txt_user_registration_password_conf" value="" />
								<?php if( isset($a_form_notice['s_txt_user_registration_password_conf_error']) && !empty($a_form_notice['s_txt_user_registration_password_conf_error']) ) : ?>
									<p class="p_texterror_cls"><?php echo $a_form_notice['s_txt_user_registration_password_conf_error']; ?></p>
								<?php endif; ?>	
							</td>
						</tr>
						<tr>
							<td>
								<label for="txt_user_registration_captcha">Captcha:</label>
								<br/>
								<span>
									<a href="#" id="anc_user_registration_update_captcha_id" title="Reload a new captcha if you can't see the current one properly.">
										Reload Captcha
									</a>
								</span>
							</td>
							<td>
								<div id="div_user_registration_captcha_image_id">
									<?php 
										if( isset($s_user_registration_form_captcha) && !empty($s_user_registration_form_captcha) )
										{
											echo $s_user_registration_form_captcha;
										}
									?>
								</div>
								<div>
									<input type="Text" id="txt_user_registration_captcha" name="txt_user_registration_captcha" value="" />
								</div>
								<?php if( isset($a_form_notice['s_txt_user_registration_captcha_error']) && !empty($a_form_notice['s_txt_user_registration_captcha_error']) ) : ?>
									<p class="p_texterror_cls"><?php echo $a_form_notice['s_txt_user_registration_captcha_error']; ?></p>
								<?php endif; ?>			
							</td>
						</tr>
						
						
						<?php
							if( isset($s_view_site_responses) && !empty($s_view_site_responses) ):
						?>
						<tr>
							<td colspan="2">
								<?php
									echo $s_view_site_responses;
								?>
							</td>
						</tr>
						<?php
							endif;
						?>
						
						
						<tr>
							<td colspan="2">
								<input type="submit" value="Register">
							</td>
						</tr>
						
					</table>
				</form>
			</section>
			
			
			
			<div style="clear:both;"></div>
		</main>