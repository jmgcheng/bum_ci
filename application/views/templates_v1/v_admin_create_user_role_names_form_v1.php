
		<main>
						
			
			<?php  if( isset($s_nav_menu) && !empty($s_nav_menu) ) { echo $s_nav_menu; } ?>
			
			
			<section class="sec_maincontent_cls">
				<h2>
					Admin - Create User Role Names
				</h2>
				
				<form id="frm_admin_create_user_role_names" name="frm_admin_create_user_role_names" action="<?php echo base_url() . 'admin/create_user_role'; ?>" method="post">
				
					<table>
						
						<tr>
							<td>
								<label for="">Name:</label>
							</td>
							<td>
								<input type="text" id="txt_admin_create_user_role_names_name" name="txt_admin_create_user_role_names_name" value="<?php echo set_value('txt_admin_create_user_role_names_name'); ?>" placeholder="" />
								<?php if( isset($a_form_notice['s_txt_admin_create_user_role_names_name_error']) && !empty($a_form_notice['s_txt_admin_create_user_role_names_name_error']) ) : ?>
									<p class="p_texterror_cls"><?php echo $a_form_notice['s_txt_admin_create_user_role_names_name_error']; ?></p>
								<?php endif; ?>
							</td>
						</tr>
						
						<tr>	
							<td>
								<label for="">Status:</label>
							</td>
							<td>
								<?php
									if( isset($a_user_role_name_status_names_result) && !empty($a_user_role_name_status_names_result) ):
								?>
								<select id="opt_admin_create_user_role_names_status" name="opt_admin_create_user_role_names_status">
									<?php
										foreach( $a_user_role_name_status_names_result AS $a_user_role_name_status_names_result_row ):
									?>
									<option value="<?php echo $a_user_role_name_status_names_result_row['i_urnsn_id']; ?>"><?php echo $a_user_role_name_status_names_result_row['s_urnsn_name']; ?></option>
									<?php
										endforeach;
									?>
								</select>
								<?php
									endif;
								?>
								<?php if( isset($a_form_notice['s_opt_admin_create_user_role_names_status_error']) && !empty($a_form_notice['s_opt_admin_create_user_role_names_status_error']) ) : ?>
									<p class="p_texterror_cls"><?php echo $a_form_notice['s_opt_admin_create_user_role_names_status_error']; ?></p>
								<?php endif; ?>
							</td>
						</tr>
						
						<?php
							if( isset($s_view_site_responses) && !empty($s_view_site_responses) ):
						?>
						<tr>
							<td colspan="2">
								<?php
									echo $s_view_site_responses;
								?>
							</td>
						</tr>
						<?php
							endif;
						?>
						
						<tr>
							<td colspan="2">
								<input type="submit" value="Create">
							</td>
						</tr>
						
					</table>
				
				</form>
				
			</section>
			
			
			
			<div style="clear:both;"></div>
		</main>
		
		
	