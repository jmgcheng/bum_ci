<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class C_notice extends CI_Controller {
	
	/**
	* contructor
	* @desc		contructor for 
	*
	**/
	public function __construct()
	{
		parent::__construct();
		
	}
	

	/**
	* index
	* @desc		
	*
	**/
	public function index()
	{
		//= Declare Start-Up Variables Here ====================
		$a_current_webpage_inc_metas = array();
		$a_current_webpage_inc_css = array();
		$a_current_webpage_inc_js_batch1 = array();
		$a_current_webpage_inc_js_batch2 = array();

		$s_view_header = '';
		$s_view_main = '';
		$s_view_footer = '';
		
		$a_site_response_info = array();
		$a_site_response_error = array();
		$a_site_response_success = array();
		
		$a_notices = array();
		//======================================================
		
		
		//======================================================
		/*
			parse uri first
		*/
		$a_expected_uri = array('notice');
		$a_assoc_uri = $this->uri->uri_to_assoc(1, $a_expected_uri);
		
		if( !isset($a_assoc_uri['notice']) || empty($a_assoc_uri['notice']) )
		{ redirect( base_url(), 'refresh'); }
		
		//
		$s_notice_query = $a_assoc_uri['notice'];
		$s_notice_query = base64_decode($s_notice_query);
		$s_notice_query = $this->encrypt->decode($s_notice_query);
		
		if( !isset($s_notice_query) || empty($s_notice_query) )
		{ redirect( base_url(), 'refresh'); }
		
		//
		$a_notice_params = array();
		parse_str($s_notice_query, $a_notice_params);
		
		if( !isset($a_notice_params['message']) || empty($a_notice_params['message']) )
		{ redirect( base_url(), 'refresh'); }
		
		
		//
		if( 	isset($a_notice_params['message_status']) && !empty($a_notice_params['message_status']) 
			&&	$a_notice_params['message_status'] == 'success'
		)
		{
			array_push( $a_site_response_success, $a_notice_params['message'] );
		}
		elseif( 	isset($a_notice_params['message_status']) && !empty($a_notice_params['message_status']) 
				&&	$a_notice_params['message_status'] == 'error'
		)
		{
			array_push( $a_site_response_error, $a_notice_params['message'] );
		}
		else
		{
			array_push( $a_site_response_info, $a_notice_params['message'] );
		}
		
		$a_notices['a_site_response_info'] = $a_site_response_info;
		$a_notices['a_site_response_error'] = $a_site_response_error;
		$a_notices['a_site_response_success'] = $a_site_response_success;
		
		//======================================================
		
		
		
		
		//======================================================
		
		$a_header_data = array();
		$s_view_header = $this->load->view('templates_v1/v_header_v1', $a_header_data, true);
		
		$a_nav_menu_data = array();
		$s_view_nav_menu = $this->load->view('templates_v1/v_nav_menu_v1', $a_nav_menu_data, true);
		
		$s_view_site_responses_data = array();
		$s_view_site_responses_data['a_form_notice'] = $a_notices;
		$s_view_site_responses = $this->load->view('templates_v1/v_site_responses', $s_view_site_responses_data, true);
		
		$a_main_data = array();
		$a_main_data['s_nav_menu'] = $s_view_nav_menu;
		$a_main_data['s_view_site_responses'] = $s_view_site_responses;
		$s_view_main = $this->load->view('templates_v1/v_notice_v1', $a_main_data, true);
		
		
		$s_view_footer_data = array();
		$s_view_footer = $this->load->view('templates_v1/v_footer_v1', $s_view_footer_data, true);
		
		
		//======================================================
		
		
		//======================================================
		array_push($a_current_webpage_inc_css, 'css/css_reset.css');
		array_push($a_current_webpage_inc_css, 'css/css_style.css');

		array_push($a_current_webpage_inc_js_batch1, 'js/jquery-1.11.1.js');
		array_push($a_current_webpage_inc_js_batch1, 'js/js_detect_browser.js');

		$a_current_webpage_data['a_current_webpage_inc_metas'] = $a_current_webpage_inc_metas;
		$a_current_webpage_data['a_current_webpage_inc_css'] = $a_current_webpage_inc_css;
		$a_current_webpage_data['a_current_webpage_inc_js_batch1'] = $a_current_webpage_inc_js_batch1;
		$a_current_webpage_data['a_current_webpage_inc_js_batch2'] = $a_current_webpage_inc_js_batch2;
		$a_current_webpage_data['s_current_webpage_title'] = SITE_NAME;

		$a_current_webpage_data['s_current_webpage_header'] = $s_view_header;
		$a_current_webpage_data['s_current_webpage_main'] = $s_view_main;
		$a_current_webpage_data['s_current_webpage_footer'] = $s_view_footer;
		$s_current_webpage = $this->load->view('templates_v1/v_html_template_v1', $a_current_webpage_data, true);
		//======================================================

		
		//======================================================
		$this->output->set_output($s_current_webpage);
		//======================================================
	}
}

/* End of file */
/* Location: ./application/controllers/ */