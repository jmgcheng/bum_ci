<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class C_admin extends CI_Controller {
	
	/**
	* contructor
	* @desc		contructor for 
	*
	**/
	public function __construct()
	{
		parent::__construct();
		
		/*
		*/
		$this->load->library('l_user');
		$this->l_user->autologin();
	}
	
	
	/**
	* is_existing
	* @desc		addon callback function for form validation. Check if data is existing.
				Opposite to what is_unique[...] is used
	*
	**/
	public function is_existing( $m_var, $s_table_detail )
	{
		//= Declare Start-Up Variables Here ====================
		$b_result = FALSE;
		
		//======================================================
		
		
		//======================================================
		$this->load->library('l_def');
	
		//======================================================
		
		
		//======================================================
		
		$b_result = $this->l_def->is_existing( $m_var, $s_table_detail );
		
		return $b_result;
		//======================================================
	}
	

	/**
	* index
	* @desc		
	*
	**/
	public function index()
	{
		
	}
	
	
	/**
	* read_all_user
	* @desc		
	*
	**/
	public function read_all_user()
	{
		//= Declare Start-Up Variables Here ====================
		$a_current_webpage_inc_metas = array();
		$a_current_webpage_inc_css = array();
		$a_current_webpage_inc_js_batch1 = array();
		$a_current_webpage_inc_js_batch2 = array();

		$s_view_header = '';
		$s_view_main = '';
		$s_view_footer = '';
		//======================================================
		
		
		//======================================================
		$this->load->library(array('pagination'));
		$this->load->library(array('l_user'));
		$this->load->library('l_def_sql');
		
		$this->load->model('m_def_table_fields');

		//======================================================
		
		
		//======================================================
		$a_user_details = $this->session->userdata('a_user_details');
		$a_user_roles_result = $this->session->userdata('a_user_roles_result');
		if( !isset($a_user_details) || empty($a_user_details) )
		{ redirect( base_url() . 'user/logout', 'refresh'); }
		if( array_key_exists( 1, $a_user_roles_result ) )
		{}
		else
		{ 
			$s_notice = '';
			$s_notice_query = '';
			$a_notice_params = array();
			$a_notice_params['message'] = 'Access Denied';
			$a_notice_params['subject'] = '';
			$a_notice_params['message_status'] = 'error';
			$s_notice_query = http_build_query($a_notice_params);
			$s_notice_query = $this->encrypt->encode($s_notice_query);
			$s_notice_query = base64_encode($s_notice_query);
			redirect( base_url() . 'notice/' . $s_notice_query, 'refresh');
		}
		
		
		/*
			parse uri first
		*/
		$a_expected_uri = array('limit', 'offset', 'sort', 'order');  
		$a_assoc_uri = $this->uri->uri_to_assoc(3, $a_expected_uri);
		
		
		/*
			check if filter is involved
		*/
		if( isset($_POST['opt_filter_admin_read_all_users_status']) && !empty($_POST['opt_filter_admin_read_all_users_status']) )
		{ 
			$a_assoc_uri['status'] = $_POST['opt_filter_admin_read_all_users_status']; 
		}
		if( isset($_POST['opt_filter_admin_read_all_users_sort']) && !empty($_POST['opt_filter_admin_read_all_users_sort']) )
		{ 
			$a_assoc_uri['sort'] = $_POST['opt_filter_admin_read_all_users_sort']; 
		}
		if( isset($_POST['opt_filter_admin_read_all_users_order']) && !empty($_POST['opt_filter_admin_read_all_users_order']) )
		{ 
			$a_assoc_uri['order'] = $_POST['opt_filter_admin_read_all_users_order']; 
		}
		if( isset($_POST['txt_filter_admin_read_all_users_limit']) && !empty($_POST['txt_filter_admin_read_all_users_limit']) )
		{ 
			$a_assoc_uri['limit'] = $_POST['txt_filter_admin_read_all_users_limit']; 
		}
		
		
		/*
			ready assoc before using sql
		*/
		$a_expected_sort = array('i_id'=>'ID', 's_date_registration'=>'Date Registered', 's_email'=>'Email', 's_username'=>'Username', 's_firstname'=>'Firstname', 's_lastname'=>'Lastname', 'i_usn_id'=>'Status');
		$a_expected_order = array('asc'=>'ASC', 'desc'=>'DESC');
		if( !isset($a_assoc_uri['limit']) || empty($a_assoc_uri['limit']) )
		{ $a_assoc_uri['limit'] = 10; }
		else
		{
			if( !is_numeric($a_assoc_uri['limit']) || $a_assoc_uri['limit'] < 1 )
			{ $a_assoc_uri['limit'] = 10; }
		}
		if( !isset($a_assoc_uri['offset']) || empty($a_assoc_uri['offset']) )
		{ $a_assoc_uri['offset'] = 0; }
		else
		{
			if( !is_numeric($a_assoc_uri['offset']) || $a_assoc_uri['offset'] < 0 )
			{ $a_assoc_uri['offset'] = 0; }
		}
		
		if( !isset($a_assoc_uri['sort']) || empty($a_assoc_uri['sort']) )
		{ $a_assoc_uri['sort'] = 's_username'; }
		else
		{
			if( !array_key_exists($a_assoc_uri['sort'], $a_expected_sort) )
			{ $a_assoc_uri['sort'] = 's_username'; }
		}
		if( !isset($a_assoc_uri['order']) || empty($a_assoc_uri['order']) )
		{ $a_assoc_uri['order'] = 'asc'; }
		else
		{
			if( !array_key_exists($a_assoc_uri['order'], $a_expected_order) )
			{ $a_assoc_uri['order'] = 'asc'; }
		}
		if( !isset($a_assoc_uri['status']) || empty($a_assoc_uri['status']) )
		{ $a_assoc_uri['status'] = 0; }
		
		
		/*
			Get user_status_names
		*/
		$a_user_status_names_result = array();
		$a_user_status_names_query_where = array();
		$a_user_status_names_query_where_in = array();
		$a_user_status_names_query_table_join = array();
		$a_user_status_names_query_params = array();
		$a_user_status_names_query_params['s_table_fields'] = $this->m_def_table_fields->s_user_status_names_fields;
		$a_user_status_names_query_params['s_table_name'] = 'user_status_names';
		$a_user_status_names_result = $this->l_def_sql->read_data( $a_user_status_names_query_params );
		//reindex
		$a_user_status_names_result_temp = array();
		if( isset($a_user_status_names_result) && !empty($a_user_status_names_result) )
		{
			foreach( $a_user_status_names_result AS $a_user_status_names_result_row )
			{
				$a_user_status_names_result_temp[$a_user_status_names_result_row['i_usn_id']] = $a_user_status_names_result_row;
			}
			$a_user_status_names_result = $a_user_status_names_result_temp;
		}
		
		
		/*
			check $a_assoc_uri['status'] in array of possible values, if not set to default 0
		*/
		if( !array_key_exists($a_assoc_uri['status'], $a_user_status_names_result) )
		{
			$a_assoc_uri['status'] = 0;
		}
		
		
		/*
		*/
		$a_u_query_where = array();
		$a_u_query_table_join = array();
		$a_u_query_order_by = array();
		$a_u_query_limit = array();
		$a_u_query_params = array();
		if( isset( $a_assoc_uri['status'] ) && !empty( $a_assoc_uri['status'] ) )
		{
			array_push( $a_u_query_where, array( 's_field' => 'users.i_usn_id', 'a_data' => $a_assoc_uri['status'] ) );
		}
		array_push( $a_u_query_table_join, array( 's_table_join_name' => 'user_status_names', 's_table_join_condition' => 'user_status_names.i_id = users.i_usn_id' ) );
		array_push( $a_u_query_order_by, array( 's_field' => $a_assoc_uri['sort'], 'a_data' => $a_assoc_uri['order'] ) );
		$a_u_query_limit = array('i_limit' => $a_assoc_uri['limit'], 'i_offset' => $a_assoc_uri['offset']);
		$a_u_query_params['a_where'] = $a_u_query_where;
		$a_u_query_params['a_order_by'] = $a_u_query_order_by;
		$a_u_query_params['a_limit'] = $a_u_query_limit;
		$a_u_query_params['a_table_join'] = $a_u_query_table_join;
		$a_u_query_params['s_table_fields'] = $this->m_def_table_fields->s_users_fields . ', ' . $this->m_def_table_fields->s_user_status_names_fields;
		$a_u_query_params['s_table_name'] = 'users';
		$a_user_result = $this->l_def_sql->read_data( $a_u_query_params );
		$a_u_query_params['b_count'] = true;
		$a_user_count_result = $this->l_def_sql->read_data( $a_u_query_params );
		
		
		/*
			Get user id showing in this current paging
		*/
		$a_showing_u_id = array();
		foreach( $a_user_result AS $a_user_result_row )
		{
			array_push( $a_showing_u_id, $a_user_result_row['i_u_id'] );
		}
		
		
		/*
			Get user_roles
		*/
		if( isset($a_showing_u_id) && !empty($a_showing_u_id) )
		{
			$a_user_roles_query_where = array();
			$a_user_roles_query_where_in = array();
			$a_user_roles_query_table_join = array();
			$a_user_roles_query_params = array();
			array_push( $a_user_roles_query_where_in, array( 's_field' => 'user_roles.i_u_id', 'a_data' => $a_showing_u_id ) );
			array_push( $a_user_roles_query_table_join, array( 's_table_join_name' => 'user_role_names', 's_table_join_condition' => 'user_role_names.i_id = user_roles.i_urn_id' ) );
			$a_user_roles_query_params['a_where_in'] = $a_user_roles_query_where_in;
			$a_user_roles_query_params['a_table_join'] = $a_user_roles_query_table_join;
			$a_user_roles_query_params['s_table_fields'] = $this->m_def_table_fields->s_user_roles_fields . ', ' . $this->m_def_table_fields->s_user_role_names_fields;
			$a_user_roles_query_params['s_table_name'] = 'user_roles';
			$a_user_roles_result = $this->l_def_sql->read_data( $a_user_roles_query_params );
		}
		
		
		/*
			ready each showing user of their user_roles
		*/
		$a_showing_u_roles = array();
		if( isset($a_user_roles_result) && !empty($a_user_roles_result) )
		{
			foreach( $a_user_roles_result AS $a_user_roles_result_row )
			{
				if( isset($a_showing_u_roles[$a_user_roles_result_row['i_ur_user_id']]) && !empty($a_showing_u_roles[$a_user_roles_result_row['i_ur_user_id']]) )
				{
					$a_showing_u_roles[$a_user_roles_result_row['i_ur_user_id']] = $a_showing_u_roles[$a_user_roles_result_row['i_ur_user_id']] . ', ' . $a_user_roles_result_row['s_urn_name'];
				}
				else
				{
					$a_showing_u_roles[$a_user_roles_result_row['i_ur_user_id']] = $a_user_roles_result_row['s_urn_name'];
				}
			}
		}
		
		
		/*
			Paging
		*/
		$i_page_uri_segment = 12;
		$a_pagination_config['base_url'] = base_url() . 'admin/read_all_user/limit/' .$a_assoc_uri['limit']. '/order/' .$a_assoc_uri['order']. '/sort/' .$a_assoc_uri['sort']. '/status/' .$a_assoc_uri['status']. '/offset/';
		$a_pagination_config['total_rows'] = $a_user_count_result['i_num_rows'];
		$a_pagination_config['per_page'] = $a_assoc_uri['limit'];
		$a_pagination_config['uri_segment'] = $i_page_uri_segment;
		$a_pagination_config['num_links'] = 5;
		$a_pagination_config['first_link'] = false;
		$a_pagination_config['last_link'] = false;
		$a_pagination_config['num_tag_open'] = '<li>';
		$a_pagination_config['num_tag_close'] = '</li>';
		$a_pagination_config['cur_tag_open'] = '<li class="clsli_pageactive_1"><a href="#" onclick="return false;">';
		$a_pagination_config['cur_tag_close'] = '</a></li>';
		$a_pagination_config['next_link'] = 'Next Page';
		$a_pagination_config['next_tag_open'] = '<li>';
		$a_pagination_config['next_tag_close'] = '</li>';
		$a_pagination_config['prev_link'] = 'Prev Page';
		$a_pagination_config['prev_tag_open'] = '<li>';
		$a_pagination_config['prev_tag_close'] = '</li>';
		$a_pagination_config['full_tag_open'] = '<ul>';
		$a_pagination_config['full_tag_close'] = '</ul>';
		$this->pagination->initialize($a_pagination_config); 
		$s_page_links_pagination = $this->pagination->create_links();
		
		
		//======================================================

		$s_view_header_data = array();
		$s_view_header = $this->load->view('templates_v1/v_header_v1', $s_view_header_data, true);
		$a_nav_menu_data = array();
		$s_view_nav_menu = $this->load->view('templates_v1/v_nav_menu_v1', $a_nav_menu_data, true);
		$s_view_main_data = array();
		$s_view_main_data['a_assoc_uri'] = $a_assoc_uri;
		$s_view_main_data['a_expected_sort'] = $a_expected_sort;
		$s_view_main_data['a_expected_order'] = $a_expected_order;
		$s_view_main_data['a_user_status_names_result'] = $a_user_status_names_result;
		$s_view_main_data['a_user_result'] = $a_user_result;
		$s_view_main_data['a_user_count_result'] = $a_user_count_result;
		$s_view_main_data['a_showing_u_roles'] = $a_showing_u_roles;
		$s_view_main_data['s_page_links_pagination'] = $s_page_links_pagination;
		$s_view_main_data['s_nav_menu'] = $s_view_nav_menu;
		$s_view_main = $this->load->view('templates_v1/v_admin_read_all_users_v1', $s_view_main_data, true);
		
		$s_view_footer_data = array();
		$s_view_footer = $this->load->view('templates_v1/v_footer_v1', $s_view_footer_data, true);
		
		//======================================================

		
		//======================================================

		array_push($a_current_webpage_inc_css, 'css/css_reset.css');
		array_push($a_current_webpage_inc_css, 'css/css_style.css');

		array_push($a_current_webpage_inc_js_batch1, 'js/jquery-1.11.1.js');
		array_push($a_current_webpage_inc_js_batch1, 'js/js_detect_browser.js');

		$a_current_webpage_data['a_current_webpage_inc_metas'] = $a_current_webpage_inc_metas;
		$a_current_webpage_data['a_current_webpage_inc_css'] = $a_current_webpage_inc_css;
		$a_current_webpage_data['a_current_webpage_inc_js_batch1'] = $a_current_webpage_inc_js_batch1;
		$a_current_webpage_data['a_current_webpage_inc_js_batch2'] = $a_current_webpage_inc_js_batch2;
		$a_current_webpage_data['s_current_webpage_title'] = SITE_NAME;

		$a_current_webpage_data['s_current_webpage_header'] = $s_view_header;
		$a_current_webpage_data['s_current_webpage_main'] = $s_view_main;
		$a_current_webpage_data['s_current_webpage_footer'] = $s_view_footer;
		$s_current_webpage = $this->load->view('templates_v1/v_html_template_v1', $a_current_webpage_data, true);
		//======================================================

		
		//======================================================
		$this->output->set_output($s_current_webpage);
		//======================================================
	}
	
	
	/**
	* create_user_form
	* @desc		
	*
	**/
	public function create_user_form( $a_form_notice = array() )
	{
		//= Declare Start-Up Variables Here ====================
		$a_current_webpage_inc_metas = array();
		$a_current_webpage_inc_css = array();
		$a_current_webpage_inc_js_batch1 = array();
		$a_current_webpage_inc_js_batch2 = array();
		
		$s_view_header = '';
		$s_view_main = '';
		$s_view_footer = '';
		
		$s_view_site_responses = '';
		//======================================================
		
		
		//======================================================
		$this->load->library('form_validation');
		$this->load->helper(array('captcha','form'));
		//======================================================
		
		
		//======================================================
		$a_user_details = $this->session->userdata('a_user_details');
		$a_user_roles_result = $this->session->userdata('a_user_roles_result');
		if( !isset($a_user_details) || empty($a_user_details) )
		{ redirect( base_url() . 'user/logout', 'refresh'); }
		if( array_key_exists( 1, $a_user_roles_result ) )
		{}
		else
		{ 
			$s_notice = '';
			$s_notice_query = '';
			$a_notice_params = array();
			$a_notice_params['message'] = 'Access Denied';
			$a_notice_params['subject'] = '';
			$a_notice_params['message_status'] = 'error';
			$s_notice_query = http_build_query($a_notice_params);
			$s_notice_query = $this->encrypt->encode($s_notice_query);
			$s_notice_query = base64_encode($s_notice_query);
			redirect( base_url() . 'notice/' . $s_notice_query, 'refresh');
		}
		
		
		/*
			Get user_role_names
		*/
		$a_user_role_names_query_where = array();
		$a_user_role_names_query_params = array();
		array_push( $a_user_role_names_query_where, array( 's_field' => 'user_role_names.i_urnsn_id', 'a_data' => 1 ) );
		$a_user_role_names_query_params['a_where'] = $a_user_role_names_query_where;
		$a_user_role_names_query_params['s_table_fields'] = $this->m_def_table_fields->s_user_role_names_fields;
		$a_user_role_names_query_params['s_table_name'] = 'user_role_names';
		$a_user_role_names_result = $this->l_def_sql->read_data( $a_user_role_names_query_params );
		
		
		/*
			Get user_status_names
		*/
		$a_user_status_names_query_where = array();
		$a_user_status_names_query_params = array();
		$a_user_status_names_query_params['s_table_fields'] = $this->m_def_table_fields->s_user_status_names_fields;
		$a_user_status_names_query_params['s_table_name'] = 'user_status_names';
		$a_user_status_names_result = $this->l_def_sql->read_data( $a_user_status_names_query_params );
		
		
		//======================================================
		
		
		$s_view_site_responses_data = array();
		$s_view_site_responses_data['a_form_notice'] = $a_form_notice;
		$s_view_site_responses = $this->load->view('templates_v1/v_site_responses', $s_view_site_responses_data, true);
		
		
		$s_view_header_data = array();
		$s_view_header = $this->load->view('templates_v1/v_header_v1', $s_view_header_data, true);
		
		$a_nav_menu_data = array();
		$s_view_nav_menu = $this->load->view('templates_v1/v_nav_menu_v1', $a_nav_menu_data, true);
		
		$s_view_main_data = array();
		$s_view_main_data['s_nav_menu'] = $s_view_nav_menu;
		$s_view_main_data['a_form_notice'] = $a_form_notice;
		$s_view_main_data['s_view_site_responses'] = $s_view_site_responses;
		$s_view_main_data['a_user_role_names_result'] = $a_user_role_names_result;
		$s_view_main_data['a_user_status_names_result'] = $a_user_status_names_result;
		$s_view_main = $this->load->view('templates_v1/v_admin_create_user_form_v1', $s_view_main_data, true);
		
		$s_view_footer_data = array();
		$s_view_footer = $this->load->view('templates_v1/v_footer_v1', $s_view_footer_data, true);
		
		//======================================================
		
		
		//======================================================
		array_push($a_current_webpage_inc_css, 'css/css_reset.css');
		array_push($a_current_webpage_inc_css, 'css/css_style.css');

		array_push($a_current_webpage_inc_js_batch1, 'js/jquery-1.11.1.js');
		array_push($a_current_webpage_inc_js_batch1, 'js/js_detect_browser.js');

		$a_current_webpage_data['a_current_webpage_inc_metas'] = $a_current_webpage_inc_metas;
		$a_current_webpage_data['a_current_webpage_inc_css'] = $a_current_webpage_inc_css;
		$a_current_webpage_data['a_current_webpage_inc_js_batch1'] = $a_current_webpage_inc_js_batch1;
		$a_current_webpage_data['a_current_webpage_inc_js_batch2'] = $a_current_webpage_inc_js_batch2;
		$a_current_webpage_data['s_current_webpage_title'] = SITE_NAME;

		$a_current_webpage_data['s_current_webpage_header'] = $s_view_header;
		$a_current_webpage_data['s_current_webpage_main'] = $s_view_main;
		$a_current_webpage_data['s_current_webpage_footer'] = $s_view_footer;
		$s_current_webpage = $this->load->view('templates_v1/v_html_template_v1', $a_current_webpage_data, true);
		//======================================================

		
		//======================================================
		$this->output->set_output($s_current_webpage);
		//======================================================
	}
	
	
	/**
	* create_user
	* @desc		
	*
	**/
	public function create_user()
	{
		//= Declare Start-Up Variables Here ====================
		$a_form_notice = array();
		$a_site_response_error = array();
		$a_site_response_info = array();
		
		$b_user_role_names_problem = false;
		//======================================================
		
		
		//======================================================
		$this->load->library('form_validation');
		$this->load->library('l_def_sql');
		$this->load->library('l_user');
		
		$this->load->helper(array('captcha','form'));
		//======================================================
		
		
		//======================================================
		$a_user_details = $this->session->userdata('a_user_details');
		$a_user_roles_result = $this->session->userdata('a_user_roles_result');
		if( !isset($a_user_details) || empty($a_user_details) )
		{ redirect( base_url() . 'user/logout', 'refresh'); }
		if( array_key_exists( 1, $a_user_roles_result ) )
		{}
		else
		{ 
			$s_notice = '';
			$s_notice_query = '';
			$a_notice_params = array();
			$a_notice_params['message'] = 'Access Denied';
			$a_notice_params['subject'] = '';
			$a_notice_params['message_status'] = 'error';
			$s_notice_query = http_build_query($a_notice_params);
			$s_notice_query = $this->encrypt->encode($s_notice_query);
			$s_notice_query = base64_encode($s_notice_query);
			redirect( base_url() . 'notice/' . $s_notice_query, 'refresh');
		}
		
		
		if( isset($_POST) && !empty($_POST) )
		{
			$this->form_validation->set_rules('txt_admin_create_user_username', 'Username', 'trim|required|min_length[5]|max_length[12]|xss_clean|is_unique[users.s_username]');
			$this->form_validation->set_rules('txt_admin_create_user_email', 'Email', 'trim|required|valid_email|xss_clean|is_unique[users.s_email]');
			$this->form_validation->set_rules('txt_admin_create_user_password', 'Password', 'trim|required|matches[txt_admin_create_user_password_conf]|md5|xss_clean');
			$this->form_validation->set_rules('txt_admin_create_user_password_conf', 'Password Confirmation', 'trim|required|xss_clean');
			$this->form_validation->set_rules('txt_admin_create_user_firstname', 'Firstname', 'trim|max_length[55]|xss_clean');
			$this->form_validation->set_rules('txt_admin_create_user_lastname', 'Lastname', 'trim|max_length[55]|xss_clean');
			$this->form_validation->set_rules('opt_admin_create_user_status', 'Status', 'trim|required|max_length[12]|xss_clean|callback_is_existing[user_status_names.i_id]');
			
			/*
				check if roles selected is existing
			*/
			if( isset($_POST['chk_admin_create_user_roles']) && !empty($_POST['chk_admin_create_user_roles']) )
			{
				$a_user_role_names_query_where = array();
				$a_user_role_names_query_where_in = array();
				$a_user_role_names_query_table_join = array();
				$a_user_role_names_query_params = array();
				array_push( $a_user_role_names_query_where_in, array( 's_field' => 'user_role_names.i_id', 'a_data' => $_POST['chk_admin_create_user_roles'] ) );
				$a_user_role_names_query_params['a_where_in'] = $a_user_role_names_query_where_in;
				$a_user_role_names_query_params['s_table_fields'] = $this->m_def_table_fields->s_user_role_names_fields;
				$a_user_role_names_query_params['s_table_name'] = 'user_role_names';
				$a_user_role_names_result = $this->l_def_sql->read_data( $a_user_role_names_query_params );

				if( isset($a_user_role_names_result) && !empty($a_user_role_names_result) )
				{
					if( count($a_user_role_names_result) != count($_POST['chk_admin_create_user_roles']) )
					{
						$b_user_role_names_problem = true;
					}
				}
				else
				{
					$b_user_role_names_problem = true;
				}
			}
			
			
			if(		$this->form_validation->run() == FALSE
				||	$b_user_role_names_problem == TRUE
			)
			{
				if( $b_user_role_names_problem == TRUE )
				{
					array_push( $a_site_response_error, 'User Roles Problems' );
				}
				
				$a_form_notice['s_txt_admin_create_user_username_error'] = form_error('txt_admin_create_user_username', ' ', ' ');
				if( isset($a_form_notice['s_txt_admin_create_user_username_error']) && !empty($a_form_notice['s_txt_admin_create_user_username_error']) )
				{
					array_push( $a_site_response_error, $a_form_notice['s_txt_admin_create_user_username_error'] );
				}
				$a_form_notice['s_txt_admin_create_user_email_error'] = form_error('txt_admin_create_user_email', ' ', ' ');
				if( isset($a_form_notice['s_txt_admin_create_user_email_error']) && !empty($a_form_notice['s_txt_admin_create_user_email_error']) )
				{
					array_push( $a_site_response_error, $a_form_notice['s_txt_admin_create_user_email_error'] );
				}
				$a_form_notice['s_txt_admin_create_user_password_error'] = form_error('txt_admin_create_user_password', ' ', ' ');
				if( isset($a_form_notice['s_txt_admin_create_user_password_error']) && !empty($a_form_notice['s_txt_admin_create_user_password_error']) )
				{
					array_push( $a_site_response_error, $a_form_notice['s_txt_admin_create_user_password_error'] );
				}
				$a_form_notice['s_txt_admin_create_user_password_conf_error'] = form_error('txt_admin_create_user_password_conf', ' ', ' ');
				if( isset($a_form_notice['s_txt_admin_create_user_password_conf_error']) && !empty($a_form_notice['s_txt_admin_create_user_password_conf_error']) )
				{
					array_push( $a_site_response_error, $a_form_notice['s_txt_admin_create_user_password_conf_error'] );
				}
				$a_form_notice['s_txt_admin_create_user_firstname_error'] = form_error('txt_admin_create_user_firstname', ' ', ' ');
				if( isset($a_form_notice['s_txt_admin_create_user_firstname_error']) && !empty($a_form_notice['s_txt_admin_create_user_firstname_error']) )
				{
					array_push( $a_site_response_error, $a_form_notice['s_txt_admin_create_user_firstname_error'] );
				}
				$a_form_notice['s_txt_admin_create_user_lastname_error'] = form_error('txt_admin_create_user_lastname', ' ', ' ');
				if( isset($a_form_notice['s_txt_admin_create_user_lastname_error']) && !empty($a_form_notice['s_txt_admin_create_user_lastname_error']) )
				{
					array_push( $a_site_response_error, $a_form_notice['s_txt_admin_create_user_lastname_error'] );
				}
				$a_form_notice['s_opt_admin_create_user_status_error'] = form_error('opt_admin_create_user_status', ' ', ' ');
				if( isset($a_form_notice['s_opt_admin_create_user_status_error']) && !empty($a_form_notice['s_opt_admin_create_user_status_error']) )
				{
					array_push( $a_site_response_error, $a_form_notice['s_opt_admin_create_user_status_error'] );
				}

				
				$a_form_notice['a_site_response_error'] = $a_site_response_error;

				$this->create_user_form($a_form_notice);
			}
			else
			{
				$s_unique_key = md5( ( strtotime('now') . $_POST['txt_admin_create_user_username'] ) );
				
				$a_new_data = array();
				$a_new_data['s_date_registration'] = date('Y-m-d H:i:s');
				$a_new_data['s_email'] = $_POST['txt_admin_create_user_email'];
				$a_new_data['s_username'] = $_POST['txt_admin_create_user_username']; 
				$a_new_data['s_password'] = $_POST['txt_admin_create_user_password'];
				$a_new_data['s_firstname'] = $_POST['txt_admin_create_user_firstname'];
				$a_new_data['s_lastname'] = $_POST['txt_admin_create_user_lastname'];
				$a_new_data['s_unique_key'] = $s_unique_key;
				$a_new_data['i_usn_id'] = $_POST['opt_admin_create_user_status'];
				
				$a_add_user_result = array();
				$a_add_user_params['s_table_name'] = 'users';
				$a_add_user_params['a_new_data'] = $a_new_data;
				$a_add_user_result = $this->l_def_sql->create_data( $a_add_user_params );

				
				if( 	isset($a_add_user_result) && !empty($a_add_user_result) 
					&&	array_key_exists("i_sql_result", $a_add_user_result)
					&& 	$a_add_user_result["i_sql_result"] == 1 
				)
				{
					/*
						add user_roles
					*/
					if( isset($_POST['chk_admin_create_user_roles']) && !empty($_POST['chk_admin_create_user_roles']) )
					{
						$a_add_user_roles_param = array();
						for( $i_counter = 0; $i_counter < count($_POST['chk_admin_create_user_roles']); $i_counter++ )
						{
							$a_add_user_roles_template = array();
							$a_add_user_roles_template['i_u_id'] = $a_add_user_result['i_insert_id'];
							$a_add_user_roles_template['i_urn_id'] = $_POST['chk_admin_create_user_roles'][$i_counter];
							
							array_push( $a_add_user_roles_param, $a_add_user_roles_template );
						}
						
						$a_add_user_roles = array();
						$a_add_user_roles['a_new_data'] = $a_add_user_roles_param;
						$a_add_user_roles['s_table_name'] = 'user_roles';
						$a_add_user_roles_result = $this->l_def_sql->create_batch_data( $a_add_user_roles );
						
						if( 	isset($a_add_user_roles_result) && !empty($a_add_user_roles_result) 
								&&	array_key_exists("i_sql_result", $a_add_user_roles_result)
								&& 	$a_add_user_roles_result["i_sql_result"] == 1 
						)
						{ }
						else
						{	
							$a_result['s_result'] = 'fail';
							array_push( $a_site_response_error, 'User Roles not Saved' );
						}	
					}
					
					
					array_push( $a_site_response_info, 'User Create' );
					$a_form_notice['a_site_response_info'] = $a_site_response_info;
					$this->create_user_form($a_form_notice);
				}
				else
				{
					array_push( $a_site_response_info, 'Unsuccessful Registration. Please Contact Administrator' );
					$a_form_notice['a_site_response_info'] = $a_site_response_info;
					$this->create_user_form($a_form_notice);
				}
				
			}
		}
		else
		{
			redirect( base_url() . 'admin/create_user_form/', 'refresh');
		}
		//======================================================
	}
	
	
	/**
	* read_profile
	* @desc		
	*
	**/
	public function read_profile()
	{
		//= Declare Start-Up Variables Here ====================
		$a_current_webpage_inc_metas = array();
		$a_current_webpage_inc_css = array();
		$a_current_webpage_inc_js_batch1 = array();
		$a_current_webpage_inc_js_batch2 = array();

		$s_view_header = '';
		$s_view_main = '';
		$s_view_footer = '';
		//======================================================
		
		
		//======================================================
		$this->load->library(array('pagination'));
		$this->load->library(array('l_user'));
		$this->load->library('l_def_sql');
		
		$this->load->model('m_def_table_fields');

		//======================================================
		
		
		//======================================================
		$a_user_details = $this->session->userdata('a_user_details');
		$a_user_roles_result = $this->session->userdata('a_user_roles_result');
		if( !isset($a_user_details) || empty($a_user_details) )
		{ redirect( base_url() . 'user/logout', 'refresh'); }
		if( array_key_exists( 1, $a_user_roles_result )	)
		{}
		else
		{ 
			$s_notice = '';
			$s_notice_query = '';
			$a_notice_params = array();
			$a_notice_params['message'] = 'Access Denied';
			$a_notice_params['subject'] = '';
			$a_notice_params['message_status'] = 'error';
			$s_notice_query = http_build_query($a_notice_params);
			$s_notice_query = $this->encrypt->encode($s_notice_query);
			$s_notice_query = base64_encode($s_notice_query);
			redirect( base_url() . 'notice/' . $s_notice_query, 'refresh');
		}

		
		/*
			parse uri first
		*/
		$a_expected_uri = array('user_id');
		$a_assoc_uri = $this->uri->uri_to_assoc(3, $a_expected_uri);
		
		
		/**/
		if( !isset($a_assoc_uri['user_id']) || empty($a_assoc_uri['user_id']) )
		{ redirect( base_url(), 'refresh'); }
		
		
		/*
		*/
		$a_u_result = array();
		$a_u_row_result = array();
		$a_u_query_where = array();
		$a_u_query_table_join = array();
		$a_u_query_limit = array();
		$a_u_query_params = array();
		array_push( $a_u_query_where, array( 's_field' => 'users.i_id', 'a_data' => $a_assoc_uri['user_id'] ) );
		array_push( $a_u_query_table_join, array( 's_table_join_name' => 'user_status_names', 's_table_join_condition' => 'user_status_names.i_id = users.i_usn_id' ) );
		$a_u_query_limit = array('i_limit' => 1, 'i_offset' => 0); 
		$a_u_query_params['a_where'] = $a_u_query_where;
		$a_u_query_params['a_limit'] = $a_u_query_limit;
		$a_u_query_params['a_table_join'] = $a_u_query_table_join;
		$a_u_query_params['s_table_fields'] = $this->m_def_table_fields->s_users_fields . ', ' . $this->m_def_table_fields->s_user_status_names_fields;
		$a_u_query_params['s_table_name'] = 'users';
		$a_u_result = $this->l_def_sql->read_data( $a_u_query_params );
		if( isset($a_u_result) && !empty($a_u_result) )
		{ $a_u_row_result = $a_u_result[0]; }

		
		/*
			Get user_roles
		*/
		$a_user_roles_query_where = array();
		$a_user_roles_query_table_join = array();
		$a_user_roles_query_params = array();
		array_push( $a_user_roles_query_where, array( 's_field' => 'user_roles.i_u_id', 'a_data' => $a_u_row_result['i_u_id'] ) );
		array_push( $a_user_roles_query_table_join, array( 's_table_join_name' => 'user_role_names', 's_table_join_condition' => 'user_role_names.i_id = user_roles.i_urn_id' ) );
		$a_user_roles_query_params['a_where'] = $a_user_roles_query_where;
		$a_user_roles_query_params['a_table_join'] = $a_user_roles_query_table_join;
		$a_user_roles_query_params['s_table_fields'] = $this->m_def_table_fields->s_user_roles_fields . ', ' . $this->m_def_table_fields->s_user_role_names_fields;
		$a_user_roles_query_params['s_table_name'] = 'user_roles';
		$a_user_roles_result = $this->l_def_sql->read_data( $a_user_roles_query_params );		
		
		//======================================================
		
		
		$s_view_header_data = array();
		$s_view_header = $this->load->view('templates_v1/v_header_v1', $s_view_header_data, true);
		
		$a_nav_menu_data = array();
		$s_view_nav_menu = $this->load->view('templates_v1/v_nav_menu_v1', $a_nav_menu_data, true);
		
		$s_view_main_data = array();
		$s_view_main_data['a_u_row_result'] = $a_u_row_result;
		$s_view_main_data['a_user_roles_result'] = $a_user_roles_result;
		$s_view_main_data['s_nav_menu'] = $s_view_nav_menu;
		$s_view_main = $this->load->view('templates_v1/v_admin_read_user_profile_v1', $s_view_main_data, true);
		
		$s_view_footer_data = array();
		$s_view_footer = $this->load->view('templates_v1/v_footer_v1', $s_view_footer_data, true);
		
		//======================================================

		
		//======================================================

		array_push($a_current_webpage_inc_css, 'css/css_reset.css');
		array_push($a_current_webpage_inc_css, 'css/css_style.css');

		array_push($a_current_webpage_inc_js_batch1, 'js/jquery-1.11.1.js');
		array_push($a_current_webpage_inc_js_batch1, 'js/js_detect_browser.js');

		$a_current_webpage_data['a_current_webpage_inc_metas'] = $a_current_webpage_inc_metas;
		$a_current_webpage_data['a_current_webpage_inc_css'] = $a_current_webpage_inc_css;
		$a_current_webpage_data['a_current_webpage_inc_js_batch1'] = $a_current_webpage_inc_js_batch1;
		$a_current_webpage_data['a_current_webpage_inc_js_batch2'] = $a_current_webpage_inc_js_batch2;
		$a_current_webpage_data['s_current_webpage_title'] = SITE_NAME;

		$a_current_webpage_data['s_current_webpage_header'] = $s_view_header;
		$a_current_webpage_data['s_current_webpage_main'] = $s_view_main;
		$a_current_webpage_data['s_current_webpage_footer'] = $s_view_footer;
		$s_current_webpage = $this->load->view('templates_v1/v_html_template_v1', $a_current_webpage_data, true);
		//======================================================

		
		//======================================================
		$this->output->set_output($s_current_webpage);
		//======================================================
	}
	
	
	/**
	* update_user_form
	* @desc		
	*
	**/
	public function update_user_form( $a_form_notice = array() )
	{
		//= Declare Start-Up Variables Here ====================
		$a_current_webpage_inc_metas = array();
		$a_current_webpage_inc_css = array();
		$a_current_webpage_inc_js_batch1 = array();
		$a_current_webpage_inc_js_batch2 = array();

		$s_view_header = '';
		$s_view_main = '';
		$s_view_footer = '';
		//======================================================
		
		
		//======================================================
		$this->load->library(array('pagination'));
		$this->load->library(array('l_user'));
		$this->load->library('l_def_sql');
		
		$this->load->model('m_def_table_fields');

		//======================================================
		
		
		//======================================================
		$a_user_details = $this->session->userdata('a_user_details');
		$a_user_roles_result = $this->session->userdata('a_user_roles_result');
		if( !isset($a_user_details) || empty($a_user_details) )
		{ redirect( base_url() . 'user/logout', 'refresh'); }
		if( array_key_exists( 1, $a_user_roles_result )	)
		{}
		else
		{ 
			$s_notice = '';
			$s_notice_query = '';
			$a_notice_params = array();
			$a_notice_params['message'] = 'Access Denied';
			$a_notice_params['subject'] = '';
			$a_notice_params['message_status'] = 'error';
			$s_notice_query = http_build_query($a_notice_params);
			$s_notice_query = $this->encrypt->encode($s_notice_query);
			$s_notice_query = base64_encode($s_notice_query);
			redirect( base_url() . 'notice/' . $s_notice_query, 'refresh');
		}
		
		
		/*
			parse uri first
		*/
		$a_expected_uri = array('user_id');
		$a_assoc_uri = $this->uri->uri_to_assoc(3, $a_expected_uri);
		
		
		/**/
		if( !isset($a_assoc_uri['user_id']) || empty($a_assoc_uri['user_id']) )
		{
			redirect( base_url(), 'refresh');
		}		
		
		
		/*
		*/
		$a_u_result = array();
		$a_u_row_result = array();
		$a_u_query_where = array();
		$a_u_query_table_join = array();
		$a_u_query_limit = array();
		$a_u_query_params = array();
		array_push( $a_u_query_where, array( 's_field' => 'users.i_id', 'a_data' => $a_assoc_uri['user_id'] ) );
		array_push( $a_u_query_table_join, array( 's_table_join_name' => 'user_status_names', 's_table_join_condition' => 'user_status_names.i_id = users.i_usn_id' ) );
		$a_u_query_limit = array('i_limit' => 1, 'i_offset' => 0);
		$a_u_query_params['a_where'] = $a_u_query_where;
		$a_u_query_params['a_limit'] = $a_u_query_limit;
		$a_u_query_params['a_table_join'] = $a_u_query_table_join;
		$a_u_query_params['s_table_fields'] = $this->m_def_table_fields->s_users_fields . ', ' . $this->m_def_table_fields->s_user_status_names_fields;
		$a_u_query_params['s_table_name'] = 'users';
		$a_u_result = $this->l_def_sql->read_data( $a_u_query_params );
		if( isset($a_u_result) && !empty($a_u_result) )
		{ $a_u_row_result = $a_u_result[0]; }
		if( !isset($a_u_row_result) || empty($a_u_row_result) )
		{ redirect( base_url(), 'refresh'); }
		
		
		/*
			Get current user_roles
		*/
		$a_u_roles_query_where = array();
		$a_u_roles_query_table_join = array();
		$a_u_roles_query_params = array();
		array_push( $a_u_roles_query_where, array( 's_field' => 'user_roles.i_u_id', 'a_data' => $a_u_row_result['i_u_id'] ) );
		array_push( $a_u_roles_query_table_join, array( 's_table_join_name' => 'user_role_names', 's_table_join_condition' => 'user_role_names.i_id = user_roles.i_urn_id' ) );
		$a_u_roles_query_params['a_where'] = $a_u_roles_query_where;
		$a_u_roles_query_params['a_table_join'] = $a_u_roles_query_table_join;
		$a_u_roles_query_params['s_table_fields'] = $this->m_def_table_fields->s_user_roles_fields . ', ' . $this->m_def_table_fields->s_user_role_names_fields;
		$a_u_roles_query_params['s_table_name'] = 'user_roles';
		$a_u_roles_result = $this->l_def_sql->read_data( $a_u_roles_query_params );
		//reindex
		$a_u_roles_result_temp = array();
		if( isset($a_u_roles_result) && !empty($a_u_roles_result) )
		{
			foreach( $a_u_roles_result AS $a_u_roles_result_row )
			{
				$a_u_roles_result_temp[$a_u_roles_result_row['i_urn_id']] = $a_u_roles_result_row;
			}
			$a_u_roles_result = $a_u_roles_result_temp;
		}
		
		
		/*
			Get user_role_names
		*/
		$a_user_role_names_query_where = array();
		$a_user_role_names_query_params = array();
		$a_user_role_names_query_params['s_table_fields'] = $this->m_def_table_fields->s_user_role_names_fields;
		$a_user_role_names_query_params['s_table_name'] = 'user_role_names';
		$a_user_role_names_result = $this->l_def_sql->read_data( $a_user_role_names_query_params );
		
		
		/*
			Get user_status_names
		*/
		$a_user_status_names_query_where = array();
		$a_user_status_names_query_params = array();
		$a_user_status_names_query_params['s_table_fields'] = $this->m_def_table_fields->s_user_status_names_fields;
		$a_user_status_names_query_params['s_table_name'] = 'user_status_names';
		$a_user_status_names_result = $this->l_def_sql->read_data( $a_user_status_names_query_params );
		
		
		//======================================================
		
		$s_view_site_responses_data = array();
		$s_view_site_responses_data['a_form_notice'] = $a_form_notice;
		$s_view_site_responses = $this->load->view('templates_v1/v_site_responses', $s_view_site_responses_data, true);
		
		$s_view_header_data = array();
		$s_view_header = $this->load->view('templates_v1/v_header_v1', $s_view_header_data, true);
		
		$a_nav_menu_data = array();
		$s_view_nav_menu = $this->load->view('templates_v1/v_nav_menu_v1', $a_nav_menu_data, true);
		
		$s_view_main_data = array();
		$s_view_main_data['s_view_site_responses'] = $s_view_site_responses;
		$s_view_main_data['a_u_row_result'] = $a_u_row_result;
		$s_view_main_data['a_u_roles_result'] = $a_u_roles_result;
		$s_view_main_data['a_user_role_names_result'] = $a_user_role_names_result;
		$s_view_main_data['a_user_status_names_result'] = $a_user_status_names_result;
		$s_view_main_data['s_nav_menu'] = $s_view_nav_menu;
		$s_view_main = $this->load->view('templates_v1/v_admin_update_user_form_v1', $s_view_main_data, true);
		
		$s_view_footer_data = array();
		$s_view_footer = $this->load->view('templates_v1/v_footer_v1', $s_view_footer_data, true);
		
		//======================================================

		
		//======================================================

		array_push($a_current_webpage_inc_css, 'css/css_reset.css');
		array_push($a_current_webpage_inc_css, 'css/css_style.css');

		array_push($a_current_webpage_inc_js_batch1, 'js/jquery-1.11.1.js');
		array_push($a_current_webpage_inc_js_batch1, 'js/js_detect_browser.js');

		$a_current_webpage_data['a_current_webpage_inc_metas'] = $a_current_webpage_inc_metas;
		$a_current_webpage_data['a_current_webpage_inc_css'] = $a_current_webpage_inc_css;
		$a_current_webpage_data['a_current_webpage_inc_js_batch1'] = $a_current_webpage_inc_js_batch1;
		$a_current_webpage_data['a_current_webpage_inc_js_batch2'] = $a_current_webpage_inc_js_batch2;
		$a_current_webpage_data['s_current_webpage_title'] = SITE_NAME;

		$a_current_webpage_data['s_current_webpage_header'] = $s_view_header;
		$a_current_webpage_data['s_current_webpage_main'] = $s_view_main;
		$a_current_webpage_data['s_current_webpage_footer'] = $s_view_footer;
		$s_current_webpage = $this->load->view('templates_v1/v_html_template_v1', $a_current_webpage_data, true);
		//======================================================

		
		//======================================================
		$this->output->set_output($s_current_webpage);
		//======================================================
	}
	
	
	/**
	* update_user
	* @desc		
	*
	**/
	public function update_user()
	{
		//= Declare Start-Up Variables Here ====================
		$a_form_notice = array();
		$a_site_response_error = array();
		$a_site_response_info = array();
		$a_site_response_success = array();
		
		$b_user_role_names_problem = false;
		//======================================================
		
		
		//======================================================
		$this->load->library('form_validation');
		$this->load->library('l_def_sql');
		
		$this->load->helper(array('form', 'security'));
		
		$this->load->model('m_def_table_fields');
		//======================================================
		
		
		//======================================================
		$a_user_details = $this->session->userdata('a_user_details');
		$a_user_roles_result = $this->session->userdata('a_user_roles_result');
		if( !isset($a_user_details) || empty($a_user_details) )
		{ redirect( base_url() . 'user/logout', 'refresh'); }
		if( array_key_exists( 1, $a_user_roles_result )	)
		{}
		else
		{ 
			$s_notice = '';
			$s_notice_query = '';
			$a_notice_params = array();
			$a_notice_params['message'] = 'Access Denied';
			$a_notice_params['subject'] = '';
			$a_notice_params['message_status'] = 'error';
			$s_notice_query = http_build_query($a_notice_params);
			$s_notice_query = $this->encrypt->encode($s_notice_query);
			$s_notice_query = base64_encode($s_notice_query);
			redirect( base_url() . 'notice/' . $s_notice_query, 'refresh');
		}
		
		
		/*
			parse uri first
		*/
		$a_expected_uri = array('user_id');
		$a_assoc_uri = $this->uri->uri_to_assoc(3, $a_expected_uri);
		
		
		/**/
		if( !isset($a_assoc_uri['user_id']) || empty($a_assoc_uri['user_id']) )
		{
			redirect( base_url(), 'refresh');
		}
		
		
		/*
			get user detail being edited
		*/
		$a_u_result = array();
		$a_u_row_result = array();
		$a_u_query_where = array();
		$a_u_query_table_join = array();
		$a_u_query_limit = array();
		$a_u_query_params = array();
		array_push( $a_u_query_where, array( 's_field' => 'users.i_id', 'a_data' => $a_assoc_uri['user_id'] ) );
		array_push( $a_u_query_table_join, array( 's_table_join_name' => 'user_status_names', 's_table_join_condition' => 'user_status_names.i_id = users.i_usn_id' ) );
		$a_u_query_limit = array('i_limit' => 1, 'i_offset' => 0); 
		$a_u_query_params['a_where'] = $a_u_query_where;
		$a_u_query_params['a_limit'] = $a_u_query_limit;
		$a_u_query_params['a_table_join'] = $a_u_query_table_join;
		$a_u_query_params['s_table_fields'] = $this->m_def_table_fields->s_users_fields . ', ' . $this->m_def_table_fields->s_user_status_names_fields;
		$a_u_query_params['s_table_name'] = 'users';
		$a_u_result = $this->l_def_sql->read_data( $a_u_query_params );
		if( isset($a_u_result) && !empty($a_u_result) )
		{ $a_u_row_result = $a_u_result[0]; }
		if( !isset($a_u_row_result) || empty($a_u_row_result) )
		{ redirect( base_url(), 'refresh'); }
		
		
		if( isset($_POST) && !empty($_POST) )
		{
			if( 	isset($_POST['txt_admin_update_user_email']) && !empty($_POST['txt_admin_update_user_email']) 
				&&	$_POST['txt_admin_update_user_email'] != $a_u_row_result['s_u_email']
			)
			{
				$this->form_validation->set_rules('txt_admin_update_user_email', 'Email', 'trim|required|max_length[55]valid_email|xss_clean|is_unique[users.s_email]');
			}
			else
			{
				$this->form_validation->set_rules('txt_admin_update_user_email', 'Email', 'trim|required|max_length[55]valid_email|xss_clean');
			}
			if( 	isset($_POST['txt_admin_update_user_username']) && !empty($_POST['txt_admin_update_user_username']) 
				&&	$_POST['txt_admin_update_user_username'] != $a_u_row_result['s_u_username']
			)
			{
				$this->form_validation->set_rules('txt_admin_update_user_username', 'Username', 'trim|required|min_length[5]|max_length[12]|xss_clean|is_unique[users.s_username]');
			}
			else
			{
				$this->form_validation->set_rules('txt_admin_update_user_username', 'Username', 'trim|required|min_length[5]|max_length[12]|xss_clean');
			}
			/*
				$str = do_hash($_POST['txt_admin_update_user_password'], 'md5');
			*/
			if( 	isset($_POST['txt_admin_update_user_password']) && !empty($_POST['txt_admin_update_user_password']) 
				&&	do_hash($_POST['txt_admin_update_user_password'], 'md5') != $a_u_row_result['s_u_password']
			)
			{
				$this->form_validation->set_rules('txt_admin_update_user_password', 'New Password', 'trim|required|matches[txt_admin_update_user_password_conf]|md5|xss_clean');
				$this->form_validation->set_rules('txt_admin_update_user_password_conf', 'New Password Confirmation', 'trim|required|xss_clean');
			}
			$this->form_validation->set_rules('txt_admin_update_user_firstname', 'First Name', 'trim|required|min_length[3]|max_length[50]|xss_clean');
			$this->form_validation->set_rules('txt_admin_update_user_lastname', 'Last Name', 'trim|min_length[3]|max_length[50]|xss_clean');
			$this->form_validation->set_rules('opt_admin_update_user_status', 'Status', 'trim|required|max_length[12]|xss_clean|callback_is_existing[user_status_names.i_id]');
			
			
			/*
				check if roles selected is existing
			*/
			if( isset($_POST['chk_admin_update_user_roles']) && !empty($_POST['chk_admin_update_user_roles']) )
			{
				$a_user_role_names_query_where = array();
				$a_user_role_names_query_where_in = array();
				$a_user_role_names_query_table_join = array();
				$a_user_role_names_query_params = array();
				array_push( $a_user_role_names_query_where_in, array( 's_field' => 'user_role_names.i_id', 'a_data' => $_POST['chk_admin_update_user_roles'] ) );
				$a_user_role_names_query_params['a_where_in'] = $a_user_role_names_query_where_in;
				$a_user_role_names_query_params['s_table_fields'] = $this->m_def_table_fields->s_user_role_names_fields;
				$a_user_role_names_query_params['s_table_name'] = 'user_role_names';
				$a_user_role_names_result = $this->l_def_sql->read_data( $a_user_role_names_query_params );

				if( isset($a_user_role_names_result) && !empty($a_user_role_names_result) )
				{
					if( count($a_user_role_names_result) != count($_POST['chk_admin_update_user_roles']) )
					{
						$b_user_role_names_problem = true;
					}
				}
				else
				{
					$b_user_role_names_problem = true;
				}
			}
			
			
			if(		$this->form_validation->run() == FALSE
				||	$b_user_role_names_problem == TRUE
			)
			{
				if( $b_user_role_names_problem == TRUE )
				{
					array_push( $a_site_response_error, 'User Roles Problems' );
				}				
				
				$a_form_notice['s_txt_admin_update_user_email_error'] = form_error('txt_admin_update_user_email', ' ', ' ');
				if( isset($a_form_notice['s_txt_admin_update_user_email_error']) && !empty($a_form_notice['s_txt_admin_update_user_email_error']) )
				{
					array_push( $a_site_response_error, $a_form_notice['s_txt_admin_update_user_email_error'] );
				}
				$a_form_notice['s_txt_admin_update_user_username_error'] = form_error('txt_admin_update_user_username', ' ', ' ');
				if( isset($a_form_notice['s_txt_admin_update_user_username_error']) && !empty($a_form_notice['s_txt_admin_update_user_username_error']) )
				{
					array_push( $a_site_response_error, $a_form_notice['s_txt_admin_update_user_username_error'] );
				}
				$a_form_notice['s_txt_admin_update_user_firstname_error'] = form_error('txt_admin_update_user_firstname', ' ', ' ');
				if( isset($a_form_notice['s_txt_admin_update_user_firstname_error']) && !empty($a_form_notice['s_txt_admin_update_user_firstname_error']) )
				{
					array_push( $a_site_response_error, $a_form_notice['s_txt_admin_update_user_firstname_error'] );
				}
				$a_form_notice['s_txt_admin_update_user_lastname_error'] = form_error('txt_admin_update_user_lastname', ' ', ' ');
				if( isset($a_form_notice['s_txt_admin_update_user_lastname_error']) && !empty($a_form_notice['s_txt_admin_update_user_lastname_error']) )
				{
					array_push( $a_site_response_error, $a_form_notice['s_txt_admin_update_user_lastname_error'] );
				}
				$a_form_notice['s_txt_admin_update_user_password_error'] = form_error('txt_admin_update_user_password', ' ', ' ');
				if( isset($a_form_notice['s_txt_admin_update_user_password_error']) && !empty($a_form_notice['s_txt_admin_update_user_password_error']) )
				{
					array_push( $a_site_response_error, $a_form_notice['s_txt_admin_update_user_password_error'] );
				}
				$a_form_notice['s_txt_admin_update_user_password_conf_error'] = form_error('txt_admin_update_user_password_conf', ' ', ' ');
				if( isset($a_form_notice['s_txt_admin_update_user_password_conf_error']) && !empty($a_form_notice['s_txt_admin_update_user_password_conf_error']) )
				{
					array_push( $a_site_response_error, $a_form_notice['s_txt_admin_update_user_password_conf_error'] );
				}
				$a_form_notice['s_opt_admin_update_user_status_error'] = form_error('opt_admin_update_user_status', ' ', ' ');
				if( isset($a_form_notice['s_opt_admin_update_user_status_error']) && !empty($a_form_notice['s_opt_admin_update_user_status_error']) )
				{
					array_push( $a_site_response_error, $a_form_notice['s_opt_admin_update_user_status_error'] );
				}

				$a_form_notice['a_site_response_error'] = $a_site_response_error;

				$this->update_user_form($a_form_notice);
			}
			else
			{
				/*
				*/
				$a_update_u_query_where = array();
				$a_update_u_query_params = array();
				array_push( $a_update_u_query_where, array( 's_field' => 'users.i_id', 'a_data' => $a_u_row_result['i_u_id'] ) );
				$a_update_data = array();
				$a_update_data['s_email'] = $_POST['txt_admin_update_user_email'];
				$a_update_data['s_username'] = $_POST['txt_admin_update_user_username'];
				$a_update_data['s_firstname'] = $_POST['txt_admin_update_user_firstname'];
				$a_update_data['s_lastname'] = $_POST['txt_admin_update_user_lastname'];
				if( isset($_POST['txt_admin_update_user_password']) && !empty($_POST['txt_admin_update_user_password']) )
				{
					$a_update_data['s_password'] = $_POST['txt_admin_update_user_password'];
				}
				$a_update_data['i_usn_id'] = $_POST['opt_admin_update_user_status'];
				$a_update_u_query_params['a_where'] = $a_update_u_query_where;
				$a_update_u_query_params['a_update_data'] = $a_update_data;
				$a_update_u_query_params['s_table_name'] = 'users';
				$a_update_u_result = $this->l_def_sql->update_data( $a_update_u_query_params );
				
				
				if( 	isset($a_update_u_result) && !empty($a_update_u_result) 
					&&	array_key_exists("i_sql_result", $a_update_u_result)
					&& 	$a_update_u_result["i_sql_result"] == 1 
				)
				{
					/*
						delete old roles
					*/
					$a_delete_u_roles_query_where = array();
					$a_delete_u_roles_query_params = array();
					array_push( $a_delete_u_roles_query_where, array( 's_field' => 'user_roles.i_u_id', 'a_data' => $a_u_row_result['i_u_id'] ) );
					$a_delete_u_roles_query_params['a_where'] = $a_delete_u_roles_query_where;
					$a_delete_u_roles_query_params['s_table_name'] = 'user_roles';
					$a_delete_u_roles_result = $this->l_def_sql->delete_data( $a_delete_u_roles_query_params );	
					
					
					/*
						add user_roles
					*/
					if( isset($_POST['chk_admin_update_user_roles']) && !empty($_POST['chk_admin_update_user_roles']) )
					{
						$a_add_user_roles_param = array();
						for( $i_counter = 0; $i_counter < count($_POST['chk_admin_update_user_roles']); $i_counter++ )
						{
							$a_add_user_roles_template = array();
							$a_add_user_roles_template['i_u_id'] = $a_u_row_result['i_u_id'];
							$a_add_user_roles_template['i_urn_id'] = $_POST['chk_admin_update_user_roles'][$i_counter];
							
							array_push( $a_add_user_roles_param, $a_add_user_roles_template );
						}
						
						$a_add_user_roles = array();
						$a_add_user_roles['a_new_data'] = $a_add_user_roles_param;
						$a_add_user_roles['s_table_name'] = 'user_roles';
						$a_add_user_roles_result = $this->l_def_sql->create_batch_data( $a_add_user_roles );
						
						if( 	isset($a_add_user_roles_result) && !empty($a_add_user_roles_result) 
								&&	array_key_exists("i_sql_result", $a_add_user_roles_result)
								&& 	$a_add_user_roles_result["i_sql_result"] == 1 
						)
						{ }
						else
						{	
							$a_result['s_result'] = 'fail';
							array_push( $a_site_response_error, 'User Roles not Saved' );
						}	
					}
					
					
					array_push( $a_site_response_success, 'Update Successful' );
					$a_form_notice['a_site_response_success'] = $a_site_response_success;
					$this->update_user_form($a_form_notice);
				}
				else
				{
					array_push( $a_site_response_info, 'Unsuccessful Update. Please Contact Administrator' );
					$a_form_notice['a_site_response_info'] = $a_site_response_info;
					$this->update_user_form($a_form_notice);
				}
			}
		}
		else
		{
			redirect( base_url() . 'admin/update_user_form/', 'refresh');
		}
		//======================================================
	}
	
	
	/**
	* create_user_role_form
	* @desc		
	*
	**/
	public function create_user_role_form( $a_form_notice = array() )
	{
		//= Declare Start-Up Variables Here ====================
		$a_current_webpage_inc_metas = array();
		$a_current_webpage_inc_css = array();
		$a_current_webpage_inc_js_batch1 = array();
		$a_current_webpage_inc_js_batch2 = array();
		
		$s_view_header = '';
		$s_view_main = '';
		$s_view_footer = '';
		
		$s_view_site_responses = '';
		//======================================================
		
		
		//======================================================
		$this->load->library('form_validation');
		$this->load->helper(array('captcha','form'));
		//======================================================
		
		
		//======================================================
		$a_user_details = $this->session->userdata('a_user_details');
		$a_user_roles_result = $this->session->userdata('a_user_roles_result');
		if( !isset($a_user_details) || empty($a_user_details) )
		{ redirect( base_url() . 'user/logout', 'refresh'); }
		if( array_key_exists( 1, $a_user_roles_result ) )
		{}
		else
		{ 
			$s_notice = '';
			$s_notice_query = '';
			$a_notice_params = array();
			$a_notice_params['message'] = 'Access Denied';
			$a_notice_params['subject'] = '';
			$a_notice_params['message_status'] = 'error';
			$s_notice_query = http_build_query($a_notice_params);
			$s_notice_query = $this->encrypt->encode($s_notice_query);
			$s_notice_query = base64_encode($s_notice_query);
			redirect( base_url() . 'notice/' . $s_notice_query, 'refresh');
		}
		
		
		/*
			Get user_status_names
		*/
		$a_user_role_name_status_names_query_where = array();
		$a_user_role_name_status_names_query_params = array();
		$a_user_role_name_status_names_query_params['s_table_fields'] = $this->m_def_table_fields->s_user_role_name_status_names_fields;
		$a_user_role_name_status_names_query_params['s_table_name'] = 'user_role_name_status_names';
		$a_user_role_name_status_names_result = $this->l_def_sql->read_data( $a_user_role_name_status_names_query_params );
		
		
		//======================================================
		
		
		$s_view_site_responses_data = array();
		$s_view_site_responses_data['a_form_notice'] = $a_form_notice;
		$s_view_site_responses = $this->load->view('templates_v1/v_site_responses', $s_view_site_responses_data, true);
		
		
		$s_view_header_data = array();
		$s_view_header = $this->load->view('templates_v1/v_header_v1', $s_view_header_data, true);
		
		$a_nav_menu_data = array();
		$s_view_nav_menu = $this->load->view('templates_v1/v_nav_menu_v1', $a_nav_menu_data, true);
		
		$s_view_main_data = array();
		$s_view_main_data['s_nav_menu'] = $s_view_nav_menu;
		$s_view_main_data['a_form_notice'] = $a_form_notice;
		$s_view_main_data['s_view_site_responses'] = $s_view_site_responses;
		$s_view_main_data['a_user_role_name_status_names_result'] = $a_user_role_name_status_names_result;
		$s_view_main = $this->load->view('templates_v1/v_admin_create_user_role_names_form_v1', $s_view_main_data, true);
		
		$s_view_footer_data = array();
		$s_view_footer = $this->load->view('templates_v1/v_footer_v1', $s_view_footer_data, true);
		
		//======================================================
		
		
		//======================================================
		array_push($a_current_webpage_inc_css, 'css/css_reset.css');
		array_push($a_current_webpage_inc_css, 'css/css_style.css');

		array_push($a_current_webpage_inc_js_batch1, 'js/jquery-1.11.1.js');
		array_push($a_current_webpage_inc_js_batch1, 'js/js_detect_browser.js');

		$a_current_webpage_data['a_current_webpage_inc_metas'] = $a_current_webpage_inc_metas;
		$a_current_webpage_data['a_current_webpage_inc_css'] = $a_current_webpage_inc_css;
		$a_current_webpage_data['a_current_webpage_inc_js_batch1'] = $a_current_webpage_inc_js_batch1;
		$a_current_webpage_data['a_current_webpage_inc_js_batch2'] = $a_current_webpage_inc_js_batch2;
		$a_current_webpage_data['s_current_webpage_title'] = SITE_NAME;

		$a_current_webpage_data['s_current_webpage_header'] = $s_view_header;
		$a_current_webpage_data['s_current_webpage_main'] = $s_view_main;
		$a_current_webpage_data['s_current_webpage_footer'] = $s_view_footer;
		$s_current_webpage = $this->load->view('templates_v1/v_html_template_v1', $a_current_webpage_data, true);
		//======================================================

		
		//======================================================
		$this->output->set_output($s_current_webpage);
		//======================================================
	}
	
	
	/**
	* create_user_role
	* @desc		
	*
	**/
	public function create_user_role()
	{
		//= Declare Start-Up Variables Here ====================
		$a_form_notice = array();
		$a_site_response_error = array();
		$a_site_response_info = array();
		
		$b_user_role_names_problem = false;
		//======================================================
		
		
		//======================================================
		$this->load->library('form_validation');
		$this->load->library('l_def_sql');
		$this->load->library('l_user');
		
		$this->load->helper(array('captcha','form'));
		//======================================================
		
		
		//======================================================
		$a_user_details = $this->session->userdata('a_user_details');
		$a_user_roles_result = $this->session->userdata('a_user_roles_result');
		if( !isset($a_user_details) || empty($a_user_details) )
		{ redirect( base_url() . 'user/logout', 'refresh'); }
		if( array_key_exists( 1, $a_user_roles_result ) )
		{}
		else
		{ 
			$s_notice = '';
			$s_notice_query = '';
			$a_notice_params = array();
			$a_notice_params['message'] = 'Access Denied';
			$a_notice_params['subject'] = '';
			$a_notice_params['message_status'] = 'error';
			$s_notice_query = http_build_query($a_notice_params);
			$s_notice_query = $this->encrypt->encode($s_notice_query);
			$s_notice_query = base64_encode($s_notice_query);
			redirect( base_url() . 'notice/' . $s_notice_query, 'refresh');
		}
		
		
		if( isset($_POST) && !empty($_POST) )
		{
			$this->form_validation->set_rules('txt_admin_create_user_role_names_name', 'Name', 'trim|required|min_length[4]|max_length[12]|xss_clean|is_unique[user_role_names.s_name]');
			$this->form_validation->set_rules('opt_admin_create_user_role_names_status', 'Status', 'trim|required|max_length[12]|xss_clean|callback_is_existing[user_role_name_status_names.i_id]');
			
			if( $this->form_validation->run() == FALSE )
			{
				$a_form_notice['s_txt_admin_create_user_role_names_name_error'] = form_error('txt_admin_create_user_role_names_name', ' ', ' ');
				if( isset($a_form_notice['s_txt_admin_create_user_role_names_name_error']) && !empty($a_form_notice['s_txt_admin_create_user_role_names_name_error']) )
				{
					array_push( $a_site_response_error, $a_form_notice['s_txt_admin_create_user_role_names_name_error'] );
				}
				$a_form_notice['s_opt_admin_create_user_role_names_status_error'] = form_error('opt_admin_create_user_role_names_status', ' ', ' ');
				if( isset($a_form_notice['s_opt_admin_create_user_role_names_status_error']) && !empty($a_form_notice['s_opt_admin_create_user_role_names_status_error']) )
				{
					array_push( $a_site_response_error, $a_form_notice['s_opt_admin_create_user_role_names_status_error'] );
				}

				$a_form_notice['a_site_response_error'] = $a_site_response_error;

				$this->create_user_role_form($a_form_notice);
			}
			else
			{
				$a_new_data = array();
				$a_new_data['s_name'] = $_POST['txt_admin_create_user_role_names_name']; 
				$a_new_data['i_urnsn_id'] = $_POST['opt_admin_create_user_role_names_status'];
				
				$a_add_user_role_name_result = array();
				$a_add_user_role_name_params['s_table_name'] = 'user_role_names';
				$a_add_user_role_name_params['a_new_data'] = $a_new_data;
				$a_add_user_role_name_result = $this->l_def_sql->create_data( $a_add_user_role_name_params );

				
				if( 	isset($a_add_user_role_name_result) && !empty($a_add_user_role_name_result) 
					&&	array_key_exists("i_sql_result", $a_add_user_role_name_result)
					&& 	$a_add_user_role_name_result["i_sql_result"] == 1 
				)
				{
					array_push( $a_site_response_info, 'User Role Name Create' );
					$a_form_notice['a_site_response_info'] = $a_site_response_info;
					$this->create_user_role_form($a_form_notice);
				}
				else
				{
					array_push( $a_site_response_info, 'User Role Name NOT Create' );
					$a_form_notice['a_site_response_info'] = $a_site_response_info;
					$this->create_user_role_form($a_form_notice);
				}
				
			}
		}
		else
		{
			redirect( base_url() . 'admin/create_user_role_form/', 'refresh');
		}
		//======================================================
	}
	
	
	/**
	* read_all_user_role
	* @desc		
	*
	**/
	public function read_all_user_role()
	{
		//= Declare Start-Up Variables Here ====================
		$a_current_webpage_inc_metas = array();
		$a_current_webpage_inc_css = array();
		$a_current_webpage_inc_js_batch1 = array();
		$a_current_webpage_inc_js_batch2 = array();

		$s_view_header = '';
		$s_view_main = '';
		$s_view_footer = '';
		//======================================================
		
		
		//======================================================
		$this->load->library(array('pagination'));
		$this->load->library(array('l_user'));
		$this->load->library('l_def_sql');
		
		$this->load->model('m_def_table_fields');

		//======================================================
		
		
		//======================================================
		$a_user_details = $this->session->userdata('a_user_details');
		$a_user_roles_result = $this->session->userdata('a_user_roles_result');
		if( !isset($a_user_details) || empty($a_user_details) )
		{ redirect( base_url() . 'user/logout', 'refresh'); }
		if( array_key_exists( 1, $a_user_roles_result ) )
		{}
		else
		{ 
			$s_notice = '';
			$s_notice_query = '';
			$a_notice_params = array();
			$a_notice_params['message'] = 'Access Denied';
			$a_notice_params['subject'] = '';
			$a_notice_params['message_status'] = 'error';
			$s_notice_query = http_build_query($a_notice_params);
			$s_notice_query = $this->encrypt->encode($s_notice_query);
			$s_notice_query = base64_encode($s_notice_query);
			redirect( base_url() . 'notice/' . $s_notice_query, 'refresh');
		}
		
		
		/*
			parse uri first
		*/
		$a_expected_uri = array('limit', 'offset', 'sort', 'order');
		$a_assoc_uri = $this->uri->uri_to_assoc(3, $a_expected_uri);
		
		
		/*
			check if filter is involved
		*/
		if( isset($_POST['opt_filter_admin_read_all_user_role_names_status']) && !empty($_POST['opt_filter_admin_read_all_user_role_names_status']) )
		{ 
			$a_assoc_uri['status'] = $_POST['opt_filter_admin_read_all_user_role_names_status']; 
		}
		if( isset($_POST['opt_filter_admin_read_all_user_role_names_sort']) && !empty($_POST['opt_filter_admin_read_all_user_role_names_sort']) )
		{ 
			$a_assoc_uri['sort'] = $_POST['opt_filter_admin_read_all_user_role_names_sort']; 
		}
		if( isset($_POST['opt_filter_admin_read_all_user_role_names_order']) && !empty($_POST['opt_filter_admin_read_all_user_role_names_order']) )
		{ 
			$a_assoc_uri['order'] = $_POST['opt_filter_admin_read_all_user_role_names_order']; 
		}
		if( isset($_POST['txt_filter_admin_read_all_users_limit']) && !empty($_POST['txt_filter_admin_read_all_users_limit']) )
		{ 
			$a_assoc_uri['limit'] = $_POST['txt_filter_admin_read_all_users_limit']; 
		}
		
		
		/*
			ready assoc before using sql
		*/
		$a_expected_sort = array('user_role_names.i_id'=>'ID', 'user_role_names.s_name'=>'Role Name', 'i_urnsn_id'=>'Status');
		$a_expected_order = array('asc'=>'ASC', 'desc'=>'DESC');
		if( !isset($a_assoc_uri['limit']) || empty($a_assoc_uri['limit']) )
		{ $a_assoc_uri['limit'] = 10; }
		else
		{
			if( !is_numeric($a_assoc_uri['limit']) || $a_assoc_uri['limit'] < 1 )
			{ $a_assoc_uri['limit'] = 10; }
		}
		if( !isset($a_assoc_uri['offset']) || empty($a_assoc_uri['offset']) )
		{ $a_assoc_uri['offset'] = 0; }
		else
		{
			if( !is_numeric($a_assoc_uri['offset']) || $a_assoc_uri['offset'] < 0 )
			{ $a_assoc_uri['offset'] = 0; }
		}
		
		if( !isset($a_assoc_uri['sort']) || empty($a_assoc_uri['sort']) )
		{ $a_assoc_uri['sort'] = 'user_role_names.s_name'; }
		else
		{
			if( !array_key_exists($a_assoc_uri['sort'], $a_expected_sort) )
			{ $a_assoc_uri['sort'] = 'user_role_names.s_name'; }
		}
		if( !isset($a_assoc_uri['order']) || empty($a_assoc_uri['order']) )
		{ $a_assoc_uri['order'] = 'asc'; }
		else
		{
			if( !array_key_exists($a_assoc_uri['order'], $a_expected_order) )
			{ $a_assoc_uri['order'] = 'asc'; }
		}
		if( !isset($a_assoc_uri['status']) || empty($a_assoc_uri['status']) )
		{ $a_assoc_uri['status'] = 0; }
		
		
		/*
			Get user_role_names
		*/
		$a_user_role_names_result = array();
		$a_user_role_names_query_where = array();
		$a_user_role_names_query_where_in = array();
		$a_user_role_names_query_table_join = array();
		$a_user_role_names_query_order_by = array();
		$a_user_role_names_query_limit = array();
		$a_user_role_names_query_params = array();
		if( isset( $a_assoc_uri['status'] ) && !empty( $a_assoc_uri['status'] ) )
		{
			array_push( $a_user_role_names_query_where, array( 's_field' => 'user_role_names.i_urnsn_id', 'a_data' => $a_assoc_uri['status'] ) );
		}
		array_push( $a_user_role_names_query_table_join, array( 's_table_join_name' => 'user_role_name_status_names', 's_table_join_condition' => 'user_role_name_status_names.i_id = user_role_names.i_urnsn_id' ) );
		array_push( $a_user_role_names_query_order_by, array( 's_field' => $a_assoc_uri['sort'], 'a_data' => $a_assoc_uri['order'] ) );		
		$a_user_role_names_query_limit = array('i_limit' => $a_assoc_uri['limit'], 'i_offset' => $a_assoc_uri['offset']);		
		$a_user_role_names_query_params['a_where'] = $a_user_role_names_query_where;
		$a_user_role_names_query_params['a_order_by'] = $a_user_role_names_query_order_by;
		$a_user_role_names_query_params['a_limit'] = $a_user_role_names_query_limit;
		$a_user_role_names_query_params['a_table_join'] = $a_user_role_names_query_table_join;
		$a_user_role_names_query_params['s_table_fields'] = $this->m_def_table_fields->s_user_role_names_fields . ', ' . $this->m_def_table_fields->s_user_role_name_status_names_fields;
		$a_user_role_names_query_params['s_table_name'] = 'user_role_names';
		$a_user_role_names_result = $this->l_def_sql->read_data( $a_user_role_names_query_params );
		$a_user_role_names_query_params['b_count'] = true;
		$a_user_role_names_count_result = $this->l_def_sql->read_data( $a_user_role_names_query_params );
		
		
		/*
			Get user_role_name_status_names
		*/
		$a_user_role_name_status_names_result = array();
		$a_user_role_name_status_names_query_where = array();
		$a_user_role_name_status_names_query_where_in = array();
		$a_user_role_name_status_names_query_table_join = array();
		$a_user_role_name_status_names_query_params = array();
		$a_user_role_name_status_names_query_params['s_table_fields'] = $this->m_def_table_fields->s_user_role_name_status_names_fields;
		$a_user_role_name_status_names_query_params['s_table_name'] = 'user_role_name_status_names';
		$a_user_role_name_status_names_result = $this->l_def_sql->read_data( $a_user_role_name_status_names_query_params );
		//reindex
		$a_user_role_name_status_names_result_temp = array();
		if( isset($a_user_role_name_status_names_result) && !empty($a_user_role_name_status_names_result) )
		{
			foreach( $a_user_role_name_status_names_result AS $a_user_role_name_status_names_result_row )
			{
				$a_user_role_name_status_names_result_temp[$a_user_role_name_status_names_result_row['i_urnsn_id']] = $a_user_role_name_status_names_result_row;
			}
			$a_user_role_name_status_names_result = $a_user_role_name_status_names_result_temp;
		}
		
		
		/*
			check $a_assoc_uri['status'] in array of possible values, if not set to default 0
		*/
		if( !array_key_exists($a_assoc_uri['status'], $a_user_role_name_status_names_result) )
		{
			$a_assoc_uri['status'] = 0;
		}
		
		
		/*
			Paging
		*/
		$i_page_uri_segment = 12;
		$a_pagination_config['base_url'] = base_url() . 'admin/read_all_user_role/limit/' .$a_assoc_uri['limit']. '/order/' .$a_assoc_uri['order']. '/sort/' .$a_assoc_uri['sort']. '/status/' .$a_assoc_uri['status']. '/offset/';
		$a_pagination_config['total_rows'] = $a_user_role_names_count_result['i_num_rows'];
		$a_pagination_config['per_page'] = $a_assoc_uri['limit'];
		$a_pagination_config['uri_segment'] = $i_page_uri_segment;
		$a_pagination_config['num_links'] = 5;
		$a_pagination_config['first_link'] = false;
		$a_pagination_config['last_link'] = false;
		$a_pagination_config['num_tag_open'] = '<li>';
		$a_pagination_config['num_tag_close'] = '</li>';
		$a_pagination_config['cur_tag_open'] = '<li class="clsli_pageactive_1"><a href="#" onclick="return false;">';
		$a_pagination_config['cur_tag_close'] = '</a></li>';
		$a_pagination_config['next_link'] = 'Next Page';
		$a_pagination_config['next_tag_open'] = '<li>';
		$a_pagination_config['next_tag_close'] = '</li>';
		$a_pagination_config['prev_link'] = 'Prev Page';
		$a_pagination_config['prev_tag_open'] = '<li>';
		$a_pagination_config['prev_tag_close'] = '</li>';
		$a_pagination_config['full_tag_open'] = '<ul>';
		$a_pagination_config['full_tag_close'] = '</ul>';
		$this->pagination->initialize($a_pagination_config); 
		$s_page_links_pagination = $this->pagination->create_links();
		
		
		//======================================================
		$s_view_header_data = array();
		$s_view_header = $this->load->view('templates_v1/v_header_v1', $s_view_header_data, true);
				
		$a_nav_menu_data = array();
		$s_view_nav_menu = $this->load->view('templates_v1/v_nav_menu_v1', $a_nav_menu_data, true);
		$s_view_main_data = array();
		$s_view_main_data['a_assoc_uri'] = $a_assoc_uri;
		$s_view_main_data['a_expected_sort'] = $a_expected_sort;
		$s_view_main_data['a_expected_order'] = $a_expected_order;
		$s_view_main_data['a_user_role_names_result'] = $a_user_role_names_result;
		$s_view_main_data['a_user_role_name_status_names_result'] = $a_user_role_name_status_names_result;
		$s_view_main_data['s_page_links_pagination'] = $s_page_links_pagination;
		$s_view_main_data['s_nav_menu'] = $s_view_nav_menu;
		$s_view_main = $this->load->view('templates_v1/v_admin_read_all_user_role_names_v1', $s_view_main_data, true);
		
		$s_view_footer_data = array();
		$s_view_footer = $this->load->view('templates_v1/v_footer_v1', $s_view_footer_data, true);
		
		//======================================================

		
		//======================================================

		array_push($a_current_webpage_inc_css, 'css/css_reset.css');
		array_push($a_current_webpage_inc_css, 'css/css_style.css');

		array_push($a_current_webpage_inc_js_batch1, 'js/jquery-1.11.1.js');
		array_push($a_current_webpage_inc_js_batch1, 'js/js_detect_browser.js');

		$a_current_webpage_data['a_current_webpage_inc_metas'] = $a_current_webpage_inc_metas;
		$a_current_webpage_data['a_current_webpage_inc_css'] = $a_current_webpage_inc_css;
		$a_current_webpage_data['a_current_webpage_inc_js_batch1'] = $a_current_webpage_inc_js_batch1;
		$a_current_webpage_data['a_current_webpage_inc_js_batch2'] = $a_current_webpage_inc_js_batch2;
		$a_current_webpage_data['s_current_webpage_title'] = SITE_NAME;

		$a_current_webpage_data['s_current_webpage_header'] = $s_view_header;
		$a_current_webpage_data['s_current_webpage_main'] = $s_view_main;
		$a_current_webpage_data['s_current_webpage_footer'] = $s_view_footer;
		$s_current_webpage = $this->load->view('templates_v1/v_html_template_v1', $a_current_webpage_data, true);
		//======================================================

		
		//======================================================
		$this->output->set_output($s_current_webpage);
		//======================================================
	}
	
	
	/**
	* update_user_role_form
	* @desc		
	*
	**/
	public function update_user_role_form( $a_form_notice = array() )
	{
		//= Declare Start-Up Variables Here ====================
		$a_current_webpage_inc_metas = array();
		$a_current_webpage_inc_css = array();
		$a_current_webpage_inc_js_batch1 = array();
		$a_current_webpage_inc_js_batch2 = array();

		$s_view_header = '';
		$s_view_main = '';
		$s_view_footer = '';
		//======================================================
		
		
		//======================================================
		$this->load->library(array('pagination'));
		$this->load->library(array('l_user'));
		$this->load->library('l_def_sql');
		
		$this->load->model('m_def_table_fields');

		//======================================================
		
		
		//======================================================
		$a_user_details = $this->session->userdata('a_user_details');
		$a_user_roles_result = $this->session->userdata('a_user_roles_result');
		if( !isset($a_user_details) || empty($a_user_details) )
		{ redirect( base_url() . 'user/logout', 'refresh'); }
		if( array_key_exists( 1, $a_user_roles_result )	)
		{}
		else
		{ 
			$s_notice = '';
			$s_notice_query = '';
			$a_notice_params = array();
			$a_notice_params['message'] = 'Access Denied';
			$a_notice_params['subject'] = '';
			$a_notice_params['message_status'] = 'error';
			$s_notice_query = http_build_query($a_notice_params);
			$s_notice_query = $this->encrypt->encode($s_notice_query);
			$s_notice_query = base64_encode($s_notice_query);
			redirect( base_url() . 'notice/' . $s_notice_query, 'refresh');
		}
		
		
		/*
			parse uri first
		*/
		$a_expected_uri = array('user_role_name_id');
		$a_assoc_uri = $this->uri->uri_to_assoc(3, $a_expected_uri);
		
		
		/**/
		if( !isset($a_assoc_uri['user_role_name_id']) || empty($a_assoc_uri['user_role_name_id']) )
		{
			redirect( base_url(), 'refresh');
		}		
		
		
		/*
		*/
		$a_user_role_names_result = array();
		$a_user_role_names_row_result = array();
		$a_user_role_names_query_where = array();
		$a_user_role_names_query_table_join = array();
		$a_user_role_names_query_limit = array();
		$a_user_role_names_query_params = array();
		array_push( $a_user_role_names_query_where, array( 's_field' => 'user_role_names.i_id', 'a_data' => $a_assoc_uri['user_role_name_id'] ) );
		array_push( $a_user_role_names_query_table_join, array( 's_table_join_name' => 'user_role_name_status_names', 's_table_join_condition' => 'user_role_name_status_names.i_id = user_role_names.i_urnsn_id' ) );
		$a_user_role_names_query_limit = array('i_limit' => 1, 'i_offset' => 0);
		$a_user_role_names_query_params['a_where'] = $a_user_role_names_query_where;
		$a_user_role_names_query_params['a_limit'] = $a_user_role_names_query_limit;
		$a_user_role_names_query_params['a_table_join'] = $a_user_role_names_query_table_join;
		$a_user_role_names_query_params['s_table_fields'] = $this->m_def_table_fields->s_user_role_names_fields . ', ' . $this->m_def_table_fields->s_user_role_name_status_names_fields;
		$a_user_role_names_query_params['s_table_name'] = 'user_role_names';
		$a_user_role_names_result = $this->l_def_sql->read_data( $a_user_role_names_query_params );
		if( isset($a_user_role_names_result) && !empty($a_user_role_names_result) )
		{ $a_user_role_names_row_result = $a_user_role_names_result[0]; }
		if( !isset($a_user_role_names_row_result) || empty($a_user_role_names_row_result) )
		{ redirect( base_url(), 'refresh'); }
		
		
		/*
			Get user_role_name_status_names
		*/
		$a_user_role_name_status_names_query_where = array();
		$a_user_role_name_status_names_query_params = array();
		$a_user_role_name_status_names_query_params['s_table_fields'] = $this->m_def_table_fields->s_user_role_name_status_names_fields;
		$a_user_role_name_status_names_query_params['s_table_name'] = 'user_role_name_status_names';
		$a_user_role_name_status_names_result = $this->l_def_sql->read_data( $a_user_role_name_status_names_query_params );
		
		
		//======================================================
		
		$s_view_site_responses_data = array();
		$s_view_site_responses_data['a_form_notice'] = $a_form_notice;
		$s_view_site_responses = $this->load->view('templates_v1/v_site_responses', $s_view_site_responses_data, true);
		
		$s_view_header_data = array();
		$s_view_header = $this->load->view('templates_v1/v_header_v1', $s_view_header_data, true);
		
		$a_nav_menu_data = array();
		$s_view_nav_menu = $this->load->view('templates_v1/v_nav_menu_v1', $a_nav_menu_data, true);
		
		$s_view_main_data = array();
		$s_view_main_data['s_view_site_responses'] = $s_view_site_responses;
		$s_view_main_data['a_user_role_names_row_result'] = $a_user_role_names_row_result;
		$s_view_main_data['a_user_role_name_status_names_result'] = $a_user_role_name_status_names_result;
		$s_view_main_data['s_nav_menu'] = $s_view_nav_menu;
		$s_view_main = $this->load->view('templates_v1/v_admin_update_user_role_names_v1', $s_view_main_data, true);
		
		$s_view_footer_data = array();
		$s_view_footer = $this->load->view('templates_v1/v_footer_v1', $s_view_footer_data, true);
		
		//======================================================

		
		//======================================================

		array_push($a_current_webpage_inc_css, 'css/css_reset.css');
		array_push($a_current_webpage_inc_css, 'css/css_style.css');

		array_push($a_current_webpage_inc_js_batch1, 'js/jquery-1.11.1.js');
		array_push($a_current_webpage_inc_js_batch1, 'js/js_detect_browser.js');

		$a_current_webpage_data['a_current_webpage_inc_metas'] = $a_current_webpage_inc_metas;
		$a_current_webpage_data['a_current_webpage_inc_css'] = $a_current_webpage_inc_css;
		$a_current_webpage_data['a_current_webpage_inc_js_batch1'] = $a_current_webpage_inc_js_batch1;
		$a_current_webpage_data['a_current_webpage_inc_js_batch2'] = $a_current_webpage_inc_js_batch2;
		$a_current_webpage_data['s_current_webpage_title'] = SITE_NAME;

		$a_current_webpage_data['s_current_webpage_header'] = $s_view_header;
		$a_current_webpage_data['s_current_webpage_main'] = $s_view_main;
		$a_current_webpage_data['s_current_webpage_footer'] = $s_view_footer;
		$s_current_webpage = $this->load->view('templates_v1/v_html_template_v1', $a_current_webpage_data, true);
		//======================================================

		
		//======================================================
		$this->output->set_output($s_current_webpage);
		//======================================================
	}
	
	
	/**
	* update_user_role
	* @desc		
	*
	**/
	public function update_user_role()
	{
		//= Declare Start-Up Variables Here ====================
		$a_form_notice = array();
		$a_site_response_error = array();
		$a_site_response_info = array();
		$a_site_response_success = array();
		
		$b_user_role_names_problem = false;
		//======================================================
		
		
		//======================================================
		$this->load->library('form_validation');
		$this->load->library('l_def_sql');
		
		$this->load->helper(array('form', 'security'));
		
		$this->load->model('m_def_table_fields');
		//======================================================
		
		
		//======================================================
		$a_user_details = $this->session->userdata('a_user_details');
		$a_user_roles_result = $this->session->userdata('a_user_roles_result');
		if( !isset($a_user_details) || empty($a_user_details) )
		{ redirect( base_url() . 'user/logout', 'refresh'); }
		if( array_key_exists( 1, $a_user_roles_result )	)
		{}
		else
		{ 
			$s_notice = '';
			$s_notice_query = '';
			$a_notice_params = array();
			$a_notice_params['message'] = 'Access Denied';
			$a_notice_params['subject'] = '';
			$a_notice_params['message_status'] = 'error';
			$s_notice_query = http_build_query($a_notice_params);
			$s_notice_query = $this->encrypt->encode($s_notice_query);
			$s_notice_query = base64_encode($s_notice_query);
			redirect( base_url() . 'notice/' . $s_notice_query, 'refresh'); 
		}
		
		
		/*
			parse uri first
		*/
		$a_expected_uri = array('user_role_name_id');
		$a_assoc_uri = $this->uri->uri_to_assoc(3, $a_expected_uri);
		
		
		/**/
		if( !isset($a_assoc_uri['user_role_name_id']) || empty($a_assoc_uri['user_role_name_id']) )
		{
			redirect( base_url(), 'refresh');
		}
		
		
		/*
			get user_role_names being edited
		*/
		/*
		*/
		$a_user_role_names_result = array();
		$a_user_role_names_row_result = array();
		$a_user_role_names_query_where = array();
		$a_user_role_names_query_table_join = array();
		$a_user_role_names_query_limit = array();
		$a_user_role_names_query_params = array();
		array_push( $a_user_role_names_query_where, array( 's_field' => 'user_role_names.i_id', 'a_data' => $a_assoc_uri['user_role_name_id'] ) );
		array_push( $a_user_role_names_query_table_join, array( 's_table_join_name' => 'user_role_name_status_names', 's_table_join_condition' => 'user_role_name_status_names.i_id = user_role_names.i_urnsn_id' ) );
		$a_user_role_names_query_limit = array('i_limit' => 1, 'i_offset' => 0);
		$a_user_role_names_query_params['a_where'] = $a_user_role_names_query_where;
		$a_user_role_names_query_params['a_limit'] = $a_user_role_names_query_limit;
		$a_user_role_names_query_params['a_table_join'] = $a_user_role_names_query_table_join;
		$a_user_role_names_query_params['s_table_fields'] = $this->m_def_table_fields->s_user_role_names_fields . ', ' . $this->m_def_table_fields->s_user_role_name_status_names_fields;
		$a_user_role_names_query_params['s_table_name'] = 'user_role_names';
		$a_user_role_names_result = $this->l_def_sql->read_data( $a_user_role_names_query_params );
		if( isset($a_user_role_names_result) && !empty($a_user_role_names_result) )
		{ $a_user_role_names_row_result = $a_user_role_names_result[0]; }
		if( !isset($a_user_role_names_row_result) || empty($a_user_role_names_row_result) )
		{ redirect( base_url(), 'refresh'); }
		
		
		if( isset($_POST) && !empty($_POST) )
		{
			if( 	isset($_POST['txt_admin_update_user_role_name_name']) && !empty($_POST['txt_admin_update_user_role_name_name']) 
				&&	$_POST['txt_admin_update_user_role_name_name'] != $a_user_role_names_row_result['s_urn_name']
			)
			{
				$this->form_validation->set_rules('txt_admin_update_user_role_name_name', 'Role Name', 'trim|required|min_length[4]|max_length[12]|xss_clean|is_unique[user_role_names.s_name]');
			}
			else
			{
				$this->form_validation->set_rules('txt_admin_update_user_role_name_name', 'Role Name', 'trim|required|min_length[4]|max_length[12]|xss_clean');
			}
			
			$this->form_validation->set_rules('opt_admin_update_user_role_name_status', 'Status', 'trim|required|max_length[12]|xss_clean|callback_is_existing[user_role_name_status_names.i_id]');
			
			
			if( $this->form_validation->run() == FALSE )
			{
				$a_form_notice['s_txt_admin_update_user_role_name_name_error'] = form_error('txt_admin_update_user_role_name_name', ' ', ' ');
				if( isset($a_form_notice['s_txt_admin_update_user_role_name_name_error']) && !empty($a_form_notice['s_txt_admin_update_user_role_name_name_error']) )
				{
					array_push( $a_site_response_error, $a_form_notice['s_txt_admin_update_user_role_name_name_error'] );
				}
				$a_form_notice['s_opt_admin_update_user_role_name_status_error'] = form_error('opt_admin_update_user_role_name_status', ' ', ' ');
				if( isset($a_form_notice['s_opt_admin_update_user_role_name_status_error']) && !empty($a_form_notice['s_opt_admin_update_user_role_name_status_error']) )
				{
					array_push( $a_site_response_error, $a_form_notice['s_opt_admin_update_user_role_name_status_error'] );
				}

				$a_form_notice['a_site_response_error'] = $a_site_response_error;

				$this->update_user_role_form($a_form_notice);
			}
			else
			{
				/*
				*/
				$a_update_user_role_names_query_where = array();
				$a_update_user_role_names_query_params = array();
				array_push( $a_update_user_role_names_query_where, array( 's_field' => 'user_role_names.i_id', 'a_data' => $a_assoc_uri['user_role_name_id'] ) );
				$a_update_data = array();
				$a_update_data['s_name'] = $_POST['txt_admin_update_user_role_name_name'];
				$a_update_data['i_urnsn_id'] = $_POST['opt_admin_update_user_role_name_status'];
				$a_update_user_role_names_query_params['a_where'] = $a_update_user_role_names_query_where;
				$a_update_user_role_names_query_params['a_update_data'] = $a_update_data;
				$a_update_user_role_names_query_params['s_table_name'] = 'user_role_names';
				$a_update_user_role_names_result = $this->l_def_sql->update_data( $a_update_user_role_names_query_params );
				
				
				if( 	isset($a_update_user_role_names_result) && !empty($a_update_user_role_names_result) 
					&&	array_key_exists("i_sql_result", $a_update_user_role_names_result)
					&& 	$a_update_user_role_names_result["i_sql_result"] == 1 
				)
				{
					array_push( $a_site_response_success, 'Update Successful' );
					$a_form_notice['a_site_response_success'] = $a_site_response_success;
					$this->update_user_role_form($a_form_notice);
				}
				else
				{
					array_push( $a_site_response_info, 'Unsuccessful Update. Please Contact Administrator' );
					$a_form_notice['a_site_response_info'] = $a_site_response_info;
					$this->update_user_role_form($a_form_notice);
				}
			}
		}
		else
		{
			redirect( base_url() . 'admin/update_user_role_form/', 'refresh');
		}
		//======================================================
	}
	
	
	/**
	* create_user_status_form
	* @desc		
	*
	**/
	public function create_user_status_form( $a_form_notice = array() )
	{
		//= Declare Start-Up Variables Here ====================
		$a_current_webpage_inc_metas = array();
		$a_current_webpage_inc_css = array();
		$a_current_webpage_inc_js_batch1 = array();
		$a_current_webpage_inc_js_batch2 = array();
		
		$s_view_header = '';
		$s_view_main = '';
		$s_view_footer = '';
		
		$s_view_site_responses = '';
		//======================================================
		
		
		//======================================================
		$this->load->library('form_validation');
		$this->load->helper(array('captcha','form'));
		//======================================================
		
		
		//======================================================
		$a_user_details = $this->session->userdata('a_user_details');
		$a_user_roles_result = $this->session->userdata('a_user_roles_result');
		if( !isset($a_user_details) || empty($a_user_details) )
		{ redirect( base_url() . 'user/logout', 'refresh'); }
		if( array_key_exists( 1, $a_user_roles_result ) )
		{}
		else
		{ 
			$s_notice = '';
			$s_notice_query = '';
			$a_notice_params = array();
			$a_notice_params['message'] = 'Access Denied';
			$a_notice_params['subject'] = '';
			$a_notice_params['message_status'] = 'error';
			$s_notice_query = http_build_query($a_notice_params);
			$s_notice_query = $this->encrypt->encode($s_notice_query);
			$s_notice_query = base64_encode($s_notice_query);
			redirect( base_url() . 'notice/' . $s_notice_query, 'refresh');
		}
		
		
		/*
			Get user_status_names
		$a_user_status_names_query_where = array();
		$a_user_status_names_query_params = array();
		$a_user_status_names_query_params['s_table_fields'] = $this->m_def_table_fields->s_user_status_names_fields;
		$a_user_status_names_query_params['s_table_name'] = 'user_status_names';
		$a_user_status_names_result = $this->l_def_sql->read_data( $a_user_status_names_query_params );
		*/
		
		
		//======================================================
		
		
		$s_view_site_responses_data = array();
		$s_view_site_responses_data['a_form_notice'] = $a_form_notice;
		$s_view_site_responses = $this->load->view('templates_v1/v_site_responses', $s_view_site_responses_data, true);
		
		
		$s_view_header_data = array();
		$s_view_header = $this->load->view('templates_v1/v_header_v1', $s_view_header_data, true);
		
		$a_nav_menu_data = array();
		$s_view_nav_menu = $this->load->view('templates_v1/v_nav_menu_v1', $a_nav_menu_data, true);
		
		$s_view_main_data = array();
		$s_view_main_data['s_nav_menu'] = $s_view_nav_menu;
		$s_view_main_data['a_form_notice'] = $a_form_notice;
		$s_view_main_data['s_view_site_responses'] = $s_view_site_responses;
		//$s_view_main_data['a_user_status_names_result'] = $a_user_status_names_result;
		$s_view_main = $this->load->view('templates_v1/v_admin_create_user_status_form_v1', $s_view_main_data, true);
		
		$s_view_footer_data = array();
		$s_view_footer = $this->load->view('templates_v1/v_footer_v1', $s_view_footer_data, true);
		
		//======================================================
		
		
		//======================================================
		array_push($a_current_webpage_inc_css, 'css/css_reset.css');
		array_push($a_current_webpage_inc_css, 'css/css_style.css');

		array_push($a_current_webpage_inc_js_batch1, 'js/jquery-1.11.1.js');
		array_push($a_current_webpage_inc_js_batch1, 'js/js_detect_browser.js');

		$a_current_webpage_data['a_current_webpage_inc_metas'] = $a_current_webpage_inc_metas;
		$a_current_webpage_data['a_current_webpage_inc_css'] = $a_current_webpage_inc_css;
		$a_current_webpage_data['a_current_webpage_inc_js_batch1'] = $a_current_webpage_inc_js_batch1;
		$a_current_webpage_data['a_current_webpage_inc_js_batch2'] = $a_current_webpage_inc_js_batch2;
		$a_current_webpage_data['s_current_webpage_title'] = SITE_NAME;

		$a_current_webpage_data['s_current_webpage_header'] = $s_view_header;
		$a_current_webpage_data['s_current_webpage_main'] = $s_view_main;
		$a_current_webpage_data['s_current_webpage_footer'] = $s_view_footer;
		$s_current_webpage = $this->load->view('templates_v1/v_html_template_v1', $a_current_webpage_data, true);
		//======================================================

		
		//======================================================
		$this->output->set_output($s_current_webpage);
		//======================================================
	}
	
	
	/**
	* create_user_status
	* @desc		
	*
	**/
	public function create_user_status()
	{
		//= Declare Start-Up Variables Here ====================
		$a_form_notice = array();
		$a_site_response_error = array();
		$a_site_response_info = array();
		
		$b_user_role_names_problem = false;
		//======================================================
		
		
		//======================================================
		$this->load->library('form_validation');
		$this->load->library('l_def_sql');
		$this->load->library('l_user');
		
		$this->load->helper(array('captcha','form'));
		//======================================================
		
		
		//======================================================
		$a_user_details = $this->session->userdata('a_user_details');
		$a_user_roles_result = $this->session->userdata('a_user_roles_result');
		if( !isset($a_user_details) || empty($a_user_details) )
		{ redirect( base_url() . 'user/logout', 'refresh'); }
		if( array_key_exists( 1, $a_user_roles_result ) )
		{}
		else
		{ 
			$s_notice = '';
			$s_notice_query = '';
			$a_notice_params = array();
			$a_notice_params['message'] = 'Access Denied';
			$a_notice_params['subject'] = '';
			$a_notice_params['message_status'] = 'error';
			$s_notice_query = http_build_query($a_notice_params);
			$s_notice_query = $this->encrypt->encode($s_notice_query);
			$s_notice_query = base64_encode($s_notice_query);
			redirect( base_url() . 'notice/' . $s_notice_query, 'refresh');
		}
		
		
		if( isset($_POST) && !empty($_POST) )
		{
			$this->form_validation->set_rules('txt_admin_create_user_status_names_name', 'Name', 'trim|required|min_length[4]|max_length[12]|xss_clean|is_unique[user_status_names.s_name]');
			
			if( $this->form_validation->run() == FALSE )
			{
				$a_form_notice['s_txt_admin_create_user_status_names_name_error'] = form_error('txt_admin_create_user_status_names_name', ' ', ' ');
				if( isset($a_form_notice['s_txt_admin_create_user_status_names_name_error']) && !empty($a_form_notice['s_txt_admin_create_user_status_names_name_error']) )
				{
					array_push( $a_site_response_error, $a_form_notice['s_txt_admin_create_user_status_names_name_error'] );
				}

				$a_form_notice['a_site_response_error'] = $a_site_response_error;

				$this->create_user_status_form($a_form_notice);
			}
			else
			{
				$a_new_data = array();
				$a_new_data['s_name'] = $_POST['txt_admin_create_user_status_names_name']; 
				
				$a_add_user_role_name_result = array();
				$a_add_user_role_name_params['s_table_name'] = 'user_status_names';
				$a_add_user_role_name_params['a_new_data'] = $a_new_data;
				$a_add_user_role_name_result = $this->l_def_sql->create_data( $a_add_user_role_name_params );

				
				if( 	isset($a_add_user_role_name_result) && !empty($a_add_user_role_name_result) 
					&&	array_key_exists("i_sql_result", $a_add_user_role_name_result)
					&& 	$a_add_user_role_name_result["i_sql_result"] == 1 
				)
				{
					array_push( $a_site_response_info, 'User Status Name Create' );
					$a_form_notice['a_site_response_info'] = $a_site_response_info;
					$this->create_user_status_form($a_form_notice);
				}
				else
				{
					array_push( $a_site_response_info, 'User Status Name NOT Create' );
					$a_form_notice['a_site_response_info'] = $a_site_response_info;
					$this->create_user_status_form($a_form_notice);
				}
				
			}
		}
		else
		{
			redirect( base_url() . 'admin/create_user_status_form/', 'refresh');
		}
		//======================================================
	}


	/**
	* read_all_user_status
	* @desc		
	*
	**/
	public function read_all_user_status( $a_form_notice = array() )
	{
		//= Declare Start-Up Variables Here ====================
		$a_current_webpage_inc_metas = array();
		$a_current_webpage_inc_css = array();
		$a_current_webpage_inc_js_batch1 = array();
		$a_current_webpage_inc_js_batch2 = array();

		$s_view_header = '';
		$s_view_main = '';
		$s_view_footer = '';
		//======================================================
		
		
		//======================================================
		$this->load->library(array('pagination'));
		$this->load->library(array('l_user'));
		$this->load->library('l_def_sql');
		
		$this->load->model('m_def_table_fields');

		//======================================================
		
		
		//======================================================
		$a_user_details = $this->session->userdata('a_user_details');
		$a_user_roles_result = $this->session->userdata('a_user_roles_result');
		if( !isset($a_user_details) || empty($a_user_details) )
		{ redirect( base_url() . 'user/logout', 'refresh'); }
		if( array_key_exists( 1, $a_user_roles_result ) )
		{}
		else
		{ 
			$s_notice = '';
			$s_notice_query = '';
			$a_notice_params = array();
			$a_notice_params['message'] = 'Access Denied';
			$a_notice_params['subject'] = '';
			$a_notice_params['message_status'] = 'error';
			$s_notice_query = http_build_query($a_notice_params);
			$s_notice_query = $this->encrypt->encode($s_notice_query);
			$s_notice_query = base64_encode($s_notice_query);
			redirect( base_url() . 'notice/' . $s_notice_query, 'refresh'); 
		}
		
		
		/*
			parse uri first
		*/
		$a_expected_uri = array('limit', 'offset', 'sort', 'order');
		$a_assoc_uri = $this->uri->uri_to_assoc(3, $a_expected_uri);
		
		
		/*
			check if filter is involved
		*/
		if( isset($_POST['opt_filter_admin_read_all_user_status_names_sort']) && !empty($_POST['opt_filter_admin_read_all_user_status_names_sort']) )
		{ 
			$a_assoc_uri['sort'] = $_POST['opt_filter_admin_read_all_user_status_names_sort']; 
		}
		if( isset($_POST['opt_filter_admin_read_all_user_status_names_order']) && !empty($_POST['opt_filter_admin_read_all_user_status_names_order']) )
		{ 
			$a_assoc_uri['order'] = $_POST['opt_filter_admin_read_all_user_status_names_order']; 
		}
		if( isset($_POST['txt_filter_admin_read_all_user_status_names_limit']) && !empty($_POST['txt_filter_admin_read_all_user_status_names_limit']) )
		{ 
			$a_assoc_uri['limit'] = $_POST['txt_filter_admin_read_all_user_status_names_limit']; 
		}
		
		
		/*
			ready assoc before using sql
		*/
		$a_expected_sort = array('i_id'=>'ID', 's_name'=>'Status Name');
		$a_expected_order = array('asc'=>'ASC', 'desc'=>'DESC');
		if( !isset($a_assoc_uri['limit']) || empty($a_assoc_uri['limit']) )
		{ $a_assoc_uri['limit'] = 10; }
		else
		{
			if( !is_numeric($a_assoc_uri['limit']) || $a_assoc_uri['limit'] < 1 )
			{ $a_assoc_uri['limit'] = 10; }
		}
		if( !isset($a_assoc_uri['offset']) || empty($a_assoc_uri['offset']) )
		{ $a_assoc_uri['offset'] = 0; }
		else
		{
			if( !is_numeric($a_assoc_uri['offset']) || $a_assoc_uri['offset'] < 0 )
			{ $a_assoc_uri['offset'] = 0; }
		}
		
		if( !isset($a_assoc_uri['sort']) || empty($a_assoc_uri['sort']) )
		{ $a_assoc_uri['sort'] = 's_name'; }
		else
		{
			if( !array_key_exists($a_assoc_uri['sort'], $a_expected_sort) )
			{ $a_assoc_uri['sort'] = 's_name'; }
		}
		if( !isset($a_assoc_uri['order']) || empty($a_assoc_uri['order']) )
		{ $a_assoc_uri['order'] = 'asc'; }
		else
		{
			if( !array_key_exists($a_assoc_uri['order'], $a_expected_order) )
			{ $a_assoc_uri['order'] = 'asc'; }
		}
		if( !isset($a_assoc_uri['status']) || empty($a_assoc_uri['status']) )
		{ $a_assoc_uri['status'] = 0; }
		
		
		/*
			Get user_status_names
		*/
		$a_user_status_names_result = array();
		$a_user_status_names_query_where = array();
		$a_user_status_names_query_where_in = array();
		$a_user_status_names_query_table_join = array();
		$a_user_status_names_query_order_by = array();
		$a_user_status_names_query_limit = array();
		$a_user_status_names_query_params = array();
		array_push( $a_user_status_names_query_order_by, array( 's_field' => $a_assoc_uri['sort'], 'a_data' => $a_assoc_uri['order'] ) );		
		$a_user_status_names_query_limit = array('i_limit' => $a_assoc_uri['limit'], 'i_offset' => $a_assoc_uri['offset']);		
		$a_user_status_names_query_params['a_order_by'] = $a_user_status_names_query_order_by;
		$a_user_status_names_query_params['a_limit'] = $a_user_status_names_query_limit;
		$a_user_status_names_query_params['s_table_fields'] = $this->m_def_table_fields->s_user_status_names_fields;
		$a_user_status_names_query_params['s_table_name'] = 'user_status_names';
		$a_user_status_names_result = $this->l_def_sql->read_data( $a_user_status_names_query_params );
		$a_user_status_names_query_params['b_count'] = true;
		$a_user_status_names_count_result = $this->l_def_sql->read_data( $a_user_status_names_query_params );
		
		
		/*
			Paging
		*/
		$i_page_uri_segment = 10;
		$a_pagination_config['base_url'] = base_url() . 'admin/read_all_user_status/limit/' .$a_assoc_uri['limit']. '/order/' .$a_assoc_uri['order']. '/sort/' .$a_assoc_uri['sort']. '/offset/';
		$a_pagination_config['total_rows'] = $a_user_status_names_count_result['i_num_rows'];
		$a_pagination_config['per_page'] = $a_assoc_uri['limit'];
		$a_pagination_config['uri_segment'] = $i_page_uri_segment;
		$a_pagination_config['num_links'] = 5;
		$a_pagination_config['first_link'] = false;
		$a_pagination_config['last_link'] = false;
		$a_pagination_config['num_tag_open'] = '<li>';
		$a_pagination_config['num_tag_close'] = '</li>';
		$a_pagination_config['cur_tag_open'] = '<li class="clsli_pageactive_1"><a href="#" onclick="return false;">';
		$a_pagination_config['cur_tag_close'] = '</a></li>';
		$a_pagination_config['next_link'] = 'Next Page';
		$a_pagination_config['next_tag_open'] = '<li>';
		$a_pagination_config['next_tag_close'] = '</li>';
		$a_pagination_config['prev_link'] = 'Prev Page';
		$a_pagination_config['prev_tag_open'] = '<li>';
		$a_pagination_config['prev_tag_close'] = '</li>';
		$a_pagination_config['full_tag_open'] = '<ul>';
		$a_pagination_config['full_tag_close'] = '</ul>';
		$this->pagination->initialize($a_pagination_config); 
		$s_page_links_pagination = $this->pagination->create_links();
		
		
		//======================================================

		$s_view_site_responses_data = array();
		$s_view_site_responses_data['a_form_notice'] = $a_form_notice;
		$s_view_site_responses = $this->load->view('templates_v1/v_site_responses', $s_view_site_responses_data, true);
		
		$s_view_header_data = array();
		$s_view_header = $this->load->view('templates_v1/v_header_v1', $s_view_header_data, true);
		$a_nav_menu_data = array();
		$s_view_nav_menu = $this->load->view('templates_v1/v_nav_menu_v1', $a_nav_menu_data, true);
		$s_view_main_data = array();
		$s_view_main_data['s_view_site_responses'] = $s_view_site_responses;
		$s_view_main_data['a_assoc_uri'] = $a_assoc_uri;
		$s_view_main_data['a_expected_sort'] = $a_expected_sort;
		$s_view_main_data['a_expected_order'] = $a_expected_order;
		$s_view_main_data['a_user_status_names_result'] = $a_user_status_names_result;
		$s_view_main_data['s_page_links_pagination'] = $s_page_links_pagination;
		$s_view_main_data['s_nav_menu'] = $s_view_nav_menu;
		$s_view_main = $this->load->view('templates_v1/v_admin_read_all_user_status_names_v1', $s_view_main_data, true);
		
		$s_view_footer_data = array();
		$s_view_footer = $this->load->view('templates_v1/v_footer_v1', $s_view_footer_data, true);
		
		//======================================================

		
		//======================================================

		array_push($a_current_webpage_inc_css, 'css/css_reset.css');
		array_push($a_current_webpage_inc_css, 'css/css_style.css');

		array_push($a_current_webpage_inc_js_batch1, 'js/jquery-1.11.1.js');
		array_push($a_current_webpage_inc_js_batch1, 'js/js_detect_browser.js');

		$a_current_webpage_data['a_current_webpage_inc_metas'] = $a_current_webpage_inc_metas;
		$a_current_webpage_data['a_current_webpage_inc_css'] = $a_current_webpage_inc_css;
		$a_current_webpage_data['a_current_webpage_inc_js_batch1'] = $a_current_webpage_inc_js_batch1;
		$a_current_webpage_data['a_current_webpage_inc_js_batch2'] = $a_current_webpage_inc_js_batch2;
		$a_current_webpage_data['s_current_webpage_title'] = SITE_NAME;

		$a_current_webpage_data['s_current_webpage_header'] = $s_view_header;
		$a_current_webpage_data['s_current_webpage_main'] = $s_view_main;
		$a_current_webpage_data['s_current_webpage_footer'] = $s_view_footer;
		$s_current_webpage = $this->load->view('templates_v1/v_html_template_v1', $a_current_webpage_data, true);
		//======================================================

		
		//======================================================
		$this->output->set_output($s_current_webpage);
		//======================================================
	}
	

	/**
	* update_user_status_form
	* @desc		
	*
	**/
	public function update_user_status_form( $a_form_notice = array() )
	{
		//= Declare Start-Up Variables Here ====================
		$a_current_webpage_inc_metas = array();
		$a_current_webpage_inc_css = array();
		$a_current_webpage_inc_js_batch1 = array();
		$a_current_webpage_inc_js_batch2 = array();

		$s_view_header = '';
		$s_view_main = '';
		$s_view_footer = '';
		//======================================================
		
		
		//======================================================
		$this->load->library(array('pagination'));
		$this->load->library(array('l_user'));
		$this->load->library('l_def_sql');
		
		$this->load->model('m_def_table_fields');

		//======================================================
		
		
		//======================================================
		$a_user_details = $this->session->userdata('a_user_details');
		$a_user_roles_result = $this->session->userdata('a_user_roles_result');
		if( !isset($a_user_details) || empty($a_user_details) )
		{ redirect( base_url() . 'user/logout', 'refresh'); }
		if( array_key_exists( 1, $a_user_roles_result )	)
		{}
		else
		{ 
			$s_notice = '';
			$s_notice_query = '';
			$a_notice_params = array();
			$a_notice_params['message'] = 'Access Denied';
			$a_notice_params['subject'] = '';
			$a_notice_params['message_status'] = 'error';
			$s_notice_query = http_build_query($a_notice_params);
			$s_notice_query = $this->encrypt->encode($s_notice_query);
			$s_notice_query = base64_encode($s_notice_query);
			redirect( base_url() . 'notice/' . $s_notice_query, 'refresh'); 
		}
		
		
		/*
			parse uri first
		*/
		$a_expected_uri = array('user_status_name_id');
		$a_assoc_uri = $this->uri->uri_to_assoc(3, $a_expected_uri);
		
		
		/**/
		if( !isset($a_assoc_uri['user_status_name_id']) || empty($a_assoc_uri['user_status_name_id']) )
		{
			redirect( base_url(), 'refresh');
		}		
		
		
		/*
		*/
		$a_user_status_names_result = array();
		$a_user_status_names_row_result = array();
		$a_user_status_names_query_where = array();
		$a_user_status_names_query_table_join = array();
		$a_user_status_names_query_limit = array();
		$a_user_status_names_query_params = array();
		array_push( $a_user_status_names_query_where, array( 's_field' => 'user_status_names.i_id', 'a_data' => $a_assoc_uri['user_status_name_id'] ) );
		$a_user_status_names_query_limit = array('i_limit' => 1, 'i_offset' => 0);
		$a_user_status_names_query_params['a_where'] = $a_user_status_names_query_where;
		$a_user_status_names_query_params['a_limit'] = $a_user_status_names_query_limit;
		$a_user_status_names_query_params['s_table_fields'] = $this->m_def_table_fields->s_user_status_names_fields;
		$a_user_status_names_query_params['s_table_name'] = 'user_status_names';
		$a_user_status_names_result = $this->l_def_sql->read_data( $a_user_status_names_query_params );
		if( isset($a_user_status_names_result) && !empty($a_user_status_names_result) )
		{ $a_user_status_names_row_result = $a_user_status_names_result[0]; }
		if( !isset($a_user_status_names_row_result) || empty($a_user_status_names_row_result) )
		{ redirect( base_url(), 'refresh'); }
		
		//======================================================
		
		$s_view_site_responses_data = array();
		$s_view_site_responses_data['a_form_notice'] = $a_form_notice;
		$s_view_site_responses = $this->load->view('templates_v1/v_site_responses', $s_view_site_responses_data, true);
		
		$s_view_header_data = array();
		$s_view_header = $this->load->view('templates_v1/v_header_v1', $s_view_header_data, true);
		
		$a_nav_menu_data = array();
		$s_view_nav_menu = $this->load->view('templates_v1/v_nav_menu_v1', $a_nav_menu_data, true);
		
		$s_view_main_data = array();
		$s_view_main_data['s_view_site_responses'] = $s_view_site_responses;
		$s_view_main_data['a_user_status_names_row_result'] = $a_user_status_names_row_result;
		$s_view_main_data['s_nav_menu'] = $s_view_nav_menu;
		$s_view_main = $this->load->view('templates_v1/v_admin_update_user_status_name_v1', $s_view_main_data, true);
		
		$s_view_footer_data = array();
		$s_view_footer = $this->load->view('templates_v1/v_footer_v1', $s_view_footer_data, true);
		
		//======================================================

		
		//======================================================

		array_push($a_current_webpage_inc_css, 'css/css_reset.css');
		array_push($a_current_webpage_inc_css, 'css/css_style.css');

		array_push($a_current_webpage_inc_js_batch1, 'js/jquery-1.11.1.js');
		array_push($a_current_webpage_inc_js_batch1, 'js/js_detect_browser.js');

		$a_current_webpage_data['a_current_webpage_inc_metas'] = $a_current_webpage_inc_metas;
		$a_current_webpage_data['a_current_webpage_inc_css'] = $a_current_webpage_inc_css;
		$a_current_webpage_data['a_current_webpage_inc_js_batch1'] = $a_current_webpage_inc_js_batch1;
		$a_current_webpage_data['a_current_webpage_inc_js_batch2'] = $a_current_webpage_inc_js_batch2;
		$a_current_webpage_data['s_current_webpage_title'] = SITE_NAME;

		$a_current_webpage_data['s_current_webpage_header'] = $s_view_header;
		$a_current_webpage_data['s_current_webpage_main'] = $s_view_main;
		$a_current_webpage_data['s_current_webpage_footer'] = $s_view_footer;
		$s_current_webpage = $this->load->view('templates_v1/v_html_template_v1', $a_current_webpage_data, true);
		//======================================================

		
		//======================================================
		$this->output->set_output($s_current_webpage);
		//======================================================
	}
	
	
	/**
	* update_user_status
	* @desc		
	*
	**/
	public function update_user_status()
	{
		//= Declare Start-Up Variables Here ====================
		$a_form_notice = array();
		$a_site_response_error = array();
		$a_site_response_info = array();
		$a_site_response_success = array();
		
		$b_user_role_names_problem = false;
		//======================================================
		
		
		//======================================================
		$this->load->library('form_validation');
		$this->load->library('l_def_sql');
		
		$this->load->helper(array('form', 'security'));
		
		$this->load->model('m_def_table_fields');
		//======================================================
		
		
		//======================================================
		$a_user_details = $this->session->userdata('a_user_details');
		$a_user_roles_result = $this->session->userdata('a_user_roles_result');
		if( !isset($a_user_details) || empty($a_user_details) )
		{ redirect( base_url() . 'user/logout', 'refresh'); }
		if( array_key_exists( 1, $a_user_roles_result )	)
		{}
		else
		{ 
			$s_notice = '';
			$s_notice_query = '';
			$a_notice_params = array();
			$a_notice_params['message'] = 'Access Denied';
			$a_notice_params['subject'] = '';
			$a_notice_params['message_status'] = 'error';
			$s_notice_query = http_build_query($a_notice_params);
			$s_notice_query = $this->encrypt->encode($s_notice_query);
			$s_notice_query = base64_encode($s_notice_query);
			redirect( base_url() . 'notice/' . $s_notice_query, 'refresh');
		}
		
		
		/*
			parse uri first
		*/
		$a_expected_uri = array('user_status_name_id');
		$a_assoc_uri = $this->uri->uri_to_assoc(3, $a_expected_uri);
		
		
		/**/
		if( !isset($a_assoc_uri['user_status_name_id']) || empty($a_assoc_uri['user_status_name_id']) )
		{
			redirect( base_url(), 'refresh');
		}
		
		
		/*
			get user_status_names being edited
		*/
		$a_user_status_names_result = array();
		$a_user_status_names_row_result = array();
		$a_user_status_names_query_where = array();
		$a_user_status_names_query_table_join = array();
		$a_user_status_names_query_limit = array();
		$a_user_status_names_query_params = array();
		array_push( $a_user_status_names_query_where, array( 's_field' => 'user_status_names.i_id', 'a_data' => $a_assoc_uri['user_status_name_id'] ) );
		$a_user_status_names_query_limit = array('i_limit' => 1, 'i_offset' => 0);
		$a_user_status_names_query_params['a_where'] = $a_user_status_names_query_where;
		$a_user_status_names_query_params['a_limit'] = $a_user_status_names_query_limit;
		$a_user_status_names_query_params['s_table_fields'] = $this->m_def_table_fields->s_user_status_names_fields;
		$a_user_status_names_query_params['s_table_name'] = 'user_status_names';
		$a_user_status_names_result = $this->l_def_sql->read_data( $a_user_status_names_query_params );
		if( isset($a_user_status_names_result) && !empty($a_user_status_names_result) )
		{ $a_user_status_names_row_result = $a_user_status_names_result[0]; }
		if( !isset($a_user_status_names_row_result) || empty($a_user_status_names_row_result) )
		{ redirect( base_url(), 'refresh'); }
		
		
		if( isset($_POST) && !empty($_POST) 
			&& $_POST['submit'] == 'Update'
		)
		{
			if( 	isset($_POST['txt_admin_update_user_status_name_name']) && !empty($_POST['txt_admin_update_user_status_name_name']) 
				&&	$_POST['txt_admin_update_user_status_name_name'] != $a_user_status_names_row_result['s_usn_name']
			)
			{
				$this->form_validation->set_rules('txt_admin_update_user_status_name_name', 'Status Name', 'trim|required|min_length[4]|max_length[12]|xss_clean|is_unique[user_status_names.s_name]');
			}
			else
			{
				$this->form_validation->set_rules('txt_admin_update_user_status_name_name', 'Status Name', 'trim|required|min_length[4]|max_length[12]|xss_clean');
			}
			
			
			if( $this->form_validation->run() == FALSE )
			{
				$a_form_notice['s_txt_admin_update_user_status_name_name_error'] = form_error('txt_admin_update_user_status_name_name', ' ', ' ');
				if( isset($a_form_notice['s_txt_admin_update_user_status_name_name_error']) && !empty($a_form_notice['s_txt_admin_update_user_status_name_name_error']) )
				{
					array_push( $a_site_response_error, $a_form_notice['s_txt_admin_update_user_status_name_name_error'] );
				}
				
				$a_form_notice['a_site_response_error'] = $a_site_response_error;

				$this->update_user_status_form($a_form_notice);
			}
			else
			{
				/*
				*/
				$a_update_user_status_names_query_where = array();
				$a_update_user_status_names_query_params = array();
				array_push( $a_update_user_status_names_query_where, array( 's_field' => 'user_status_names.i_id', 'a_data' => $a_assoc_uri['user_status_name_id'] ) );
				$a_update_data = array();
				$a_update_data['s_name'] = $_POST['txt_admin_update_user_status_name_name'];
				$a_update_user_status_names_query_params['a_where'] = $a_update_user_status_names_query_where;
				$a_update_user_status_names_query_params['a_update_data'] = $a_update_data;
				$a_update_user_status_names_query_params['s_table_name'] = 'user_status_names';
				$a_update_user_status_names_result = $this->l_def_sql->update_data( $a_update_user_status_names_query_params );
				
				
				if( 	isset($a_update_user_status_names_result) && !empty($a_update_user_status_names_result) 
					&&	array_key_exists("i_sql_result", $a_update_user_status_names_result)
					&& 	$a_update_user_status_names_result["i_sql_result"] == 1 
				)
				{
					array_push( $a_site_response_success, 'Update Successful' );
					$a_form_notice['a_site_response_success'] = $a_site_response_success;
					$this->update_user_status_form($a_form_notice);
				}
				else
				{
					array_push( $a_site_response_info, 'Unsuccessful Update. Please Contact Administrator' );
					$a_form_notice['a_site_response_info'] = $a_site_response_info;
					$this->update_user_status_form($a_form_notice);
				}
			}
		}
		elseif( isset($_POST) && !empty($_POST) 
			&& $_POST['submit'] == 'Delete'
		)
		{
			/*
				delete
			*/
			$a_delete_user_status_names_query_where = array();
			$a_delete_user_status_names_query_params = array();
			array_push( $a_delete_user_status_names_query_where, array( 's_field' => 'user_status_names.i_id', 'a_data' => $a_assoc_uri['user_status_name_id'] ) );
			$a_delete_user_status_names_query_params['a_where'] = $a_delete_user_status_names_query_where;
			$a_delete_user_status_names_query_params['s_table_name'] = 'user_status_names';
			$a_delete_user_status_names_result = $this->l_def_sql->delete_data( $a_delete_user_status_names_query_params );	
			
			
			if( 	isset($a_delete_user_status_names_result) && !empty($a_delete_user_status_names_result) 
				&&	array_key_exists("i_sql_result", $a_delete_user_status_names_result)
				&& 	$a_delete_user_status_names_result["i_sql_result"] == 1 
			)
			{
				array_push( $a_site_response_success, 'Delete Successful' );
				$a_form_notice['a_site_response_success'] = $a_site_response_success;
				$this->read_all_user_status($a_form_notice);
			}
			else
			{
				array_push( $a_site_response_info, 'Unsuccessful Delete. Please Contact Administrator' );
				$a_form_notice['a_site_response_info'] = $a_site_response_info;
				$this->update_user_status_form($a_form_notice);
			}
			
			
			
		}
		else
		{
			redirect( base_url() . 'admin/update_user_status_form/', 'refresh');
		}
		//======================================================
	}
	































	
}

/* End of file */
/* Location: ./application/controllers/ */