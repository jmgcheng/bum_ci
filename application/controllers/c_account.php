<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class C_account extends CI_Controller {
	
	/**
	* contructor
	* @desc		contructor for 
	*
	**/
	public function __construct()
	{
		parent::__construct();
		
		/*
		*/
		$this->load->library('l_user');
		$this->l_user->autologin();
	}
	

	/**
	* index
	* @desc		
	*
	**/
	public function index()
	{
		
	}
	
	
	/**
	* read_profile
	* @desc		
	*
	**/
	public function read_profile()
	{
		//= Declare Start-Up Variables Here ====================
		$a_current_webpage_inc_metas = array();
		$a_current_webpage_inc_css = array();
		$a_current_webpage_inc_js_batch1 = array();
		$a_current_webpage_inc_js_batch2 = array();

		$s_view_header = '';
		$s_view_main = '';
		$s_view_footer = '';
		//======================================================
		
		
		//======================================================
		$this->load->library(array('pagination'));
		$this->load->library(array('l_user'));
		$this->load->library('l_def_sql');
		
		$this->load->model('m_def_table_fields');

		//======================================================
		
		
		//======================================================
		$a_user_details = $this->session->userdata('a_user_details');
		$a_user_roles_result = $this->session->userdata('a_user_roles_result');
		if( !isset($a_user_details) || empty($a_user_details) )
		{ redirect( base_url() . 'user/logout', 'refresh'); }
		
		//======================================================
		
		
		$s_view_header_data = array();
		$s_view_header = $this->load->view('templates_v1/v_header_v1', $s_view_header_data, true);
		
		$a_nav_menu_data = array();
		$s_view_nav_menu = $this->load->view('templates_v1/v_nav_menu_v1', $a_nav_menu_data, true);
		
		$s_view_main_data = array();
		$s_view_main_data['a_user_details'] = $a_user_details;
		$s_view_main_data['a_user_roles_result'] = $a_user_roles_result;
		$s_view_main_data['s_nav_menu'] = $s_view_nav_menu;
		$s_view_main = $this->load->view('templates_v1/v_account_read_profile_v1', $s_view_main_data, true);
		
		$s_view_footer_data = array();
		$s_view_footer = $this->load->view('templates_v1/v_footer_v1', $s_view_footer_data, true);
		
		//======================================================

		
		//======================================================

		array_push($a_current_webpage_inc_css, 'css/css_reset.css');
		array_push($a_current_webpage_inc_css, 'css/css_style.css');

		array_push($a_current_webpage_inc_js_batch1, 'js/jquery-1.11.1.js');
		array_push($a_current_webpage_inc_js_batch1, 'js/js_detect_browser.js');

		$a_current_webpage_data['a_current_webpage_inc_metas'] = $a_current_webpage_inc_metas;
		$a_current_webpage_data['a_current_webpage_inc_css'] = $a_current_webpage_inc_css;
		$a_current_webpage_data['a_current_webpage_inc_js_batch1'] = $a_current_webpage_inc_js_batch1;
		$a_current_webpage_data['a_current_webpage_inc_js_batch2'] = $a_current_webpage_inc_js_batch2;
		$a_current_webpage_data['s_current_webpage_title'] = SITE_NAME;

		$a_current_webpage_data['s_current_webpage_header'] = $s_view_header;
		$a_current_webpage_data['s_current_webpage_main'] = $s_view_main;
		$a_current_webpage_data['s_current_webpage_footer'] = $s_view_footer;
		$s_current_webpage = $this->load->view('templates_v1/v_html_template_v1', $a_current_webpage_data, true);
		//======================================================

		
		//======================================================
		$this->output->set_output($s_current_webpage);
		//======================================================
	}
	
	
	/**
	* update_profile_form
	* @desc		
	*
	**/
	public function update_profile_form( $a_form_notice = array() )
	{
		//= Declare Start-Up Variables Here ====================
		$a_current_webpage_inc_metas = array();
		$a_current_webpage_inc_css = array();
		$a_current_webpage_inc_js_batch1 = array();
		$a_current_webpage_inc_js_batch2 = array();

		$s_view_header = '';
		$s_view_main = '';
		$s_view_footer = '';
		//======================================================
		
		
		//======================================================
		$this->load->library(array('pagination'));
		$this->load->library(array('l_user'));
		$this->load->library('l_def_sql');
		
		$this->load->model('m_def_table_fields');

		//======================================================
		
		
		//======================================================
		$a_user_details = $this->session->userdata('a_user_details');
		$a_user_roles_result = $this->session->userdata('a_user_roles_result');
		if( !isset($a_user_details) || empty($a_user_details) )
		{ redirect( base_url() . 'user/logout', 'refresh'); }
		if( !isset($a_user_details['i_u_id']) || empty($a_user_details['i_u_id']) )
		{ redirect( base_url() . 'user/logout', 'refresh'); }
		
		
		/*
		*/
		$a_u_result = array();
		$a_u_row_result = array();
		$a_u_query_where = array();
		$a_u_query_limit = array();
		$a_u_query_params = array();
		array_push( $a_u_query_where, array( 's_field' => 'users.i_id', 'a_data' => $a_user_details['i_u_id'] ) );
		$a_u_query_limit = array('i_limit' => 1, 'i_offset' => 0);
		$a_u_query_params['a_where'] = $a_u_query_where;
		$a_u_query_params['a_limit'] = $a_u_query_limit;
		$a_u_query_params['s_table_fields'] = $this->m_def_table_fields->s_users_fields;
		$a_u_query_params['s_table_name'] = 'users';
		$a_u_result = $this->l_def_sql->read_data( $a_u_query_params );
		if( isset($a_u_result) && !empty($a_u_result) )
		{ $a_u_row_result = $a_u_result[0]; }
		if( !isset($a_u_row_result) || empty($a_u_row_result) )
		{ redirect( base_url(), 'refresh'); }
		
		
		//======================================================
		
		$s_view_site_responses_data = array();
		$s_view_site_responses_data['a_form_notice'] = $a_form_notice;
		$s_view_site_responses = $this->load->view('templates_v1/v_site_responses', $s_view_site_responses_data, true);
		
		$s_view_header_data = array();
		$s_view_header = $this->load->view('templates_v1/v_header_v1', $s_view_header_data, true);
		
		$a_nav_menu_data = array();
		$s_view_nav_menu = $this->load->view('templates_v1/v_nav_menu_v1', $a_nav_menu_data, true);
		
		$s_view_main_data = array();
		$s_view_main_data['s_view_site_responses'] = $s_view_site_responses;
		$s_view_main_data['a_u_row_result'] = $a_u_row_result;
		$s_view_main_data['s_nav_menu'] = $s_view_nav_menu;
		$s_view_main = $this->load->view('templates_v1/v_account_update_profile_v1', $s_view_main_data, true);
		
		$s_view_footer_data = array();
		$s_view_footer = $this->load->view('templates_v1/v_footer_v1', $s_view_footer_data, true);
		
		//======================================================

		
		//======================================================

		array_push($a_current_webpage_inc_css, 'css/css_reset.css');
		array_push($a_current_webpage_inc_css, 'css/css_style.css');

		array_push($a_current_webpage_inc_js_batch1, 'js/jquery-1.11.1.js');
		array_push($a_current_webpage_inc_js_batch1, 'js/js_detect_browser.js');

		$a_current_webpage_data['a_current_webpage_inc_metas'] = $a_current_webpage_inc_metas;
		$a_current_webpage_data['a_current_webpage_inc_css'] = $a_current_webpage_inc_css;
		$a_current_webpage_data['a_current_webpage_inc_js_batch1'] = $a_current_webpage_inc_js_batch1;
		$a_current_webpage_data['a_current_webpage_inc_js_batch2'] = $a_current_webpage_inc_js_batch2;
		$a_current_webpage_data['s_current_webpage_title'] = SITE_NAME;

		$a_current_webpage_data['s_current_webpage_header'] = $s_view_header;
		$a_current_webpage_data['s_current_webpage_main'] = $s_view_main;
		$a_current_webpage_data['s_current_webpage_footer'] = $s_view_footer;
		$s_current_webpage = $this->load->view('templates_v1/v_html_template_v1', $a_current_webpage_data, true);
		//======================================================

		
		//======================================================
		$this->output->set_output($s_current_webpage);
		//======================================================
	}
	
	
	/**
	* update_profile
	* @desc		
	*
	**/
	public function update_profile()
	{
		//= Declare Start-Up Variables Here ====================
		$a_form_notice = array();
		$a_site_response_error = array();
		$a_site_response_info = array();
		$a_site_response_success = array();
		//======================================================
		
		
		//======================================================
		$this->load->library('form_validation');
		$this->load->library('l_def_sql');
		
		$this->load->helper(array('form', 'security'));
		
		$this->load->model('m_def_table_fields');
		//======================================================
		
		
		//======================================================
		$a_user_details = $this->session->userdata('a_user_details');
		$a_user_roles_result = $this->session->userdata('a_user_roles_result');
		if( !isset($a_user_details) || empty($a_user_details) )
		{ redirect( base_url() . 'user/logout', 'refresh'); }
		if( !isset($a_user_details['i_u_id']) || empty($a_user_details['i_u_id']) )
		{ redirect( base_url() . 'user/logout', 'refresh'); }
		
		
		if( isset($_POST) && !empty($_POST) )
		{
			if( 	isset($_POST['txt_account_update_profile_email']) && !empty($_POST['txt_account_update_profile_email']) 
				&&	$_POST['txt_account_update_profile_email'] != $a_user_details['s_u_email']
			)
			{
				$this->form_validation->set_rules('txt_account_update_profile_email', 'Email', 'trim|required|max_length[55]valid_email|xss_clean|is_unique[users.s_email]');
			}
			else
			{
				$this->form_validation->set_rules('txt_account_update_profile_email', 'Email', 'trim|required|max_length[55]valid_email|xss_clean');
			}
			if( 	isset($_POST['txt_account_update_profile_username']) && !empty($_POST['txt_account_update_profile_username']) 
				&&	$_POST['txt_account_update_profile_username'] != $a_user_details['s_u_username']
			)
			{
				$this->form_validation->set_rules('txt_account_update_profile_username', 'Username', 'trim|required|min_length[5]|max_length[12]|xss_clean|is_unique[users.s_username]');
			}
			else
			{
				$this->form_validation->set_rules('txt_account_update_profile_username', 'Username', 'trim|required|min_length[5]|max_length[12]|xss_clean');
			}
			/*
				$str = do_hash($_POST['txt_account_update_profile_new_password'], 'md5');
			*/
			if( 	isset($_POST['txt_account_update_profile_new_password']) && !empty($_POST['txt_account_update_profile_new_password']) 
				&&	do_hash($_POST['txt_account_update_profile_new_password'], 'md5') != $a_user_details['s_u_password']
			)
			{
				$this->form_validation->set_rules('txt_account_update_profile_new_password', 'New Password', 'trim|required|matches[txt_account_update_profile_new_password_conf]|md5|xss_clean');
				$this->form_validation->set_rules('txt_account_update_profile_new_password_conf', 'New Password Confirmation', 'trim|required|xss_clean');
			}
			$this->form_validation->set_rules('txt_account_update_profile_firstname', 'First Name', 'trim|required|min_length[3]|max_length[50]|xss_clean');
			$this->form_validation->set_rules('txt_account_update_profile_lastname', 'Last Name', 'trim|min_length[3]|max_length[50]|xss_clean');
			
			
			if( $this->form_validation->run() == FALSE )
			{
				$a_form_notice['s_txt_account_update_profile_email_error'] = form_error('txt_account_update_profile_email', ' ', ' ');
				if( isset($a_form_notice['s_txt_account_update_profile_email_error']) && !empty($a_form_notice['s_txt_account_update_profile_email_error']) )
				{
					array_push( $a_site_response_error, $a_form_notice['s_txt_account_update_profile_email_error'] );
				}
				$a_form_notice['s_txt_account_update_profile_username_error'] = form_error('txt_account_update_profile_username', ' ', ' ');
				if( isset($a_form_notice['s_txt_account_update_profile_username_error']) && !empty($a_form_notice['s_txt_account_update_profile_username_error']) )
				{
					array_push( $a_site_response_error, $a_form_notice['s_txt_account_update_profile_username_error'] );
				}
				$a_form_notice['s_txt_account_update_profile_firstname_error'] = form_error('txt_account_update_profile_firstname', ' ', ' ');
				if( isset($a_form_notice['s_txt_account_update_profile_firstname_error']) && !empty($a_form_notice['s_txt_account_update_profile_firstname_error']) )
				{
					array_push( $a_site_response_error, $a_form_notice['s_txt_account_update_profile_firstname_error'] );
				}
				$a_form_notice['s_txt_account_update_profile_lastname_error'] = form_error('txt_account_update_profile_lastname', ' ', ' ');
				if( isset($a_form_notice['s_txt_account_update_profile_lastname_error']) && !empty($a_form_notice['s_txt_account_update_profile_lastname_error']) )
				{
					array_push( $a_site_response_error, $a_form_notice['s_txt_account_update_profile_lastname_error'] );
				}
				$a_form_notice['s_txt_account_update_profile_new_password_error'] = form_error('txt_account_update_profile_new_password', ' ', ' ');
				if( isset($a_form_notice['s_txt_account_update_profile_new_password_error']) && !empty($a_form_notice['s_txt_account_update_profile_new_password_error']) )
				{
					array_push( $a_site_response_error, $a_form_notice['s_txt_account_update_profile_new_password_error'] );
				}
				$a_form_notice['s_txt_account_update_profile_new_password_conf_error'] = form_error('txt_account_update_profile_new_password_conf', ' ', ' ');
				if( isset($a_form_notice['s_txt_account_update_profile_new_password_conf_error']) && !empty($a_form_notice['s_txt_account_update_profile_new_password_conf_error']) )
				{
					array_push( $a_site_response_error, $a_form_notice['s_txt_account_update_profile_new_password_conf_error'] );
				}
				$a_form_notice['a_site_response_error'] = $a_site_response_error;

				$this->update_profile_form($a_form_notice);
			}
			else
			{
				/*
				*/
				$a_update_u_query_where = array();
				$a_update_u_query_params = array();
				array_push( $a_update_u_query_where, array( 's_field' => 'users.i_id', 'a_data' => $a_user_details['i_u_id'] ) );
				$a_update_data = array();
				$a_update_data['s_email'] = $_POST['txt_account_update_profile_email'];
				$a_update_data['s_username'] = $_POST['txt_account_update_profile_username'];
				$a_update_data['s_firstname'] = $_POST['txt_account_update_profile_firstname'];
				$a_update_data['s_lastname'] = $_POST['txt_account_update_profile_lastname'];
				if( isset($_POST['txt_account_update_profile_new_password']) && !empty($_POST['txt_account_update_profile_new_password']) )
				{
					$a_update_data['s_password'] = $_POST['txt_account_update_profile_new_password'];
				}
				$a_update_u_query_params['a_where'] = $a_update_u_query_where;
				$a_update_u_query_params['a_update_data'] = $a_update_data;
				$a_update_u_query_params['s_table_name'] = 'users';
				$a_update_u_result = $this->l_def_sql->update_data( $a_update_u_query_params );
				
				
				if( 	isset($a_update_u_result) && !empty($a_update_u_result) 
					&&	array_key_exists("i_sql_result", $a_update_u_result)
					&& 	$a_update_u_result["i_sql_result"] == 1 
				)
				{
					/*
						update user session details
					*/
					$a_u_result = array();
					$a_u_row_result = array();
					$a_u_query_where = array();
					$a_u_query_limit = array();
					$a_u_query_params = array();
					array_push( $a_u_query_where, array( 's_field' => 'users.i_id', 'a_data' => $a_user_details['i_u_id'] ) );
					$a_u_query_limit = array('i_limit' => 1, 'i_offset' => 0);
					$a_u_query_params['a_where'] = $a_u_query_where;
					$a_u_query_params['a_limit'] = $a_u_query_limit;
					$a_u_query_params['s_table_fields'] = $this->m_def_table_fields->s_users_fields;
					$a_u_query_params['s_table_name'] = 'users';
					$a_u_result = $this->l_def_sql->read_data( $a_u_query_params );
					if( isset($a_u_result) && !empty($a_u_result) )
					{ $a_u_row_result = $a_u_result[0]; }
					if( !isset($a_u_row_result) || empty($a_u_row_result) )
					{ redirect( base_url() . 'user/logout', 'refresh'); }
					$a_session_user_details = array(
						'a_user_details'  => $a_u_row_result
					);
					$this->session->set_userdata($a_session_user_details);
					
					
					array_push( $a_site_response_success, 'Update Successful' );
					$a_form_notice['a_site_response_success'] = $a_site_response_success;
					$this->update_profile_form($a_form_notice);
				}
				else
				{
					array_push( $a_site_response_info, 'Unsuccessful Update. Please Contact Administrator' );
					$a_form_notice['a_site_response_info'] = $a_site_response_info;
					$this->update_profile_form($a_form_notice);
				}
			}
		}
		else
		{
			redirect( base_url() . 'account/update_profile_form/', 'refresh');
		}
		//======================================================
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}

/* End of file */
/* Location: ./application/controllers/ */